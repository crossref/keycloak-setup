FROM quay.io/keycloak/keycloak:23.0.5
COPY extensions/crossref-management/target/deploy/*.jar /opt/keycloak/providers/
ENV KC_HTTP_RELATIVE_PATH=/auth
ENV KC_HTTP_PORT=8888
#ENV JAVA_OPTS_APPEND="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005"
