# Keymaker

Crossref authentication and authorization framework

## Getting started

### Prerequisites

Make sure you have cloned the integration project, and set up in your PATH the cloned directory.

Make sure you have globally setup in your `.bashrc` the env var KEYCLOAK_DEV_DIR pointing to your cloned `idm` project root directory

### Using Docker

Keycloak version we are using is the latest `16.1.0` and it is downloaded from dockerhub. The way we are running it is by creating a new docker image updating the keycloak one by adding our own extensions in it. 

The script `build_and_run.sh` does everything for you:

* Compiles the java extension (in a docker container)
* copies it to a extesions_mount directory
* runs integration project `idm_dev` setup with user and password `admin:admin` mounting the compiled extension
* you can access keycloak on https://localhost:8888/auth/admin/

To test the current extension you can execute `run_tests.sh`
