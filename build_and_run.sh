#!/bin/bash
#./create_certs.sh


if ! type "integration" > /dev/null 2> /dev/null; then
    echo "Crossref integration tool not found in path, please clone and include it in the PATH"
    exit -1
fi

if [ ${1:-X} = "debug" ] 
then
	DBG="-e KEYCLOAK_LOGLEVEL=DEBUG";
fi

integration idm_dev stop

rm -rf extensions_mount
docker-compose -f idm-devenv.yml up compile_extension || exit -1
docker-compose -f idm-devenv.yml rm -fvs
mkdir -p extensions_mount
cp extensions/crossref-management/target/deploy/*.jar extensions_mount
integration idm_dev start
