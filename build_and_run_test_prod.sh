#!/bin/bash
#./create_certs.sh
./create_dev_test_certs.sh

if [ ${1:-X} = "debug" ] 
then
	DBG="-e KEYCLOAK_LOGLEVEL=DEBUG";
fi

mvn clean && mvn package -DskipTests -Drevision=NA
docker build -t keycloak-km -f Dockerfile.test.prod .

COMPOSEFILE=docker-compose-test-prod.yml

docker-compose -f $COMPOSEFILE kill
docker-compose -f $COMPOSEFILE rm --force

docker-compose -f $COMPOSEFILE up $1
