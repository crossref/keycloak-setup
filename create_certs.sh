#!/bin/bash

if [ ! -d cert ] 
then
  mkdir cert
fi

if [ ! -f cert/application.keystore ] 
then
    echo "generation certificate for keycloak in ====> cert/application.keystore"
    keytool -genkey -alias localhost -keyalg RSA -keystore cert/application.keystore -validity 10000 -storepass password -keypass password -dname "cn=localhost, ou=Unknown, o=Unknown, c=Unknown"
    echo "generation certificate for webbrowser in ====> cert/cert.pem"
    keytool -exportcert -rfc -keystore cert/application.keystore -alias localhost -file cert/certificate.pem -keypass password -storepass password
fi

