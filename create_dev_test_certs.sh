#!/bin/bash

if [ ! -d dev-test-certs ] 
then
  mkdir dev-test-certs
fi

if [ ! -f cert/application.keystore ] 
then
    echo "generating certificate and key file for keycloak running under docker.local"
    openssl req -newkey rsa:2048 -nodes -keyout dev-test-certs/key.pem -x509 -days 365 -out dev-test-certs/cert.pem -subj "/C=US/ST=SomeState/L=SomeCity/O=Crossref/OU=SomeDepartment/CN=localhost" -batch
fi

