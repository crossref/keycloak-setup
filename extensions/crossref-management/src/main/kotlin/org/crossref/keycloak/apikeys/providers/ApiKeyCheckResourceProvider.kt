package org.crossref.keycloak.apikeys.providers // ktlint-disable filename

import com.google.common.cache.Cache
import org.crossref.keycloak.apikeys.resources.ApiKeyCheckResource
import org.crossref.keycloak.apikeys.resources.PermissionInfo
import org.keycloak.services.resource.RealmResourceProvider

class ApiKeyCheckResourceProvider(
    private val session: org.keycloak.models.KeycloakSession,
    private val apiKeyCache: Cache<Pair<String, String>, PermissionInfo>,
) : RealmResourceProvider {

    override fun close() {}
    override fun getResource(): Any = ApiKeyCheckResource(session, apiKeyCache)
}
