package org.crossref.keycloak.apikeys.providers

import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import org.crossref.keycloak.apikeys.resources.PermissionInfo
import org.crossref.keycloak.util.Constants.Crossref.Companion.DEFAULT_APIKEY_CACHE_SIZE
import org.crossref.keycloak.util.Constants.Crossref.Companion.DEFAULT_APIKEY_CACHE_TTL
import org.crossref.keycloak.util.CrossrefResourceProviderFactory
import org.keycloak.Config.Scope
import org.keycloak.models.KeycloakSession
import org.keycloak.services.resource.RealmResourceProvider
import java.util.concurrent.TimeUnit

class ApiKeyCheckResourceProviderFactory : CrossrefResourceProviderFactory() {
    private lateinit var apiKeyCache: Cache<Pair<String, String>, PermissionInfo>
    companion object {
        val CACHE_SIZE: Long = (System.getenv("APIKEY_CACHE_SIZE") ?: "").toLongOrNull() ?: DEFAULT_APIKEY_CACHE_SIZE
        val cacheTTL: Long = (System.getenv("APIKEY_CACHE_TTL") ?: "").toLongOrNull() ?: DEFAULT_APIKEY_CACHE_TTL
    }

    /**
     * This method tries to create and return a singleton class attribute containing the  infinispan cache
     * for ApiKey permissions. As it will be created in the first call, we need a thread safe  approach
     * to avoid failures during the first calls.
     */

    override fun initializeExtension(session: KeycloakSession) {
        apiKeyCache = CacheBuilder.newBuilder()
            .maximumSize(CACHE_SIZE)
            .expireAfterWrite(cacheTTL, TimeUnit.SECONDS)
            .build()
    }

    override fun getResourceProvider(session: KeycloakSession): RealmResourceProvider {
        return ApiKeyCheckResourceProvider(session, apiKeyCache)
    }

    override fun init(config: Scope) {
        println("Crossref-ApiKey Plugin enabled! [ version = ${this.javaClass.`package`.implementationVersion} ]")
    }

    override fun getId(): String {
        return "checkApiKeyPermissions"
    }
}
