package org.crossref.keycloak.apikeys.resources

import com.fasterxml.jackson.core.type.TypeReference
import com.google.common.cache.Cache
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.crossref.keycloak.util.KeycloakUtils
import org.keycloak.OAuthErrorException
import org.keycloak.authorization.AuthorizationProvider
import org.keycloak.authorization.Decision
import org.keycloak.models.KeycloakSession
import org.keycloak.representations.idm.authorization.AuthorizationRequest
import org.keycloak.representations.idm.authorization.PolicyEvaluationRequest
import org.keycloak.services.ErrorResponseException
import org.keycloak.util.JsonSerialization.mapper
import java.util.concurrent.locks.ReentrantLock
import kotlin.streams.toList

data class PermissionInfo(val memberId: Long, val resources: Set<String>, val keyName: String, val enabled: Boolean)
class ApiKeyCheckResource(
    private val session: KeycloakSession,
    private val apiKeyCache: Cache<Pair<String, String>, PermissionInfo>,
) : KeycloakUtils() {

    private val realm = session.context.realm
    private var clientCreds: MutableMap<Pair<String, String>, Boolean> = mutableMapOf()
    private val clientCredsLock = ReentrantLock()

    /**
     * PermissionRequest is the object required to be sent by the  client in order to check permissions
     */
    data class PermissionRequest(
        val apiKeyHash: String = "",
        val clientSecret: String = "",
        val resource: String = "",
        val clientName: String = "",
        val scope: String = "",
        val resourcePath: String = "",
    )

    /**
     * This method fetches permissions from keycloak, given an apiKey and client credentials
     * the output is a complex object containing decisions, policies, resources, etc.
     * * Currently, for simplicity we only extract the resource names for those decisions where
     * the outcome was == Decision.PERMIT and return a set of those names
     * * Feel free to explore the evaluation object for more complex queries
     * println(ObjectMapper()
     *   .writeValueAsString(
     *      PolicyEvaluationResponseBuilder
     *        .build(evaluation, resourceServer, authorizationProvider, identity)))
     * */
    private fun fetchPermissions(apiKeyHash: String, clientName: String): PermissionInfo? {
        val emptyContext = mapOf("attributes" to mapOf<String, String>())
        val client = realm.getClientByClientId(clientName)
        val userList = session.users().searchForUserByUserAttributeStream(realm, "api-key", apiKeyHash).toList()

        if (userList.size != 1 || client == null) {
            return null
        }
        val user = userList[0]
        val memberId = user
            .groupsStream
            .findFirst()
            .let { if (it.isEmpty) null else it.get().attributes.getOrDefault("memberId", null)?.first()?.toLong() } ?: -1

        val request = PolicyEvaluationRequest()
        val authorizationProvider = session.getProvider(AuthorizationProvider::class.java)

        val resourceServer = authorizationProvider
            .storeFactory
            .resourceServerStore
            .findByClient(client)

        request.userId = user.id
        request.roleIds = user.realmRoleMappingsStream
            .filter { r -> r.name.matches("^ROLE_.*$".toRegex()) }
            .map { r -> r.name }
            .toList()
        request.context = emptyContext
        val identity = this.createIdentity(request, resourceServer, authorizationProvider, realm)

        return try {
            val authReq = AuthorizationRequest()
            val claims: MutableMap<String, List<String>> = mutableMapOf()
            val givenAttributes = request.context["attributes"]
            givenAttributes?.let {
                givenAttributes.forEach { (key, entryValue) ->
                    entryValue?.let {
                        claims[key] = it.split(",")
                    }
                }
            }
            authReq.claims = claims

            PermissionInfo(
                memberId,
                // not to evaluate permissions if api-key is disabled
                if (user.isEnabled) {
                    this.evaluate(request, createEvaluationContext(request, identity, authorizationProvider), authReq, resourceServer, authorizationProvider).results
                        .filter { p -> p.effect == Decision.Effect.PERMIT }
                        .map { p -> p.permission.resource.name }
                        .toSet()
                } else {
                    emptySet()
                },
                user.username,
                user.isEnabled,
            )
        } catch (e: Exception) {
            throw ErrorResponseException(
                OAuthErrorException.SERVER_ERROR,
                "Error while evaluating permissions.",
                Response.Status.INTERNAL_SERVER_ERROR,
            )
        } finally {
            identity.close()
        }
    }

    /**
     * checkApiKey is the endpoint where permission requests will be sent.
     * If there is permissions granted for a given PermissionRequest Object
     * 200 will be returned as a HTTP StatusCode
     * 401 will be returned otherwise
     */
    @jakarta.ws.rs.POST
    @jakarta.ws.rs.Produces("application/json")
    @jakarta.ws.rs.Consumes("application/json")
    fun checkApiKey(input: String): Response {
        val req = mapper.readValue(input, object : TypeReference<PermissionRequest>() {})

        val clientName = req.clientName
        val apiKeyHash = req.apiKeyHash
        val secret = req.clientSecret
        val resource = req.resource
        val clientCredPair = Pair(clientName, secret)

        if (!clientCreds.containsKey(clientCredPair)) {
            val client = realm.getClientByClientId(clientName)

            if (client != null && client.secret == secret) {
                clientCredsLock.lock()
                clientCreds = clientCreds.keys
                    .filter { p -> p.first != clientName }.associateWith { true }
                    .toMutableMap()
                clientCreds[clientCredPair] = true

                clientCredsLock.unlock()
            } else {
                return Response.status(401)
                    .entity(java.util.Collections.singletonMap("error", "Client credentials failed"))
                    .type(MediaType.APPLICATION_JSON)
                    .build()
            }
        }

        val creds = Pair(apiKeyHash, clientName)

        (
            apiKeyCache.getIfPresent(creds) ?: fetchPermissions(apiKeyHash, clientName)?.also {
                apiKeyCache.put(creds, it)
            }
            )?.let {
            // Access will be granted iif apikey is enabled and resource found
            val granted = it.enabled && it.resources.contains(resource)
            return Response.status(if (granted) 200 else 401)
                .entity(mapOf("access" to if (granted) "granted" else "denied", "memberId" to it.memberId, "keyName" to it.keyName))
                .type(MediaType.APPLICATION_JSON)
                .build()
        }
        return Response.status(401)
            .entity(mapOf("access" to "denied", "memberId" to -1, "keyName" to ""))
            .type(MediaType.APPLICATION_JSON)
            .build()
    }
}
