package org.crossref.keycloak.management

import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.validation.constraints.Pattern
import jakarta.validation.constraints.Size
import org.crossref.keycloak.util.Constants
import org.crossref.keycloak.util.Constants.Crossref.Companion.MAX_KEY_DESCRIPTION_LENGTH
import org.crossref.keycloak.util.Constants.Crossref.Companion.ORG_NAME_PATTERN
import org.keycloak.models.GroupModel
import org.keycloak.models.UserModel
import org.keycloak.models.jpa.entities.GroupEntity
import java.util.UUID

enum class Role(val roleName: String) {
    STAFF("ROLE_STAFF"),
    USER("ROLE_USER"),
    APIKEY("ROLE_APIKEY"),
    ADMIN("ROLE_ADMIN"),
    PWDSYNC("ROLE_PWDSYNC")
}

enum class Subscription {
    METADATA_PLUS
}

data class CrossrefManagementConfiguration(
    val apiKeySalt: String,
    val authenticatorUrl: String,
    val authenticatorCredentials: String
)

data class Organization(
    @JsonProperty("orgId")
    val orgId: UUID,
    @JsonProperty("name")
    @Pattern(regexp = ORG_NAME_PATTERN)
    val name: String,
    @JsonProperty("memberId")
    val memberId: Long
)

data class User(
    @JsonProperty("userId")
    var userId: UUID?,
    @JsonProperty("username")
    val username: String
)

data class APIKey(
    @JsonProperty("description")
    @Size(min = 0, max = MAX_KEY_DESCRIPTION_LENGTH)
    val description: String = "",
    @JsonProperty("issuedBy")
    val issuedBy: UUID,
    @JsonProperty("keyId")
    var keyId: UUID = UUID.randomUUID(),
    @JsonProperty("name")
    var name: String = "apikey-$keyId@crossref.org",
    @JsonProperty("enabled")
    var enabled: Boolean = true
)

data class APIKeyResponse(
    @JsonProperty("keyObject")
    val keyObject: APIKey,
    @JsonProperty("key")
    val key: String
)

data class ErrorResponse(
    @JsonProperty("message")
    val message: String,
    @JsonProperty("type")
    val type: Constants.ResourceResponseType = Constants.ResourceResponseType.INTERNAL_ERROR
)

fun fromGroupModel(group: GroupModel) = Organization(
    UUID.fromString(group.id),
    group.name,
    (group.attributes?.get("memberId") ?: listOf("-1"))[0].toLong()
)

fun fromGroupEntity(group: GroupEntity) = Organization(
    UUID.fromString(group.id),
    group.name,
    group.attributes.firstOrNull { it.name.equals("memberId") }?.value?.toLongOrNull() ?: -1
)

fun fromUserModel(user: UserModel) = User(
    UUID.fromString(user.id),
    user.username
)

fun fromUserModelToKey(user: UserModel) = APIKey(
    user.attributes?.get("description")?.get(0) ?: "",
    UUID.fromString(user.attributes?.get("issuedBy")?.get(0) ?: ""),
    UUID.fromString(user.id),
    user.username,
    user.isEnabled
)

enum class SyncOperation {
    DELETE,
    CREATE,
    SYNC
}

data class UserSyncMessage(
    @JsonProperty("username")
    val username: String,
    @JsonProperty("operation")
    val operation: SyncOperation,
    @JsonProperty("salt")
    val salt: String,
    @JsonProperty("hash")
    val hash: String,
    @JsonProperty("algorithm")
    val algorithm: String,
    @JsonProperty("iterations")
    val iterations: Int
)
