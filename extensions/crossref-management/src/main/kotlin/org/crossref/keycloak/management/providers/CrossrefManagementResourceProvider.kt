package org.crossref.keycloak.management.providers // ktlint-disable filename

import org.crossref.keycloak.management.CrossrefManagementConfiguration
import org.crossref.keycloak.management.resources.CrossrefManagementResource
import org.keycloak.models.KeycloakSession
import org.keycloak.services.resource.RealmResourceProvider

class CrossrefManagementResourceProvider(
    private val session: KeycloakSession,
    private val config: CrossrefManagementConfiguration
) : RealmResourceProvider {
    override fun close() {}
    override fun getResource(): Any = CrossrefManagementResource(session, config)
}
