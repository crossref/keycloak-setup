package org.crossref.keycloak.management.providers

import org.crossref.keycloak.management.CrossrefManagementConfiguration
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.util.Constants.Crossref.Companion.ORGS_BASE_GROUP
import org.crossref.keycloak.util.Constants.Crossref.Companion.PWDSYNC_USER
import org.crossref.keycloak.util.Constants.Crossref.Companion.REALM
import org.crossref.keycloak.util.Constants.Crossref.Companion.VOID_GROUP
import org.crossref.keycloak.util.Constants.Crossref.Companion.clientResources
import org.crossref.keycloak.util.Constants.Crossref.Companion.realmClients
import org.crossref.keycloak.util.CrossrefResourceProviderFactory
import org.jboss.logging.Logger
import org.keycloak.Config.Scope
import org.keycloak.authorization.AuthorizationProvider
import org.keycloak.authorization.AuthorizationProviderFactory
import org.keycloak.authorization.model.Policy
import org.keycloak.authorization.model.Resource
import org.keycloak.common.enums.SslRequired
import org.keycloak.credential.hash.PasswordHashProvider
import org.keycloak.models.ClientModel
import org.keycloak.models.Constants
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory
import org.keycloak.models.PasswordPolicy.parse
import org.keycloak.models.RealmModel
import org.keycloak.models.utils.DefaultKeyProviders
import org.keycloak.models.utils.RepresentationToModel
import org.keycloak.models.utils.RepresentationToModel.toModel
import org.keycloak.protocol.oidc.OIDCConfigAttributes.BACKCHANNEL_LOGOUT_SESSION_REQUIRED
import org.keycloak.protocol.oidc.OIDCConfigAttributes.BACKCHANNEL_LOGOUT_URL
import org.keycloak.protocol.oidc.OIDCConfigAttributes.USE_REFRESH_TOKEN_FOR_CLIENT_CREDENTIALS_GRANT
import org.keycloak.representations.idm.ClientRepresentation
import org.keycloak.representations.idm.CredentialRepresentation
import org.keycloak.representations.idm.authorization.PolicyRepresentation
import org.keycloak.representations.idm.authorization.ResourceOwnerRepresentation
import org.keycloak.representations.idm.authorization.ResourcePermissionRepresentation
import org.keycloak.representations.idm.authorization.ResourceRepresentation
import org.keycloak.services.managers.ClientManager
import org.keycloak.services.managers.RealmManager
import org.keycloak.services.resource.RealmResourceProvider
import org.keycloak.util.JsonSerialization.mapper
import java.io.PrintWriter
import java.io.StringWriter
import java.net.URI
import java.util.UUID
import kotlin.concurrent.thread
import kotlin.streams.toList

class CrossrefManagementResourceProviderFactory : CrossrefResourceProviderFactory() {

    private val primaryInstance: Boolean = (System.getenv("KEYCLOAK_PRIMARY") ?: "").isNotEmpty()
    private val crossrefSyncSecret: String? = System.getenv("KEYCLOAK_SYNC_CREDENTIALS")
    private val contentSecurityPolicy = System.getenv("REALM_CONTENT_SECURITY_POLICY") ?: ""
    private val realmTestUsers = System.getenv("REALM_TEST_USERS") ?: ""
    private val realmTestUserPassword = System.getenv("REALM_TEST_USER_PASSWORD") ?: ""
    private val realmLoginTheme = System.getenv("REALM_LOGIN_THEME") ?: ""
    private val keyManagerConfig = fetchConfigFromEnv()
    private val logger: Logger = Logger.getLogger(CrossrefManagementResourceProviderFactory::class.java)
    private var initSetupFinished = false
    companion object {
        private val config: CrossrefManagementConfiguration = CrossrefManagementConfiguration(
            System.getenv("APIKEY_SALT") ?: "",
            System.getenv("AUTHENTICATOR_URL")?.let { originalUrl ->
                URI(originalUrl).let {
                    URI(it.scheme, null, it.host, it.port, "/api/v1/keycloak-sync", null, null).toString()
                }
            } ?: "",
            System.getenv("AUTHENTICATOR_INTERNAL_USER").let { user ->
                System.getenv("AUTHENTICATOR_INTERNAL_PASSWORD").let { pwd ->
                    if (user != null && pwd != null && user.isNotEmpty() and pwd.isNotEmpty()) "$user:$pwd" else ""
                }
            }
        )
        const val resourceId = "crossref-management"
    }

    /*
     * Only one instance can make this changes in the DB that will be
     * set as primary via ENV vars
     */
    override fun initializeExtension(session: KeycloakSession) {
        if (primaryInstance) {
            session.realms().getRealmByName(REALM) ?: session.realms().createRealm(REALM)
        }
    }

    /*
     * We need to wait until realm has finished the setup:
     * Realm creation
     * Clients setup
     */
    override fun getResourceProvider(session: KeycloakSession): RealmResourceProvider {
        while (!initSetupFinished) { Thread.sleep(1000) }
        return CrossrefManagementResourceProvider(session, config)
    }

    override fun init(config: Scope) {
        logger.info("Crossref resource management Plugin enabled! [ version = ${this.javaClass.`package`.implementationVersion} ]")
    }

    private fun createOrUpdateTestUsers(realm: RealmModel, session: KeycloakSession) {
        realmTestUsers.split(":").windowed(2, 2).forEach { (userName, role) ->
            realm.getRole(role)?.let { role ->
                session
                    .users()
                    .getUserByEmail(realm, userName) ?: session.users().addUser(realm, userName).also {
                    it.isEnabled = true
                    it.isEmailVerified = true
                    it.email = userName
                    it.grantRole(role)
                    it.credentialManager().also { cm ->
                        cm.createStoredCredential(
                            session
                                .getProvider(PasswordHashProvider::class.java, realm.passwordPolicy.hashAlgorithm)
                                .encodedCredential(realmTestUserPassword, realm.passwordPolicy.hashIterations)
                        )
                    }
                }
            }
        }
    }

    /*
     * This method will be executed every time keycloak restarts, will overwrite pwdsync user password
     * and will createOrUpdate clients
     */
    private fun updateCrossrefRealmConfig(session: KeycloakSession, authzProvider: AuthorizationProvider) {
        RealmManager(session).also { rm ->
            rm.getRealmByName(REALM).also { realm ->
                crossrefSyncSecret?.let { secret ->
                    session.users().getUserByUsername(realm, PWDSYNC_USER)?.also { user ->
                        user.credentialManager().also { cm ->
                            cm.storedCredentialsStream.forEach { cm.removeStoredCredentialById(it.id) }
                            cm.createStoredCredential(
                                session.getProvider(PasswordHashProvider::class.java, realm.passwordPolicy.hashAlgorithm).encodedCredential(secret, realm.passwordPolicy.hashIterations)
                            )
                        }
                        user.requiredActionsStream.forEach { action -> user.removeRequiredAction(action) }
                        user.credentialManager()
                    }
                }
                if (contentSecurityPolicy.isNotEmpty()) {
                    realm.getBrowserSecurityHeaders().toMutableMap().let { secHeaders ->
                        secHeaders["contentSecurityPolicy"] = contentSecurityPolicy
                        realm.browserSecurityHeaders = secHeaders.toMap()
                    }
                }
                if (realmTestUsers.isNotEmpty() && realmTestUserPassword.isNotEmpty()) {
                    createOrUpdateTestUsers(realm, session)
                }
                if (realmLoginTheme.isNotEmpty()) {
                    realm.setLoginTheme(realmLoginTheme)
                }
                logger.info("$REALM realm client setup")
                realmClients.forEach { (clientId, rep) -> createOrUpdateClient(rep, authzProvider, session, keyManagerConfig.values.filter { it["ID"] == clientId }.firstOrNull()) }
            }
        }
    }

    private fun boolOpt(value: String?): Boolean? = value?.let {
        if (setOf("0", "n").contains(value.lowercase())) {
            false
        } else if (setOf("1", "y").contains(value.lowercase())) {
            true
        } else {
            null
        }
    }

    fun createResource(resource: ResourceRepresentation, authorization: AuthorizationProvider, client: ClientModel): Resource? {
        val resourceServer = authorization.storeFactory.resourceServerStore.findByClient(client)

        var owner = resource.owner
        if (owner == null) {
            owner = ResourceOwnerRepresentation()
            owner.id = resourceServer.getClientId()
            resource.owner = owner
        }

        return toModel(resource, resourceServer, authorization)
    }

    fun createPolicy(policy: PolicyRepresentation, authorization: AuthorizationProvider, client: ClientModel): Policy? {
        val policyStore = authorization.storeFactory.policyStore
        val resourceServer = authorization.storeFactory.resourceServerStore.findByClient(client)
        policyStore.findByName(resourceServer, policy.name)?.also { throw Exception("Policy already exists") }

        return policyStore.create(resourceServer, policy)
    }

    fun createPermission(permission: ResourcePermissionRepresentation, authorization: AuthorizationProvider, client: ClientModel): Policy? {
        val policyStore = authorization.storeFactory.policyStore
        val resourceServer = authorization.storeFactory.resourceServerStore.findByClient(client)
        policyStore.findByName(resourceServer, permission.name)?.also { throw Exception("Permission already exists") }

        return policyStore.create(resourceServer, permission)
    }

    /*
     * This method creates a client if it doesn't exist and when it is created (or already existing) allows
     * updating specific parameters passed via env vars.
     */
    fun createOrUpdateClient(rep: ClientRepresentation, authzProvider: AuthorizationProvider, session: KeycloakSession, config: Map<String, String?>?) {
        val realm = authzProvider.realm
        var client = realm.getClientByClientId(rep.clientId)
        val voidGroupId = session.groups().getTopLevelGroupsStream(realm).filter { it.name == "void" }.findFirst().get().id

        if (client == null) {
            logger.info("Creating client ${rep.clientId}")

            client = ClientManager.createClient(session, realm, rep)

            // We only create policies permissions and resources if auth is enabled in the client
            if (rep.authorizationServicesEnabled == true) {
                RepresentationToModel.createResourceServer(client, session, true)
                clientResources[rep.clientId]?.let {
                    setupResourcesPoliciesAndPermissions(client, authzProvider, it, voidGroupId)
                }
            }
        }

        logger.info("Updating client ${rep.clientId}")
        config?.let {
            client.baseUrl = config["HOMEURL"] ?: "http://NOTDEFINED/"
            client.rootUrl = config["ROOTURL"] ?: "http://NOTDEFINED/"
            client.secret = config["SECRET"] ?: UUID.randomUUID().toString()
            config["REDIRECTURIS"]?.let { client.redirectUris = it.split(';').toSet() }
            config["WEBORIGINS"]?.let { client.webOrigins = it.split(';').toSet() }
            boolOpt(config["BACKCHANNEL_LOGOUT_SESSION_REQUIRED"]).let {
                client.setAttribute(BACKCHANNEL_LOGOUT_SESSION_REQUIRED, it.toString())
            }
            config["BACKCHANNEL_LOGOUT_URL"]?.let {
                client.setAttribute(BACKCHANNEL_LOGOUT_URL, it)
            }
            boolOpt(config["USE_REFRESH_TOKEN_FOR_CLIENT_CREDENTIALS_GRANT"])?.let {
                client.setAttribute(USE_REFRESH_TOKEN_FOR_CLIENT_CREDENTIALS_GRANT, it.toString())
            }
        }
    }

    private fun setupResourcesPoliciesAndPermissions(client: ClientModel, authorization: AuthorizationProvider, resources: List<Triple<ResourceRepresentation, PolicyRepresentation, ResourcePermissionRepresentation>>, voidGroupId: String) {
        clientResources[client.clientId]?.forEach { (resource, policy, permission) ->
            createResource(resource, authorization, client)?.let { newResource ->
                createPolicy(
                    policy.apply {
                        config = mapOf(
                            "groups" to mapper.writeValueAsString(
                                listOf(
                                    mapOf<String, Any>(
                                        "id" to voidGroupId,
                                        "extendChildren" to false
                                    )
                                )
                            )
                        )
                    },
                    authorization,
                    client
                )?.let { newPolicy ->
                    createPermission(
                        permission.also {
                            it.policies = setOf(newPolicy.id)
                            it.resources = setOf(newResource.id)
                        },
                        authorization,
                        client
                    )
                }
            }
        }
    }

    /*
     * This method is executed only once when the realm does not exist
     * if some of the following parameters need to be made customizable better moving
     * them to the function update CrossrefRealmConfig as it is executed everytime
     * keycloak restarts.
     */
    private fun createCrossrefRealm(session: KeycloakSession) {
        logger.info("Realm not found, creating realm $REALM")
        RealmManager(session).createRealm(REALM).also { realm ->
            realm.name = REALM
            realm.displayName = REALM
            realm.displayNameHtml = REALM
            realm.isEnabled = true
            realm.isRegistrationEmailAsUsername = true
            realm.defaultSignatureAlgorithm = Constants.DEFAULT_SIGNATURE_ALGORITHM
            realm.ssoSessionIdleTimeout = 1800
            realm.accessTokenLifespan = 60
            realm.accessTokenLifespanForImplicitFlow = Constants.DEFAULT_ACCESS_TOKEN_LIFESPAN_FOR_IMPLICIT_FLOW_TIMEOUT
            realm.ssoSessionMaxLifespan = 36000
            realm.offlineSessionIdleTimeout = Constants.DEFAULT_OFFLINE_SESSION_IDLE_TIMEOUT
            realm.isOfflineSessionMaxLifespanEnabled = false
            realm.offlineSessionMaxLifespan = Constants.DEFAULT_OFFLINE_SESSION_MAX_LIFESPAN
            realm.accessCodeLifespan = 60
            realm.accessCodeLifespanUserAction = 300
            realm.accessCodeLifespanLogin = 1800
            realm.sslRequired = SslRequired.EXTERNAL
            realm.isRegistrationAllowed = false
            realm.isAdminEventsEnabled = true
            realm.isEventsEnabled = true
            realm.eventsExpiration = 432000
            realm.accessTokenLifespan = 300
            realm.isAdminEventsDetailsEnabled = true
            realm.addRequiredCredential(CredentialRepresentation.PASSWORD)

            DefaultKeyProviders.createProviders(realm)
            if (realm.topLevelGroupsStream.filter { it.name == ORGS_BASE_GROUP }.findFirst().isEmpty) {
                logger.info("Creating base group $ORGS_BASE_GROUP")
                realm.createGroup(ORGS_BASE_GROUP)
            }
            if (realm.topLevelGroupsStream.filter { it.name == VOID_GROUP }.findFirst().isEmpty) {
                // The reason we create a /void group is to assign it to the group policies. Group policies must
                // have at least one group assigned, they cannot be empty
                logger.info("Creating void group")
                realm.createGroup(VOID_GROUP)
            }

            // Setting the password policy
            realm.passwordPolicy = parse(session, "passwordHistory(3) and upperCase(1) and lowerCase(1) and digits(1) and length(8) and specialChars(1) and hashIterations(150000) and hashAlgorithm(pbkdf2-sha512)")

            // Creating base roles
            realm.addRole(Role.STAFF.roleName).also { staffRole ->
                staffRole.addCompositeRole(realm.addRole(Role.USER.roleName))
                staffRole.addCompositeRole(realm.addRole(Role.APIKEY.roleName))
                staffRole.addCompositeRole(
                    realm.addRole(Role.PWDSYNC.roleName).also { role ->
                        session.users().addUser(realm, PWDSYNC_USER).also {
                            it.grantRole(role)
                            it.isEnabled = true
                            it.isEmailVerified = true
                        }
                    }
                )
            }
        }
    }

    /*
     * Our postInit function spawns a thread that will set up the realm and clients. Realm will be created only once
     * clients and some realm config will be executed everytime keycloak restarts, allowing changing basic config
     * using ENV vars.
     *
     * The reason we spawn a thread is that a blocking mechanism doesn't work, we need to wait for keycloak to start
     * and load basic capabilities, and that doesn't happen until extensions execute postInit functions
     *
     * postInit thread will finish setting initSetupFinished to true, and allowing the extension functioning normally
     */
    override fun postInit(factory: KeycloakSessionFactory) {
        thread {
            while (true) {
                // We set a delay for an infinite loop waiting for table REALM to be created
                Thread.sleep(3000)
                val session = factory.create()

                session.transactionManager.begin()

                var realms: Set<String> = setOf()

                try {
                    realms = session.realms().realmsStream.map { it.name }.toList().toSet()
                } catch (_: Exception) {
                    // REALM table does not exist
                }

                if (realms.contains("master")) {
                    Thread.sleep(3000)
                    if (!realms.contains(REALM)) {
                        createCrossrefRealm(session)
                    }

                    try {
                        updateCrossrefRealmConfig(
                            session,
                            (factory.getProviderFactory(AuthorizationProvider::class.java) as AuthorizationProviderFactory).create(session, session.realms().getRealmByName(REALM))
                        )
                    } catch (e: Exception) {
                        StringWriter().also { sw ->
                            PrintWriter(sw).also { pw ->
                                e.printStackTrace(pw)
                                logger.error(sw.toString())
                            }
                        }
                    }
                    initSetupFinished = true
                    session.transactionManager.commit()
                    return@thread
                }

                session.transactionManager.rollback()
            }
        }
    }
    override fun close() {}

    override fun getId(): String {
        return resourceId
    }

    /*
     * Environment variables will follow the following nomenclature:
     * CL_NAME_VARIABLE_OF_INTEREST = 10      where CL means CLIENT
     *
     * for example:
     * CL_RESTAPI_SESSION_TTL = 10
     *
     * will be accessed config["RESTAPI"]["SESSION_TTL"]
     */
    private fun fetchConfigFromEnv() = System.getenv()
        .filter { it.key.matches("^CL_[^_]+_.*$".toRegex()) }
        .map { (k, value) -> k.split("_", limit = 3).let { (_, client, v) -> listOf(client, v, value) } }
        .groupBy({ it.first() }, { Pair(it.get(1), it.get(2)) })
        .mapValues { it.value.toMap() }
}
