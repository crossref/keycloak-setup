package org.crossref.keycloak.management.resources

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import io.swagger.v3.core.util.Json
import io.swagger.v3.jaxrs2.Reader
import io.swagger.v3.oas.annotations.Hidden
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.models.OpenAPI
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.ForbiddenException
import jakarta.ws.rs.HeaderParam
import jakarta.ws.rs.NotAuthorizedException
import jakarta.ws.rs.OPTIONS
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.QueryParam
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.crossref.keycloak.management.APIKey
import org.crossref.keycloak.management.APIKeyResponse
import org.crossref.keycloak.management.CrossrefManagementConfiguration
import org.crossref.keycloak.management.ErrorResponse
import org.crossref.keycloak.management.Organization
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.management.Subscription
import org.crossref.keycloak.management.User
import org.crossref.keycloak.management.UserSyncMessage
import org.crossref.keycloak.management.services.APIKeyManagerService
import org.crossref.keycloak.management.services.OrgManagerService
import org.crossref.keycloak.management.services.UserManagerService
import org.crossref.keycloak.util.Constants
import org.crossref.keycloak.util.Constants.Crossref.Companion.CROSSREF_UI_CLIENT
import org.keycloak.authorization.util.Tokens
import org.keycloak.events.EventBuilder
import org.keycloak.events.EventStoreProvider
import org.keycloak.events.EventType
import org.keycloak.models.Constants.DEFAULT_MAX_RESULTS
import org.keycloak.models.KeycloakSession
import org.keycloak.representations.AccessToken
import org.keycloak.representations.info.SystemInfoRepresentation
import java.text.SimpleDateFormat
import java.util.UUID
import kotlin.streams.toList

@OpenAPIDefinition(
    info = Info(
        title = "Crossref resource management API",
        description = "This API will allow to manage Crossref resources within the keycloak framework",
        version = "1.0",
    ),
)
class CrossrefManagementResource(
    private val session: KeycloakSession,
    private val config: CrossrefManagementConfiguration,
) {
    companion object {
        var openApiJsonDef: String? = null
    }

    val eventBuilder = EventBuilder(session.context.realm, session, session.context.connection)
        .event(EventType.EXECUTE_ACTIONS)
        .detail("origin", this.javaClass.name).let {
            try {
                getAccessToken().let { at -> if (at != null) it.user(at.subject) else it }
            } catch (e: Exception) { it }
        }

    private fun getAccessToken() = Tokens.getAccessToken(session)

    private fun sessionHasRole(accessToken: AccessToken?, role: Role) = accessToken?.let { return it.realmAccess.isUserInRole(role.roleName) } ?: false

    private fun requiresAnyRole(vararg roles: Role) {
        with(getAccessToken() ?: throw NotAuthorizedException("Not authorized")) {
            if (!roles.any {
                sessionHasRole(this, it)
            }
            ) {
                throw ForbiddenException("Forbidden")
            }
        }
    }

    private fun disallowAnyRole(vararg roles: Role) {
        with(getAccessToken() ?: throw NotAuthorizedException("Not authorized")) {
            if (roles.any {
                sessionHasRole(this, it)
            }
            ) {
                throw ForbiddenException("Forbidden")
            }
        }
    }
    private fun ensureUserBelongsToOrg(memberId: Long) {
        getAccessToken().let { accessToken ->
            if (!sessionHasRole(accessToken, Role.STAFF) && sessionHasRole(accessToken, Role.USER)) {
                session.users()
                    .getUserById(session.context.realm, accessToken.subject)
                    .groupsStream
                    .filter {
                        (it.attributes?.getOrDefault("memberId", listOf(""))?.get(0) ?: "") == memberId.toString()
                    }
                    .toList().isEmpty() && throw ForbiddenException()
            }
        }
    }

    private val orgManagerService: OrgManagerService by lazy { OrgManagerService(session) }
    private val userManagerService: UserManagerService by lazy { UserManagerService(session, config) }
    private val keyManagerService: APIKeyManagerService by lazy { APIKeyManagerService(session, config) }

    /*
     * HeartBeat
     */
    @jakarta.ws.rs.GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/heartbeat")
    @Hidden
    fun heartbeat(): String {
        val objectMapper = ObjectMapper()
        return objectMapper.writeValueAsString(
            mapOf(
                "status" to "ok",
                "keycloak_version" to SystemInfoRepresentation.create(session.keycloakSessionFactory.serverStartupTimestamp).version,
                "keyManager_version" to CrossrefManagementResource.Companion::class.java.`package`.implementationVersion,
            ),
        )
    }

    /*
     *
     * API Specs
     * As keycloak does not use spring, we cannot use SpringFox or other tools.
     * There are some unmaintained projects that do the same on JAX-RS, but they
     * did not work correctly. Manual generation of API's is fairly simple using swagger
     * as all the other tools do, I've just removed introspection and directly
     * specified the classes I want to generate documentation for.
     *
     */

    @jakarta.ws.rs.GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/openapi")
    @Hidden
    fun api(): String {
        if (openApiJsonDef == null) {
            val reader = Reader(OpenAPI())
            val swagger: OpenAPI = reader.read(this.javaClass)
            val objectMapper = ObjectMapper()
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
            openApiJsonDef = Json.pretty(swagger)
        }
        return openApiJsonDef as String
    }

    /*
     *
     * Organization management
     *
     */

    @Operation(summary = "Get organizations", description = "Returns all the organizations. If the parameter *name* is provided, results will be filtered by name (containing *name* as a substring).")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Found organizations",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = Organization::class))))) ],
            ),
        ],
    )
    @jakarta.ws.rs.GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/organization")
    fun findOrgs(
        @QueryParam("name") name: String? = null,
        @QueryParam("page") page: Int? = null,
        @QueryParam("pagesize") pagesize: Int? = null,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        return Response.ok().entity(
            orgManagerService.findOrgs(name ?: "", page, pagesize),
        ).setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Get organization by member ID", description = "Returns the organization with matching the member ID provided")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Found organization",
                content = [ (Content(mediaType = "application/json", schema = Schema(implementation = Organization::class))) ],
            ),
            ApiResponse(
                responseCode = "404",
                description = "Organization not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/organization/{memberId}")
    fun getOrg(
        @PathParam("memberId") memberId: Long,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        return Response.ok().entity(
            orgManagerService.findOrgByMemberId(memberId),
        ).setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Update an Organization", description = "Creates or updates an organization")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Organization updated",
                content = [ (Content(mediaType = "application/json", schema = Schema(implementation = Organization::class))) ],
            ),
            ApiResponse(
                responseCode = "201",
                description = "Organization created",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = Organization::class))))) ],
            ),
            ApiResponse(
                responseCode = "409",
                description = "Update/creation failed because of a name/memberId conflict",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/organization")
    fun updateOrg(
        organization: Organization,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)

        orgManagerService.updateOrg(organization).let {
            return if (it.orgId != organization.orgId) {
                eventBuilder
                    .detail("operation", Constants.ResourceResponseType.ORG_CREATED.name)
                    .detail("name", organization.name)
                    .detail("memberId", organization.memberId.toString())
                    .detail("orgId", it.orgId.toString()).success()
                Response.status(Constants.ResourceResponseType.ORG_CREATED.code).entity(it).setUpCORS(originHeader, session).build()
            } else {
                eventBuilder
                    .detail("operation", Constants.ResourceResponseType.ORG_UPDATED.name)
                    .detail("name", organization.name)
                    .detail("memberId", organization.memberId.toString())
                    .detail("orgId", it.orgId.toString()).success()
                Response.status(Constants.ResourceResponseType.ORG_UPDATED.code).entity(it).setUpCORS(originHeader, session).build()
            }
        }
    }

    @Operation(summary = "Create an Organization", description = "Creates an organization")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "201",
                description = "Organization created",
                content = [ (Content(mediaType = "application/json", schema = Schema(implementation = Organization::class))) ],
            ),
            ApiResponse(
                responseCode = "409",
                description = "Update/creation failed because of a name/memberId conflict",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "400",
                description = "Organization name contains invalid characters",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/organization")
    fun addOrg(
        org: Organization,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)

        return Response.status(Constants.ResourceResponseType.ORG_CREATED.code)
            .entity(
                orgManagerService.addOrg(org).also {
                    eventBuilder
                        .detail("operation", Constants.ResourceResponseType.ORG_CREATED.name)
                        .detail("name", it.name)
                        .detail("memberId", it.memberId.toString())
                        .detail("orgId", it.orgId.toString()).success()
                },
            )
            .setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "List organization users", description = "List users within an organization")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Found users",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = User::class))))) ],
            ),
            ApiResponse(
                responseCode = "404",
                description = "Organization not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/organization/{memberId}/users")
    fun listUsersInOrg(
        @PathParam("memberId") memberId: Long,
        @QueryParam("page") page: Int?,
        @QueryParam("pagesize") pagesize: Int?,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.USER, Role.STAFF)
        ensureUserBelongsToOrg(memberId)

        return Response.ok()
            .entity(orgManagerService.getUsersFromOrg(memberId, page, pagesize))
            .setUpCORS(originHeader, session).build()
    }

    /*
     *
     * User management
     *
     */

    @Operation(summary = "User Sync", description = "Syncs user with authenticator")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "204",
                description = "User synced",
            ),
            ApiResponse(
                responseCode = "400",
                description = "Bad request",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.POST
    @Hidden
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/user/sync")
    fun userSync(
        request: UserSyncMessage,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.PWDSYNC)
        userManagerService.syncUser(request, eventBuilder.detail("operation", "usersync").detail("action", request.operation.name).detail("username", request.username))
        return Response.noContent().setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Get users", description = "Gets all list of users")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "List of Users",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = User::class))))) ],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/user")
    fun getUsers(
        @QueryParam("email") email: String?,
        @QueryParam("page") page: Int?,
        @QueryParam("pagesize") pagesize: Int? = null,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)
        return Response.ok().entity(
            userManagerService.getUsers(email ?: "", page, pagesize),
        ).setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Create a User", description = "Creates a normal user")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "201",
                description = "User created",
                content = [ (Content(mediaType = "application/json", schema = Schema(implementation = User::class))) ],
            ),
            ApiResponse(
                responseCode = "409",
                description = "Creation failed because of a username conflict",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/user")
    fun addUser(
        user: User,
        @QueryParam("role") role: Role?,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)
        return Response.status(Constants.ResourceResponseType.USER_CREATED.code)
            .entity(
                userManagerService.addUser(user, role ?: Role.USER).also {
                    eventBuilder
                        .detail("operation", Constants.ResourceResponseType.USER_CREATED.name)
                        .detail("username", user.username)
                        .detail("role", role.toString())
                        .success()
                },
            )
            .setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Delete a User", description = "Deletes a user")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "204",
                description = "User deleted",
            ),
            ApiResponse(
                responseCode = "404",
                description = "Deletion failed because user is not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/user/{userId}")
    fun deleteUser(
        @PathParam("userId") userId: UUID,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)
        userManagerService.delUser(userId).also {
            eventBuilder
                .detail("userId", userId.toString())
                .detail("operation", Constants.ResourceResponseType.USER_DELETED.name)
                .success()
        }
        return Response.status(Constants.ResourceResponseType.USER_DELETED.code).setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Add user to organization", description = "Adds a normal user to an organization")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "204",
                description = "User added to organization",
            ),
            ApiResponse(
                responseCode = "404",
                description = "User or organization not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/organization/{memberId}/user/{userId}")
    fun addUserToOrg(
        @PathParam("memberId") memberId: Long,
        @PathParam("userId") userId: UUID,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)
        orgManagerService.addUserToOrg(memberId, userId).also {
            eventBuilder
                .detail("operation", Constants.ResourceResponseType.USER_ASSIGNED.name)
                .detail("userId", userId.toString())
                .detail("memberId", memberId.toString())
                .success()
        }
        return Response.status(Constants.ResourceResponseType.USER_ASSIGNED.code)
            .setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Remove user from organization", description = "removes a normal user from an organization")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "204",
                description = "User removed from organization",
            ),
            ApiResponse(
                responseCode = "404",
                description = "User or organization not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/organization/{memberId}/user/{userId}")
    fun removeUserFromOrg(
        @PathParam("memberId") memberId: Long,
        @PathParam("userId") userId: UUID,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)
        orgManagerService.removeUserFromOrg(memberId, userId).also {
            eventBuilder
                .detail("operation", Constants.ResourceResponseType.USER_UNASSIGNED.name)
                .detail("userId", userId.toString())
                .detail("memberId", memberId.toString())
                .success()
        }
        return Response.status(Constants.ResourceResponseType.USER_UNASSIGNED.code)
            .setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "List user organizations", description = "Lists organizations a given user belongs to")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Found organizations",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = Organization::class))))) ],
            ),
            ApiResponse(
                responseCode = "404",
                description = "User not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/user/{userId}/organizations")
    fun listUserOrgs(
        @PathParam("userId") userId: UUID,
        @QueryParam("name") name: String?,
        @QueryParam("page") page: Int?,
        @QueryParam("pagesize") pagesize: Int?,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.USER, Role.STAFF)

        getAccessToken().subject != userId.toString() &&
            !sessionHasRole(getAccessToken(), Role.STAFF) &&
            throw ForbiddenException()

        return Response.ok()
            .entity(userManagerService.getOrgs(userId, name, page, pagesize))
            .setUpCORS(originHeader, session).build()
    }

    /*
     *
     * APIKey management
     *
     */

    @Operation(summary = "Create an API Key", description = "Creates an API Key")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "201",
                description = "API Key created",
                content = [ (Content(mediaType = "application/json", schema = Schema(implementation = APIKeyResponse::class))) ],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "400",
                description = "Description is too long",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/organization/{memberId}/key")
    fun addAPIKey(
        key: APIKey,
        @PathParam("memberId") memberId: Long,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.USER)
        disallowAnyRole(Role.STAFF)

        ensureUserBelongsToOrg(memberId)

        return Response.status(Constants.ResourceResponseType.KEY_CREATED.code)
            .entity(
                keyManagerService.addKey(
                    APIKey(
                        key.description,
                        UUID.fromString(getAccessToken()?.subject),
                    ),
                    orgManagerService.findOrgByMemberId(memberId).orgId,
                ).also {
                    eventBuilder
                        .detail("operation", Constants.ResourceResponseType.KEY_CREATED.name)
                        .detail("keyName", it.keyObject.name)
                        .detail("memberId", memberId.toString())
                        .success()
                },
            )
            .setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "List API Keys", description = "Lists API Key for a given organization")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Found API Keys",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = APIKey::class))))) ],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.TEXT_PLAIN)
    @Path("/organization/{memberId}/key")
    fun listAPIKeys(
        @PathParam("memberId") memberId: Long,
        @QueryParam("page") page: Int? = null,
        @QueryParam("pagesize") pagesize: Int? = null,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.USER, Role.STAFF)
        ensureUserBelongsToOrg(memberId)

        return Response.ok()
            .entity(
                keyManagerService.listKeys(
                    orgManagerService.findOrgByMemberId(memberId).orgId,
                    page,
                    pagesize,
                ),
            )
            .setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Disable API Key", description = "Disables API Key")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "API Key disabled",
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "404",
                description = "API Key not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.PUT
    @Path("/key/{keyId}/disable")
    fun disableAPIKey(
        @PathParam("keyId") keyId: UUID,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)
        keyManagerService.setKeyEnabled(keyId, false).also {
            eventBuilder
                .detail("operation", Constants.ResourceResponseType.KEY_DISABLED.name)
                .detail("keyId", keyId.toString()).success()
        }

        return Response.status(Constants.ResourceResponseType.KEY_DISABLED.code)
            .entity("")
            .setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Enable API Key", description = "Enables API Key")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "API Key Enabled",
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "404",
                description = "API Key not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.PUT
    @Path("/key/{keyId}/enable")
    fun enableAPIKey(
        @PathParam("keyId") keyId: UUID,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)
        keyManagerService.setKeyEnabled(keyId, true).also {
            eventBuilder
                .detail("operation", Constants.ResourceResponseType.KEY_ENABLED.name)
                .detail("keyId", keyId.toString()).success()
        }
        return Response.status(Constants.ResourceResponseType.KEY_ENABLED.code)
            .entity("")
            .setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Delete API Key", description = "Deletes API Key")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "204",
                description = "API Key Deleted",
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "404",
                description = "API Key not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.DELETE
    @Path("/key/{keyId}")
    fun deleteAPIKey(
        @PathParam("keyId") keyId: UUID,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.USER)
        disallowAnyRole(Role.STAFF)

        val memberId = keyManagerService.getApiKeyOrganization(keyId).memberId
        ensureUserBelongsToOrg(memberId)
        keyManagerService.deleteKey(keyId).also {
            eventBuilder
                .detail("operation", Constants.ResourceResponseType.KEY_DELETED.name)
                .detail("keyId", keyId.toString())
                .detail("memberId", memberId.toString()).success()
        }

        return Response.status(Constants.ResourceResponseType.KEY_DELETED.code)
            .setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Update API Key", description = "Updates API Key, only description can be updated")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "API Key description updated",
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "404",
                description = "API Key not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "400",
                description = "Description is too long",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/key")
    fun updateAPIKey(
        key: APIKey,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.USER)
        disallowAnyRole(Role.STAFF)

        ensureUserBelongsToOrg(keyManagerService.getApiKeyOrganization(key.keyId).memberId)
        return Response.status(Constants.ResourceResponseType.KEY_UPDATED.code)
            .entity(keyManagerService.updateKey(key))
            .setUpCORS(originHeader, session).build()
    }

    /*
     *
     * SUBSCRIPTIONS
     *
     */

    @Operation(summary = "Add subscription to an org", description = "adds a subscription to an Org")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "204",
                description = "subscription updated",
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "404",
                description = "Organization not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.PUT
    @Path("/organization/{memberId}/subscription/{subscription}")
    fun addOrgSubscription(
        @PathParam("memberId") memberId: Long,
        @PathParam("subscription") subscription: Subscription,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)
        orgManagerService.addOrgSubscription(memberId, subscription).also {
            eventBuilder
                .detail("operation", Constants.ResourceResponseType.SUBSCRIPTION_ADDED.name)
                .detail("memberId", memberId.toString())
                .detail("subscription", subscription.name)
                .success()
        }
        return Response.status(Constants.ResourceResponseType.SUBSCRIPTION_ADDED.code).setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Remove Org subscription", description = "Removes an Org subscription")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "204",
                description = "subscription updated",
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "404",
                description = "Organization not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.DELETE
    @Path("/organization/{memberId}/subscription/{subscription}")
    fun removeOrgSubscription(
        @PathParam("memberId") memberId: Long,
        @PathParam("subscription") subscription: Subscription,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF)
        orgManagerService.delOrgSubscription(memberId, subscription).also {
            eventBuilder
                .detail("operation", Constants.ResourceResponseType.SUBSCRIPTION_REMOVED.name)
                .detail("memberId", memberId.toString())
                .detail("subscription", subscription.name)
                .success()
        }
        return Response.status(Constants.ResourceResponseType.SUBSCRIPTION_REMOVED.code).setUpCORS(originHeader, session).build()
    }

    @Operation(summary = "Get org subscriptions", description = "Gets organization subscriptions")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Found subscriptions",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = Subscription::class))))) ],
            ),
            ApiResponse(
                responseCode = "401",
                description = "Operation not authorized",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "403",
                description = "Operation forbidden",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
            ApiResponse(
                responseCode = "404",
                description = "Organization not found",
                content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))],
            ),
        ],
    )
    @jakarta.ws.rs.GET
    @Path("/organization/{memberId}/subscription")
    fun getOrgSubscription(
        @PathParam("memberId") memberId: Long,
        @HeaderParam("origin") originHeader: String?,
    ): Response {
        requiresAnyRole(Role.STAFF, Role.USER)
        ensureUserBelongsToOrg(memberId)
        return Response
            .ok()
            .entity(orgManagerService.getOrgSubscriptions(memberId))
            .setUpCORS(originHeader, session).build()
    }

    @Hidden
    @jakarta.ws.rs.GET
    @Path("/auditlog")
    fun auditLog(
        @QueryParam("search") searchString: String?,
        @HeaderParam("origin") originHeader: String?,
    ): Response? {
        requiresAnyRole(Role.STAFF)

        val eventStore = session.getProvider(EventStoreProvider::class.java)
        val query = eventStore.createQuery().realm(session.context.realm.id)

        query.type(EventType.EXECUTE_ACTIONS)
        query.maxResults(DEFAULT_MAX_RESULTS)

        return Response.ok()
            .entity(
                query.resultStream.map {
                    mapOf<String, Any>(
                        "timestamp" to SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(it.time),
                        "details" to it.details,
                        "userId" to it.userId,
                    )
                }.filter { it.toString().contains(searchString ?: "") }.limit(1000),
            )
            .setUpCORS(originHeader, session).build()
    }

    // See: https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request
    @Hidden
    @Path("{any:.*}")
    @OPTIONS
    fun preflight(
        @HeaderParam("origin") originHeader: String?,
    ): Response? {
        return Response
            .noContent()
            .header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
            .header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization")
            .setUpCORS(originHeader, session)
            .build()
    }
}

private fun Response.ResponseBuilder.setUpCORS(originHeader: String?, session: KeycloakSession): Response.ResponseBuilder {
    session.context.realm.getClientByClientId(CROSSREF_UI_CLIENT).webOrigins.also { webOrigins ->
        if (webOrigins.contains("*")) {
            header("Access-Control-Allow-Origin", "*")
        } else {
            webOrigins.firstOrNull { it.startsWith(originHeader ?: "") }?.let { header("Access-Control-Allow-Origin", "*") }
        }
    }
    return this
}
