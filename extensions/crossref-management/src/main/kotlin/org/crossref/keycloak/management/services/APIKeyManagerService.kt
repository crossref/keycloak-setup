package org.crossref.keycloak.management.services

import org.apache.commons.codec.binary.Base32
import org.crossref.keycloak.management.APIKey
import org.crossref.keycloak.management.APIKeyResponse
import org.crossref.keycloak.management.CrossrefManagementConfiguration
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.management.fromGroupModel
import org.crossref.keycloak.management.fromUserModelToKey
import org.crossref.keycloak.management.utils.KeyDescriptionTooLong
import org.crossref.keycloak.management.utils.KeyNotFoundException
import org.crossref.keycloak.management.utils.OrgNotFoundException
import org.crossref.keycloak.management.utils.PaginatedResource
import org.crossref.keycloak.util.Constants.Crossref.Companion.MAX_KEY_DESCRIPTION_LENGTH
import org.keycloak.models.KeycloakSession
import java.math.BigInteger
import java.security.MessageDigest
import java.util.UUID
import kotlin.math.min
import kotlin.streams.toList

class APIKeyManagerService(
    private val session: KeycloakSession,
    private val config: CrossrefManagementConfiguration
) {
    private fun generateApiKey(): String {
        var key = ""
        for (i in 1..3) {
            key += Base32()
                .encodeAsString(
                    UUID
                        .randomUUID()
                        .toString()
                        .replace("-", "")
                        .toBigInteger(16).toByteArray()
                )
                .replace("=", "")
        }

        return "x${key.slice(0..19)}r${key.slice(20..39)}e${key.slice(40..59)}f".lowercase()
    }

    fun getApiKeyOrganization(keyId: UUID) = fromGroupModel(
        (session.users().getUserById(session.context.realm, keyId.toString()) ?: throw KeyNotFoundException())
            .groupsStream
            .findFirst()
            .get()
    )

    fun addKey(key: APIKey, orgId: UUID): APIKeyResponse {
        if (key.description.length > MAX_KEY_DESCRIPTION_LENGTH) {
            throw KeyDescriptionTooLong()
        }

        val group = session.context.realm.getGroupById(orgId.toString()) ?: throw OrgNotFoundException()

        val secretKey = generateApiKey()

        session.users().addUser(session.context.realm, key.keyId.toString(), key.name, false, false).let {
            val sha256 = MessageDigest.getInstance("SHA-256") ?: throw InstantiationException("Could not instantiate docker key identifier, no SHA-256 algorithm available.")
            it.email = "${key.name}"
            it.isEnabled = true
            it.setAttribute("description", listOf(key.description))
            it.setAttribute("issuedBy", listOf(key.issuedBy.toString()))
            it.isEmailVerified = true
            it.setAttribute(
                "api-key",
                listOf(
                    BigInteger(1, sha256.digest((config.apiKeySalt + secretKey).toByteArray()))
                        .toString(16).let { hexHash ->
                            if (hexHash.length < 64) {
                                "0".repeat(64 - hexHash.length) + hexHash
                            } else {
                                hexHash
                            }
                        }
                        .lowercase()
                )
            )
            it.grantRole(session.context.realm.getRole(Role.APIKEY.roleName))
            it.joinGroup(group)
            return APIKeyResponse(fromUserModelToKey(it), secretKey)
        }
    }

    fun listKeys(orgId: UUID, page: Int?, pageSize: Int?) = PaginatedResource<APIKey>(
        { _page: Int, _pageSize: Int ->
            session.users().getGroupMembersStream(
                session.context.realm,
                session.context.realm.getGroupById(orgId.toString())
                    ?: throw OrgNotFoundException()
            ).filter {
                it.email.startsWith("apikey-")
            }.map { fromUserModelToKey(it) }.toList().let {
                it.subList(min(_page * _pageSize, it.size), min((_page + 1) * _pageSize, it.size))
            }
        },
        page,
        pageSize
    )

    fun setKeyEnabled(keyId: UUID, enabled: Boolean) {
        (session.users().getUserById(session.context.realm, keyId.toString()) ?: throw KeyNotFoundException()).isEnabled =
            enabled
    }

    fun deleteKey(keyId: UUID) {
        (session.users().getUserById(session.context.realm, keyId.toString()) ?: throw KeyNotFoundException()).let {
            session.users().removeUser(session.context.realm, it)
        }
    }

    fun updateKey(key: APIKey): APIKey {
        if (key.description.length > 255) {
            throw KeyDescriptionTooLong()
        }

        (session.users().getUserById(session.context.realm, key.keyId.toString()) ?: throw KeyNotFoundException()).let {
            it.setAttribute("description", listOf(key.description))
            return fromUserModelToKey(it)
        }
    }
}
