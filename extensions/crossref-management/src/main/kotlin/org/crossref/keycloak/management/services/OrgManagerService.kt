package org.crossref.keycloak.management.services

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.type.TypeReference
import org.crossref.keycloak.management.Organization
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.management.Subscription
import org.crossref.keycloak.management.User
import org.crossref.keycloak.management.fromGroupEntity
import org.crossref.keycloak.management.fromGroupModel
import org.crossref.keycloak.management.fromUserModel
import org.crossref.keycloak.management.utils.InvalidOrgNameException
import org.crossref.keycloak.management.utils.OrgAlreadyExistsException
import org.crossref.keycloak.management.utils.OrgNotFoundException
import org.crossref.keycloak.management.utils.PaginatedResource
import org.crossref.keycloak.management.utils.UserNotFoundException
import org.crossref.keycloak.util.Constants
import org.crossref.keycloak.util.Constants.Crossref.Companion.ORG_NAME_PATTERN
import org.keycloak.authorization.AuthorizationProvider
import org.keycloak.authorization.model.Policy
import org.keycloak.connections.jpa.JpaConnectionProvider
import org.keycloak.models.GroupModel
import org.keycloak.models.KeycloakSession
import org.keycloak.models.ModelDuplicateException
import org.keycloak.models.jpa.entities.GroupEntity
import org.keycloak.models.utils.RepresentationToModel
import org.keycloak.representations.idm.authorization.GroupPolicyRepresentation
import org.keycloak.representations.idm.authorization.GroupPolicyRepresentation.GroupDefinition
import org.keycloak.util.JsonSerialization.mapper
import java.util.UUID
import kotlin.streams.toList

class OrgManagerService(
    private val session: KeycloakSession
) {

    data class PolicyGroup(
        @JsonProperty("id") val id: String,
        @JsonProperty("extendChildren") val extendChildren: Boolean
    )

    companion object {
        private const val ORGANIZATIONS_ROOT_NAME = "organizations"
        private var ORGANIZATIONS_ROOT_ID: String? = null
    }

    private fun getRootOrgId(): String {
        if (ORGANIZATIONS_ROOT_ID == null) {
            val group =
                session.context.realm.topLevelGroupsStream.filter { it.name == ORGANIZATIONS_ROOT_NAME }.findFirst()
            if (group.isEmpty) {
                throw Exception("Top level [$ORGANIZATIONS_ROOT_NAME] group has not been created")
            }
            ORGANIZATIONS_ROOT_ID = group.get().id
        }

        return ORGANIZATIONS_ROOT_ID as String
    }

    fun findOrgByMemberId(memberId: Long): Organization = fromGroupModel(getGroupByMemberId(memberId))

    private fun findOrgByName(name: String): Organization =
        (session.getProvider(JpaConnectionProvider::class.java) as JpaConnectionProvider)
            .entityManager
            .createQuery(
                "select g from GroupEntity g where LOWER(g.name) = :name and g.parentId='${getRootOrgId()}'",
                GroupEntity::class.java
            )
            .setParameter("name", name.lowercase())
            .resultStream
            .map { fromGroupEntity(it) }
            .toList().let {
                it.isEmpty() && throw OrgNotFoundException()
                return it.first()
            }

    fun findOrgs(name: String, page: Int? = null, pageSize: Int? = null): PaginatedResource<Organization> =
        PaginatedResource(
            { _page: Int, _pageSize: Int ->
                if (name.toIntOrNull() != null) {
                    try {
                        listOf(fromGroupModel(getGroupByMemberId(name.toLong())))
                    } catch (e: OrgNotFoundException) {
                        emptyList()
                    }
                } else {
                    (session.getProvider(JpaConnectionProvider::class.java) as JpaConnectionProvider)
                        .entityManager.let { entityManager ->
                            if (name.isEmpty()) {
                                entityManager.createQuery(
                                    "select g from GroupEntity g where g.parentId='${getRootOrgId()}'",
                                    GroupEntity::class.java
                                )
                            } else {
                                entityManager.createQuery(
                                    "select g from GroupEntity g where LOWER(g.name) LIKE :name and g.parentId='${getRootOrgId()}'",
                                    GroupEntity::class.java
                                ).also { it.setParameter("name", "%${name.lowercase()}%") }
                            }
                        }.also {
                            it.firstResult = _page * _pageSize
                            it.setMaxResults(_pageSize)
                        }
                        .resultStream
                        .map { fromGroupEntity(it) }
                        .toList()
                }
            },
            page,
            pageSize
        )

    fun addOrg(organization: Organization): Organization {
        ORG_NAME_PATTERN.toRegex().matches(organization.name) || throw InvalidOrgNameException()

        try {
            getGroupByMemberId(organization.memberId)
            throw OrgAlreadyExistsException(
                organization.name,
                organization.memberId
            ) // We ignore OrgNotFoundException as it will be thrown by keycloak libraries,
            // and it is a requirement for this use case
        } catch (e: OrgNotFoundException) {}

        val grp: GroupModel = try {
            session.context.realm.createGroup(
                organization.name,
                session.context.realm.getGroupById(
                    getRootOrgId()
                )
            )
        } catch (e: ModelDuplicateException) {
            throw OrgAlreadyExistsException(
                organization.name,
                organization.memberId
            )
        }

        grp.setAttribute("memberId", listOf(organization.memberId.toString()))
        grp.setAttribute("subscriptions", listOf("[]"))

        return Organization(
            UUID.fromString(grp.id),
            organization.name,
            organization.memberId
        )
    }

    private fun getGroupByMemberId(memberId: Long): GroupModel =
        session.groups()
            .searchGroupsByAttributes(session.context.realm, mapOf("memberId" to "$memberId"), 0, 1)
            .findAny().let {
                it.isEmpty && throw OrgNotFoundException()
                it.get()
            }

    fun updateOrg(organization: Organization): Organization {
        "[a-zA-Z0-9_&()\\[\\]., -]+".toRegex().matches(organization.name) || throw InvalidOrgNameException()

        try {
            findOrgByName(organization.name).let {
                if (organization.orgId != it.orgId) {
                    throw OrgAlreadyExistsException(organization.name, organization.memberId)
                }
            }
        } catch (e: OrgNotFoundException) {
            /* We continue if the group has not been found */
        }

        with(session.context.realm.getGroupById(organization.orgId.toString())) {
            this.let {
                if (it == null) {
                    return addOrg(organization)
                } else {
                    it.name = organization.name
                    return fromGroupModel(it)
                }
            }
        }
    }

    fun addUserToOrg(memberId: Long, userId: UUID) {
        val orgId = findOrgByMemberId(memberId).orgId
        (
            session.users().getUserById(session.context.realm, userId.toString())
                ?: throw UserNotFoundException()
            ).joinGroup(
            session.context.realm.getGroupById(orgId.toString()) ?: throw OrgNotFoundException()
        )
    }

    fun removeUserFromOrg(memberId: Long, userId: UUID) {
        val orgId = findOrgByMemberId(memberId).orgId
        (
            session.users().getUserById(session.context.realm, userId.toString())
                ?: throw UserNotFoundException()
            ).leaveGroup(
            session.context.realm.getGroupById(orgId.toString()) ?: throw OrgNotFoundException()
        )
    }

    fun getUsersFromOrg(memberId: Long, page: Int?, pageSize: Int?): PaginatedResource<User> {
        session.context.realm.getRole(Role.USER.roleName).let { userRole ->
            return PaginatedResource(
                { _page: Int, _pageSize: Int ->
                    session.users().getGroupMembersStream(
                        session.context.realm,
                        session.context.realm.getGroupById(findOrgByMemberId(memberId).orgId.toString())
                            ?: throw OrgNotFoundException(),
                        _page * _pageSize,
                        _pageSize
                    ).filter { it.hasRole(userRole) }
                        .map { fromUserModel(it) }.toList()
                },
                page,
                pageSize
            )
        }
    }

    private fun getSubscriptionPolicies(subscription: Subscription) =
        Constants.Crossref.SUBSCRIPTION_POLICIES.getOrDefault(subscription, mapOf()).flatMap {
            session
                .getProvider(AuthorizationProvider::class.java)
                .storeFactory
                .resourceServerStore
                .findByClient(session.context.realm.getClientByClientId(it.key))
                .let { resourceServer ->
                    it.value.map { policyName ->
                        session
                            .getProvider(AuthorizationProvider::class.java)
                            .storeFactory
                            .policyStore
                            .findByName(resourceServer, policyName)
                            .id
                            .let { policyId ->
                                session
                                    .getProvider(AuthorizationProvider::class.java)
                                    .storeFactory
                                    .policyStore
                                    .findById(session.context.realm, resourceServer, policyId)
                            }
                    }
                }
        }

    private fun parseOrgs(orgsJson: String) = mapper
        .readValue(orgsJson, object : TypeReference<List<GroupDefinition>>() {})
        .associateBy({ it.id }, { it })
        .toMutableMap()

    private fun addOrgToPolicy(policy: Policy, policyGroup: GroupDefinition) =
        parseOrgs(policy.config.getOrDefault("groups", "[]")).also {
            it[policyGroup.id] = policyGroup
        }.values.toMutableSet()

    private fun delOrgFromPolicy(policy: Policy, policyGroup: GroupDefinition) =
        parseOrgs(policy.config.getOrDefault("groups", "[]")).also {
            it.remove(policyGroup.id)
        }.values.toMutableSet()

    fun addOrgSubscription(memberId: Long, subscription: Subscription) {
        val org = getGroupByMemberId(memberId)

        mapper
            .readValue(org.attributes.getOrDefault("subscriptions", listOf("[]"))[0], object : TypeReference<List<String>>() {})
            .let {
                it.toMutableSet().let { s ->
                    s.add(subscription.toString())
                    org.setAttribute("subscriptions", listOf(mapper.writeValueAsString(s.toList())))
                }
            }

        getSubscriptionPolicies(subscription).forEach { policy ->
            RepresentationToModel.toModel(
                GroupPolicyRepresentation().also {
                    it.id = policy.id
                    it.name = policy.name
                    it.type = policy.type
                    it.logic = policy.logic
                    it.decisionStrategy = policy.decisionStrategy
                    it.groupsClaim = ""
                    it.groups = addOrgToPolicy(
                        policy,
                        GroupDefinition().also { gd ->
                            gd.id = org.id
                            gd.isExtendChildren = false
                        }
                    )
                },
                session
                    .getProvider(AuthorizationProvider::class.java),
                policy
            )
        }
    }

    fun delOrgSubscription(memberId: Long, subscription: Subscription) {
        val org = getGroupByMemberId(memberId)

        mapper
            .readValue(org.attributes.getOrDefault("subscriptions", listOf("[]"))[0], object : TypeReference<List<String>>() {})
            .let {
                it.toMutableSet().let { s ->
                    s.remove(subscription.toString())
                    org.setAttribute("subscriptions", listOf(mapper.writeValueAsString(s.toList())))
                }
            }

        getSubscriptionPolicies(subscription).forEach { policy ->
            RepresentationToModel.toModel(
                GroupPolicyRepresentation().also {
                    it.id = policy.id
                    it.name = policy.name
                    it.type = policy.type
                    it.logic = policy.logic
                    it.decisionStrategy = policy.decisionStrategy
                    it.groupsClaim = ""
                    it.groups = delOrgFromPolicy(
                        policy,
                        GroupDefinition().also { gd ->
                            gd.id = org.id
                            gd.isExtendChildren = false
                        }
                    )
                },
                session
                    .getProvider(AuthorizationProvider::class.java),
                policy
            )
        }
    }

    fun getOrgSubscriptions(memberId: Long): List<Subscription> {
        val org = getGroupByMemberId(memberId)

        return mapper
            .readValue(org.attributes.getOrDefault("subscriptions", listOf("[]"))[0], object : TypeReference<List<Subscription>>() {})
    }
}
