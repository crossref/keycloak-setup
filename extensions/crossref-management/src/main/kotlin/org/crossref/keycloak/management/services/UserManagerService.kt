package org.crossref.keycloak.management.services

import io.sentry.Sentry
import jakarta.ws.rs.ForbiddenException
import jakarta.ws.rs.core.Response
import org.apache.commons.validator.routines.EmailValidator
import org.crossref.keycloak.management.CrossrefManagementConfiguration
import org.crossref.keycloak.management.Organization
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.management.SyncOperation
import org.crossref.keycloak.management.User
import org.crossref.keycloak.management.UserSyncMessage
import org.crossref.keycloak.management.fromGroupModel
import org.crossref.keycloak.management.fromUserModel
import org.crossref.keycloak.management.utils.InvalidUsernameException
import org.crossref.keycloak.management.utils.PaginatedResource
import org.crossref.keycloak.management.utils.UserAlreadyExistsException
import org.crossref.keycloak.management.utils.UserNotFoundException
import org.crossref.keycloak.util.Constants
import org.keycloak.authentication.actiontoken.execactions.ExecuteActionsActionToken
import org.keycloak.common.util.Time
import org.keycloak.credential.CredentialModel
import org.keycloak.email.EmailException
import org.keycloak.email.EmailTemplateProvider
import org.keycloak.events.EventBuilder
import org.keycloak.models.KeycloakSession
import org.keycloak.models.ModelDuplicateException
import org.keycloak.models.UserModel
import org.keycloak.representations.idm.CredentialRepresentation
import org.keycloak.services.ErrorResponse
import org.keycloak.services.ServicesLogger
import org.keycloak.services.resources.LoginActionsService
import org.keycloak.util.JsonSerialization.mapper
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.util.Base64
import java.util.UUID
import java.util.concurrent.TimeUnit
import kotlin.streams.toList

class UserManagerService(private val session: KeycloakSession, private val config: CrossrefManagementConfiguration) {
    private val emailValidator: EmailValidator = EmailValidator.getInstance()

    fun getUsers(email: String, page: Int?, pageSize: Int?): PaginatedResource<User> {
        session.context.realm.getRole(Role.USER.roleName).let { userRole ->
            return PaginatedResource(
                { _page: Int, _pageSize: Int ->
                    session.users()
                        .searchForUserStream(
                            session.context.realm,
                            mapOf(UserModel.SEARCH to "*$email*", UserModel.USERNAME to "@"),
                            _page * _pageSize,
                            _pageSize,
                        ).filter { it.hasRole(userRole) }
                        .map { fromUserModel(it) }
                        .toList()
                },
                page,
                pageSize,
            )
        }
    }

    fun sendActions(user: UserModel, actions: List<String>) {
        val lifespan = session.context.realm.actionTokenGeneratedByAdminLifespan
        val expiration: Int = Time.currentTime() + lifespan
        val token = ExecuteActionsActionToken(user.id, user.email, expiration, actions, null, org.keycloak.models.Constants.ACCOUNT_MANAGEMENT_CLIENT_ID)

        try {
            val builder = LoginActionsService.actionTokenProcessor(session.context.uri)
            builder.queryParam("key", token.serialize(session, session.context.realm, session.context.uri))
            val link = builder.build(Constants.Crossref.REALM).toString()
            session.getProvider(EmailTemplateProvider::class.java)
                .setAttribute(org.keycloak.models.Constants.TEMPLATE_ATTR_REQUIRED_ACTIONS, token.requiredActions)
                .setRealm(session.context.realm)
                .setUser(user)
                .sendExecuteActions(link, TimeUnit.SECONDS.toMinutes(lifespan.toLong()))
        } catch (e: EmailException) {
            ServicesLogger.LOGGER.failedToSendActionsEmail(e)
            ErrorResponse.error("Failed to send execute actions email", Response.Status.INTERNAL_SERVER_ERROR)
        }
    }

    fun syncAuthenticator(username: String) {
        if (config.authenticatorUrl.isNotEmpty() && config.authenticatorCredentials.isNotEmpty()) {
            HttpRequest.newBuilder()
                .uri(URI.create(config.authenticatorUrl))
                .header("Content-type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(mapOf("username" to username))))
                .header("Authorization", "Basic " + Base64.getEncoder().encodeToString(config.authenticatorCredentials.toByteArray(Charsets.UTF_8)))
                .build().also {
                    try {
                        HttpClient.newBuilder().build().send(it, HttpResponse.BodyHandlers.ofString()).statusCode().let {
                            if (it != 200) {
                                throw Exception("Authenticator sync failed for user $username, with statuscode $it")
                            }
                        }
                    } catch (e: Exception) {
                        Sentry.captureException(e)
                    }
                }
        }
    }

    fun addUser(user: User, role: Role): User {
        if (!emailValidator.isValid(user.username)) {
            throw InvalidUsernameException()
        }

        if (!setOf(Role.USER, Role.STAFF).contains(role)) {
            throw ForbiddenException()
        }

        try {
            session.users().addUser(session.context.realm, user.username)?.let {
                it.email = user.username
                it.isEnabled = true
                it.grantRole(session.context.realm.getRole(role.roleName))
                // We'll only send email when authenticator gets fully replaced by Keycloak
                // sendActions(it, listOf(UserModel.RequiredAction.VERIFY_EMAIL.toString()))
                syncAuthenticator(user.username)
                return fromUserModel(it)
            }
        } catch (e: ModelDuplicateException) {
            throw UserAlreadyExistsException(user.username)
        }
        throw Exception("Could not create user")
    }

    fun delUser(userId: UUID) {
        with(session.users().getUserById(session.context.realm, userId.toString())) {
            this ?: throw UserNotFoundException()
            session.users().removeUser(session.context.realm, this)
        }
    }

    fun getOrgs(userId: UUID, name: String?, page: Int?, pageSize: Int?) = PaginatedResource<Organization> (
        { _page: Int, _pageSize: Int ->
            (
                session.users()
                    .getUserById(session.context.realm, userId.toString()) ?: throw UserNotFoundException()
                )
                .getGroupsStream(name ?: "", _page * _pageSize, _pageSize)
                .map { fromGroupModel(it) }
                .toList()
        },
        page,
        pageSize,
    )

    // This function will never apply to accounts that are not exclusively attached to ROLE_USER
    // It will be called only by the Authenticator service in order to synchronize password hashes
    // of users in our legacy authentication system. This endpoint is not exposed in our openapi spec
    fun syncUser(request: UserSyncMessage, eventBuilder: EventBuilder) {
        val disallowedRoles = Role.values().filter { it != Role.USER }.map { it.roleName }.toSet()

        val userModel = session.users().getUserByEmail(session.context.realm, request.username)

        if (request.operation == SyncOperation.CREATE && userModel == null) {
            session.users().addUser(session.context.realm, request.username).also {
                it.isEmailVerified = true
                it.email = request.username
                it.isEnabled = true
                it.grantRole(session.context.realm.getRole(Role.USER.roleName))
                syncUserHash(it, request)
                eventBuilder.detail("syncOperation", SyncOperation.CREATE.name).success()
            }
            return
        }

        userModel?.let { userModel ->
            if (userModel.roleMappingsStream.map { it.name }.toList().toSet().intersect(disallowedRoles).isEmpty() &&
                userModel.hasRole(session.context.realm.getRole(Role.USER.roleName))
            ) {
                if (request.operation == SyncOperation.SYNC || request.operation == SyncOperation.CREATE) {
                    syncUserHash(userModel, request)
                    eventBuilder.detail("syncOperation", SyncOperation.SYNC.name).success()
                } else if (request.operation == SyncOperation.DELETE) {
                    session.users().removeUser(session.context.realm, userModel)
                    eventBuilder.detail("syncOperation", SyncOperation.DELETE.name).success()
                }
            }
        }
    }

    private fun syncUserHash(userModel: UserModel, request: UserSyncMessage) {
        if (request.hash.isEmpty() || request.algorithm.isEmpty()) {
            return
        }

        userModel.credentialManager().getStoredCredentialsByTypeStream(CredentialRepresentation.PASSWORD).forEach { userModel.credentialManager().removeStoredCredentialById(it.id) }
        userModel.credentialManager().createStoredCredential(
            CredentialModel().also {
                it.type = CredentialRepresentation.PASSWORD
                it.credentialData = mapper.writeValueAsString(mapOf("hashIterations" to request.iterations, "algorithm" to request.algorithm))
                it.secretData = mapper.writeValueAsString(mapOf("salt" to request.salt, "value" to request.hash))
            },
        )
    }
}
