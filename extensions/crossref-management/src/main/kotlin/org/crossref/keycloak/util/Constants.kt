package org.crossref.keycloak.util

import org.crossref.keycloak.management.Subscription
import org.jboss.resteasy.reactive.RestResponse.Status
import org.keycloak.broker.oidc.mappers.UserAttributeMapper.USER_ATTRIBUTE
import org.keycloak.protocol.oidc.OIDCConfigAttributes.USE_REFRESH_TOKEN
import org.keycloak.protocol.oidc.OIDCConfigAttributes.USE_REFRESH_TOKEN_FOR_CLIENT_CREDENTIALS_GRANT
import org.keycloak.protocol.oidc.mappers.ClaimsParameterWithValueIdTokenMapper.CLAIM_NAME
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper.INCLUDE_IN_ACCESS_TOKEN
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper.INCLUDE_IN_ID_TOKEN
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper.INCLUDE_IN_USERINFO
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper.JSON_TYPE
import org.keycloak.representations.idm.ClientRepresentation
import org.keycloak.representations.idm.ProtocolMapperRepresentation
import org.keycloak.representations.idm.authorization.DecisionStrategy
import org.keycloak.representations.idm.authorization.Logic
import org.keycloak.representations.idm.authorization.PolicyRepresentation
import org.keycloak.representations.idm.authorization.ResourcePermissionRepresentation
import org.keycloak.representations.idm.authorization.ResourceRepresentation

class Constants {
    class Crossref {
        companion object {
            const val REALM = "crossref"
            const val ORGS_BASE_GROUP = "organizations"
            const val VOID_GROUP = "void"
            const val QUERY_PAGE_SIZE = 10
            const val DEFAULT_APIKEY_CACHE_TTL: Long = 60
            const val DEFAULT_APIKEY_CACHE_SIZE: Long = 20000
            const val PWDSYNC_USER = "crossref-sync"
            const val RESOURCES_CLIENT = "crossref-resources"
            const val CROSSREF_UI_CLIENT = "ui"
            const val MAX_KEY_DESCRIPTION_LENGTH = 255
            const val ORG_NAME_PATTERN = "[a-zA-Z0-9_&:()\\[\\]., -]+"
            val SUBSCRIPTION_POLICIES = mapOf(
                Subscription.METADATA_PLUS to mapOf(
                    RESOURCES_CLIENT to listOf("metadata-plus-data-access-policy")
                )
            )

            // Initial resources and policies configurations for realm clients
            val clientResources = mapOf(
                RESOURCES_CLIENT to listOf(
                    Triple(
                        ResourceRepresentation().apply {
                            name = "metadata-plus-data"
                            displayName = "Plus data resource"
                            // So far uris are not used, but we specify them
                            // as it is the normal usage of keycloak resources
                            uris = setOf("/snapshots/monthly/*", "/oai*")
                        },
                        PolicyRepresentation().apply {
                            type = "group"
                            name = "metadata-plus-data-access-policy"
                            logic = Logic.POSITIVE
                            decisionStrategy = DecisionStrategy.UNANIMOUS
                            config = mapOf("groups" to "[]")
                        },
                        ResourcePermissionRepresentation().apply {
                            name = "metadata-plus-data-permission"
                        }
                    )
                )
            )

            // Initial realm client configurations
            val realmClients = mapOf(
                RESOURCES_CLIENT to ClientRepresentation().apply {
                    clientId = RESOURCES_CLIENT
                    name = "Crossref resources"
                    protocol = "openid-connect"
                    authorizationServicesEnabled = true
                    isServiceAccountsEnabled = true
                    isEnabled = true
                    isFullScopeAllowed = true
                    description = "Crossref resources and permissions client"
                    clientAuthenticatorType = "client-secret"
                    isDirectAccessGrantsEnabled = false
                    isBearerOnly = false
                    isImplicitFlowEnabled = false
                    isStandardFlowEnabled = false
                    isPublicClient = false
                    attributes = mutableMapOf(
                        USE_REFRESH_TOKEN_FOR_CLIENT_CREDENTIALS_GRANT to "true",
                        USE_REFRESH_TOKEN to "true"
                    )
                },
                CROSSREF_UI_CLIENT to ClientRepresentation().apply {
                    clientId = CROSSREF_UI_CLIENT
                    name = "Crossref UI"
                    description = "Crossref UI portal client"
                    isEnabled = true
                    isPublicClient = true
                    protocol = "openid-connect"
                    isStandardFlowEnabled = true
                    isImplicitFlowEnabled = false
                    isDirectAccessGrantsEnabled = true
                    defaultClientScopes = listOf("roles")
                    protocolMappers = listOf(
                        ProtocolMapperRepresentation().apply {
                            name = "email"
                            protocol = "openid-connect"
                            protocolMapper = "oidc-usermodel-property-mapper"
                            config = mapOf(
                                INCLUDE_IN_USERINFO to "true",
                                USER_ATTRIBUTE to "email",
                                INCLUDE_IN_ID_TOKEN to "true",
                                INCLUDE_IN_ACCESS_TOKEN to "true",
                                CLAIM_NAME to "email",
                                JSON_TYPE to "String"
                            )
                        }
                    )
                }
            )
        }
    }

    enum class ResourceResponseType(val code: Int) {
        RESOURCE_NOT_FOUND(Status.NOT_FOUND.statusCode),
        USER_NOT_FOUND(RESOURCE_NOT_FOUND.code),
        ORG_NOT_FOUND(RESOURCE_NOT_FOUND.code),
        KEY_NOT_FOUND(RESOURCE_NOT_FOUND.code),
        RESOURCE_CREATED(Status.CREATED.statusCode),
        USER_CREATED(RESOURCE_CREATED.code),
        ORG_CREATED(RESOURCE_CREATED.code),
        KEY_CREATED(RESOURCE_CREATED.code),
        DUPLICATED_RESOURCE(Status.CONFLICT.statusCode),
        DUPLICATED_USER(DUPLICATED_RESOURCE.code),
        DUPLICATED_ORG(DUPLICATED_RESOURCE.code),
        RESOURCE_UPDATED(Status.OK.statusCode),
        KEY_DISABLED(RESOURCE_UPDATED.code),
        KEY_ENABLED(RESOURCE_UPDATED.code),
        ORG_UPDATED(RESOURCE_UPDATED.code),
        KEY_UPDATED(RESOURCE_UPDATED.code),
        RESOURCE_DELETED(Status.NO_CONTENT.statusCode),
        USER_DELETED(RESOURCE_DELETED.code),
        KEY_DELETED(RESOURCE_DELETED.code),
        RESOURCE_ASSIGNED(Status.NO_CONTENT.statusCode),
        USER_ASSIGNED(RESOURCE_ASSIGNED.code),
        RESOURCE_UNASSIGNED(Status.NO_CONTENT.statusCode),
        USER_UNASSIGNED(RESOURCE_UNASSIGNED.code),
        FORBIDDEN(Status.FORBIDDEN.statusCode),
        INVALID_RESOURCE(Status.BAD_REQUEST.statusCode),
        INVALID_KEY_DESCRIPTION(INVALID_RESOURCE.code),
        SUBSCRIPTION_ADDED(RESOURCE_ASSIGNED.code),
        SUBSCRIPTION_REMOVED(RESOURCE_UNASSIGNED.code),
        SERVICE_UNAVAILABLE(Status.SERVICE_UNAVAILABLE.statusCode),
        BAD_REQUEST(Status.BAD_REQUEST.statusCode),
        INTERNAL_ERROR(Status.INTERNAL_SERVER_ERROR.statusCode),
        NOT_AUTHORIZED(Status.UNAUTHORIZED.statusCode),
    }
}
