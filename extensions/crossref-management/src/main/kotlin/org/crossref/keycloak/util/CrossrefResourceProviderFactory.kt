package org.crossref.keycloak.util

import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory
import org.keycloak.services.resource.RealmResourceProvider
import org.keycloak.services.resource.RealmResourceProviderFactory
import java.util.concurrent.locks.ReentrantLock

abstract class CrossrefResourceProviderFactory : RealmResourceProviderFactory {
    private val primaryInstance: Boolean = (System.getenv("KEYCLOAK_PRIMARY") ?: "").isNotEmpty()
    private val lock = ReentrantLock()
    private var extensionInitialized = false

    override fun create(session: KeycloakSession): RealmResourceProvider {
        if (!extensionInitialized) {
            lock.lock()
            mutexInitializer(session)
            lock.unlock()
        }
        return getResourceProvider(session)
    }

    private fun mutexInitializer(session: KeycloakSession) {
        if (!extensionInitialized) {
            initializeExtension(session)
            extensionInitialized = true
        }
    }

    open fun initializeExtension(session: KeycloakSession) {
    }

    abstract fun getResourceProvider(session: KeycloakSession): RealmResourceProvider

    override fun postInit(factory: KeycloakSessionFactory) {}
    override fun close() {}

    fun isPrimaryInstance() = primaryInstance
}
