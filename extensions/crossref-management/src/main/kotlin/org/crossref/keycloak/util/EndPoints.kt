package org.crossref.keycloak.management.utils

import jakarta.ws.rs.container.ContainerRequestContext
import jakarta.ws.rs.container.ContainerResponseContext
import jakarta.ws.rs.core.Link
import jakarta.ws.rs.core.UriInfo
import jakarta.ws.rs.ext.Provider
import org.crossref.keycloak.management.utils.LinkPaginationContainerResponseFilter.Companion.PAGE_QUERY_PARAM
import org.crossref.keycloak.util.Constants
import org.jboss.resteasy.reactive.server.ServerResponseFilter
import java.util.stream.Stream
import kotlin.math.max
import kotlin.streams.toList

interface Paginated<T> {
    fun currentPage(): List<T>
    fun currentPageIndex(): Int
    fun pageCount(): Int
    fun totalCount(): Int
    fun pageSize(): Int
}

class PaginatedResource<T>(val pageGenerator: (Int, Int) -> List<T>, _page: Int?, _pageSize: Int?) : Paginated<T> {
    private val page: Int
    private val pageSize: Int

    init {
        page = max((_page ?: 1) - 1, 0)
        pageSize = _pageSize ?: Constants.Crossref.QUERY_PAGE_SIZE
    }

    override fun currentPage(): List<T> = pageGenerator(page, pageSize)

    override fun currentPageIndex(): Int {
        return page + 1
    }

    override fun pageCount(): Int {
        return -1
    }

    override fun totalCount(): Int {
        return -1
    }

    override fun pageSize(): Int {
        return pageSize
    }
}

class LinkPagination(private val currentPageIndex: Int, private val pageCount: Int) {
    fun toLinks(uriInfo: UriInfo, last: Boolean = false): Stream<Link?>? {
        if (currentPageIndex == 1 && pageCount == 1) {
            return Stream.empty()
        }
        val linkStreamBuilder: Stream.Builder<Link> = Stream.builder()
        if (currentPageIndex > 1) {
            linkStreamBuilder.accept(
                Link.fromUriBuilder(
                    uriInfo.requestUriBuilder
                        .replaceQueryParam(
                            PAGE_QUERY_PARAM,
                            currentPageIndex - 1
                        )
                )
                    .rel("prev")
                    .build()
            )
        }
        if (!last && pageCount <= 0 || currentPageIndex < pageCount) {
            linkStreamBuilder.accept(
                Link.fromUriBuilder(
                    uriInfo.requestUriBuilder
                        .replaceQueryParam(
                            PAGE_QUERY_PARAM,
                            currentPageIndex + 1
                        )
                )
                    .rel("next")
                    .build()
            )
        }
        linkStreamBuilder.accept(
            Link.fromUriBuilder(
                uriInfo.requestUriBuilder
                    .replaceQueryParam(
                        PAGE_QUERY_PARAM,
                        1
                    )
            )
                .rel("first")
                .build()
        )
        if (pageCount > 0) {
            linkStreamBuilder.accept(
                Link.fromUriBuilder(
                    uriInfo.requestUriBuilder
                        .replaceQueryParam(
                            PAGE_QUERY_PARAM,
                            pageCount
                        )
                )
                    .rel("last")
                    .build()
            )
        }
        return linkStreamBuilder.build()
    }
}

@Provider
class LinkPaginationContainerResponseFilter {
    @ServerResponseFilter
    fun filter(
        requestContext: ContainerRequestContext,
        responseContext: ContainerResponseContext
    ) {
        if (responseContext.entity !is Paginated<*>) {
            return
        }

        val uriInfo = requestContext.uriInfo
        val entity = responseContext.entity as Paginated<*>
        val page = entity.currentPage()
        val pageSize = entity.pageSize()

        responseContext.entity = page

        with(LinkPagination(entity.currentPageIndex(), entity.pageCount())) {
            (
                if (page.size < pageSize) {
                    this.toLinks(uriInfo, true)
                } else {
                    this.toLinks(uriInfo)
                }
                )?.toList()?.let {
                responseContext.headers.add("Link", it.joinToString(", "))
            }
        }

        if (entity.totalCount() >= 0) {
            responseContext.headers.add(X_TOTAL_COUNT, entity.totalCount())
            responseContext.headers.add(X_PAGE_COUNT, entity.pageCount())
        }
    }

    companion object {
        const val X_TOTAL_COUNT = "X-Total-Count"
        const val X_PAGE_COUNT = "X-Page-Count"
        const val PAGE_QUERY_PARAM = "page"
    }
}
