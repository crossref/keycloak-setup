package org.crossref.keycloak.management.utils

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.databind.exc.ValueInstantiationException
import jakarta.ws.rs.ForbiddenException
import jakarta.ws.rs.NotAuthorizedException
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import org.crossref.keycloak.management.ErrorResponse
import org.crossref.keycloak.util.Constants
import org.jboss.logging.Logger
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper
import org.keycloak.events.jpa.JpaEventStoreProvider

fun buildResponse(msg: String, responseType: Constants.ResourceResponseType, log: Boolean = false): RestResponse<ErrorResponse> {
    if (log) {
        Logger.getLogger(JpaEventStoreProvider::class.java).error(msg)
    }
    return RestResponse.ResponseBuilder
        .create<ErrorResponse>(responseType.code)
        .header("Content-Type", MediaType.APPLICATION_JSON)
        .header("Access-Control-Allow-Origin", "*")
        .entity(ErrorResponse(msg, responseType))
        .build()
}

class ExceptionMapper {

    @ServerExceptionMapper
    fun toResponse(ex: AuthServiceUnavailableException) =
        buildResponse("Authorization service unavailable", Constants.ResourceResponseType.SERVICE_UNAVAILABLE)

    @ServerExceptionMapper
    fun toResponse(ex: OrgNotFoundException) =
        buildResponse("Organization not found", Constants.ResourceResponseType.ORG_NOT_FOUND)

    @ServerExceptionMapper
    fun toResponse(ex: OrgAlreadyExistsException) = buildResponse(
        "Organization with name ${ex.name} and / or Sugar ID ${ex.memberId} already exists",
        Constants.ResourceResponseType.DUPLICATED_ORG,
    )

    @ServerExceptionMapper
    @Produces(MediaType.APPLICATION_JSON)
    fun toResponse(ex: UserAlreadyExistsException) = buildResponse(
        "User with username ${ex.username} already exists",
        Constants.ResourceResponseType.DUPLICATED_USER,
    )

    @ServerExceptionMapper
    fun toResponse(ex: UserNotFoundException) = buildResponse(
        "User not found",
        Constants.ResourceResponseType.USER_NOT_FOUND,
    )

    @ServerExceptionMapper
    fun toResponse(ex: UsernameNotFoundException) = buildResponse(
        "Username ${ex.username} not found",
        Constants.ResourceResponseType.RESOURCE_NOT_FOUND,
    )

    @ServerExceptionMapper
    fun toResponse(ex: KeyNotFoundException) = buildResponse(
        "API Key not found",
        Constants.ResourceResponseType.KEY_NOT_FOUND,
    )

    @ServerExceptionMapper
    fun toResponse(ex: InvalidUsernameException) = buildResponse(
        "Username must be a valid email address",
        Constants.ResourceResponseType.BAD_REQUEST,
    )

    @ServerExceptionMapper
    fun toResponse(ex: InvalidOrgNameException) = buildResponse(
        "Organization name contains invalid characters",
        Constants.ResourceResponseType.BAD_REQUEST,
    )

    @ServerExceptionMapper
    fun toResponse(ex: UserAlreadyOrgAdminException) = buildResponse(
        "User with ID ${ex.userId} is already an administrator of organization with ID ${ex.orgId}",
        Constants.ResourceResponseType.DUPLICATED_USER,
    )

    @ServerExceptionMapper
    fun toResponse(ex: UserNotOrgAdminException) = buildResponse(
        "User with ID ${ex.userId} is not an administrator of organization with ID ${ex.orgId}",
        Constants.ResourceResponseType.FORBIDDEN,
    )

    @ServerExceptionMapper
    fun toResponse(ex: KeyDescriptionTooLong) = buildResponse(
        "Key description cannot be more than 255 characters",
        Constants.ResourceResponseType.INVALID_KEY_DESCRIPTION,
    )

    @ServerExceptionMapper
    fun toResponse(ex: InvalidFormatException) = buildResponse(
        "Invalid input",
        Constants.ResourceResponseType.BAD_REQUEST,
    )

    @ServerExceptionMapper
    fun toResponse(ex: ValueInstantiationException) =
        buildResponse(
            "Invalid input: ${ex.message}",
            Constants.ResourceResponseType.BAD_REQUEST,
        )

    @ServerExceptionMapper
    fun toResponse(ex: NotFoundException) = if (ex.cause is IllegalArgumentException) {
        buildResponse(
            "Invalid input: ${ex.message}",
            Constants.ResourceResponseType.BAD_REQUEST,
        )
    } else {
        buildResponse(
            "Invalid input: ${ex.message}",
            Constants.ResourceResponseType.INTERNAL_ERROR,
        )
    }

    @ServerExceptionMapper
    fun toResponse(ex: NotAuthorizedException) = buildResponse(
        "Not Authorized",
        Constants.ResourceResponseType.NOT_AUTHORIZED,
    )

    @ServerExceptionMapper
    fun toResponse(ex: ForbiddenException) = buildResponse(
        "Forbidden",
        Constants.ResourceResponseType.FORBIDDEN,
    )

    @ServerExceptionMapper
    fun toResponse(ex: JsonMappingException) = buildResponse(
        "Error parsing JSON:\n\n${ex.path.map { it.description }.joinToString("\n")}\n\n${ex.cause?.message}",
        Constants.ResourceResponseType.BAD_REQUEST,
    )

    @ServerExceptionMapper
    fun toResponse(ex: JsonParseException) = buildResponse(
        "Error parsing JSON:\n\nLOCATION = ${ex.processor.valueAsString}\n\n${ex.message}",
        Constants.ResourceResponseType.BAD_REQUEST,
        true,
    )
}
