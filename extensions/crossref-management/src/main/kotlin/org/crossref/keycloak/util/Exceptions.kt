package org.crossref.keycloak.management.utils

import org.crossref.keycloak.management.Subscription
import java.util.UUID

class AuthServiceUnavailableException(connectExceptionMessage: String?) : Exception(connectExceptionMessage)

class OrgNotFoundException : Exception()

class OrgAlreadyExistsException(val name: String, val memberId: Long) : Exception()

class UserAlreadyExistsException(val username: String) : Exception()

class UserNotFoundException : Exception()

class UsernameNotFoundException(val username: String) : Exception()

class KeyNotFoundException : Exception()

class KeyDescriptionTooLong : Exception()

class InvalidUsernameException : Exception()

class InvalidOrgNameException : Exception()

class UserAlreadyOrgAdminException(val orgId: UUID, val userId: UUID) : Exception()

class UserNotOrgAdminException(val orgId: UUID, val userId: UUID) : Exception()

class OrgSubscriptionUnchangedException(val orgId: UUID, val subscription: Subscription, val status: Boolean) :
    Exception()

class KeyStatusUnchangedException(val keyId: UUID, val status: Boolean) :
    Exception()
