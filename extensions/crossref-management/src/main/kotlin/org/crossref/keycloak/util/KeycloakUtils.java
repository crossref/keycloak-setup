package org.crossref.keycloak.util;

import org.keycloak.authorization.AuthorizationProvider;
import org.keycloak.authorization.admin.PolicyEvaluationService;
import org.keycloak.authorization.attribute.Attributes;
import org.keycloak.authorization.common.DefaultEvaluationContext;
import org.keycloak.authorization.common.KeycloakIdentity;
import org.keycloak.authorization.model.Resource;
import org.keycloak.authorization.model.ResourceServer;
import org.keycloak.authorization.model.Scope;
import org.keycloak.authorization.permission.Permissions;
import org.keycloak.authorization.permission.ResourcePermission;
import org.keycloak.authorization.policy.evaluation.EvaluationContext;
import org.keycloak.authorization.store.ScopeStore;
import org.keycloak.authorization.store.StoreFactory;
import org.keycloak.models.ClientModel;
import org.keycloak.models.ClientSessionContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.UserSessionModel;
import org.keycloak.protocol.oidc.OIDCLoginProtocol;
import org.keycloak.protocol.oidc.TokenManager;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.idm.authorization.AuthorizationRequest;
import org.keycloak.representations.idm.authorization.PolicyEvaluationRequest;
import org.keycloak.representations.idm.authorization.ResourceRepresentation;
import org.keycloak.representations.idm.authorization.ScopeRepresentation;
import org.keycloak.services.Urls;
import org.keycloak.services.managers.AuthenticationManager;
import org.keycloak.sessions.AuthenticationSessionModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class KeycloakUtils {
    public EvaluationContext createEvaluationContext(PolicyEvaluationRequest representation, KeycloakIdentity identity, AuthorizationProvider authorization) {
        return new DefaultEvaluationContext(identity, authorization.getKeycloakSession()) {
            @Override
            public Attributes getAttributes() {
                Map<String, Collection<String>> attributes = new HashMap<>(super.getAttributes().toMap());
                Map<String, String> givenAttributes = representation.getContext().get("attributes");

                if (givenAttributes != null) {
                    givenAttributes.forEach((key, entryValue) -> {
                        if (entryValue != null) {
                            List<String> values = new ArrayList<>();
                            Collections.addAll(values, entryValue.split(","));

                            attributes.put(key, values);
                        }
                    });
                }

                return Attributes.from(attributes);
            }
        };
    }

    public PolicyEvaluationService.EvaluationDecisionCollector evaluate(PolicyEvaluationRequest evaluationRequest, EvaluationContext evaluationContext, AuthorizationRequest request, ResourceServer resourceServer, AuthorizationProvider authorization) {
        List<ResourcePermission> permissions = createPermissions(evaluationRequest, authorization, request, resourceServer);

        if (permissions.isEmpty()) {
            return authorization.evaluators().from(evaluationContext, resourceServer, request).evaluate(new PolicyEvaluationService.EvaluationDecisionCollector(authorization, resourceServer, request));
        }

        return authorization.evaluators().from(permissions, evaluationContext).evaluate(new PolicyEvaluationService.EvaluationDecisionCollector(authorization, resourceServer, request));
    }

    private List<ResourcePermission> createPermissions(PolicyEvaluationRequest representation, AuthorizationProvider authorization, AuthorizationRequest request, ResourceServer resourceServer) {
        return representation.getResources().stream().flatMap((Function<ResourceRepresentation, Stream<ResourcePermission>>) resource -> {
            StoreFactory storeFactory = authorization.getStoreFactory();
            if (resource == null) {
                resource = new ResourceRepresentation();
            }

            Set<ScopeRepresentation> givenScopes = resource.getScopes();

            if (givenScopes == null) {
                givenScopes = new HashSet<>();
            }

            ScopeStore scopeStore = storeFactory.getScopeStore();

            Set<Scope> scopes = givenScopes.stream().map(scopeRepresentation -> scopeStore.findByName(resourceServer, scopeRepresentation.getName())).collect(Collectors.toSet());

            if (resource.getId() != null) {
                Resource resourceModel = storeFactory.getResourceStore().findById(resourceServer.getRealm(), resourceServer, resource.getId());
                return new ArrayList<>(Collections.singletonList(
                        Permissions.createResourcePermissions(resourceModel, resourceServer, scopes, authorization, request))).stream();
            } else if (resource.getType() != null) {
                return storeFactory.getResourceStore().findByType(resourceServer, resource.getType()).stream().map(resource1 -> Permissions.createResourcePermissions(resource1,
                        resourceServer, scopes, authorization, request));
            } else {
                if (scopes.isEmpty()) {
                    return Stream.empty();
                }

                List<Resource> resources = storeFactory.getResourceStore().findByScopes(resourceServer, scopes);

                if (resources.isEmpty()) {
                    return scopes.stream().map(scope -> new ResourcePermission(null, new ArrayList<>(Collections.singletonList(scope)), resourceServer));
                }


                return resources.stream().map(resource12 -> Permissions.createResourcePermissions(resource12, resourceServer,
                        scopes, authorization, request));
            }
        }).collect(Collectors.toList());
    }

    public static class CloseableKeycloakIdentity extends KeycloakIdentity {
        private final UserSessionModel userSession;

        public CloseableKeycloakIdentity(AccessToken accessToken, KeycloakSession keycloakSession, UserSessionModel userSession) {
            super(accessToken, keycloakSession);
            this.userSession = userSession;
        }

        public void close() {
            if (userSession != null) {
                keycloakSession.sessions().removeUserSession(realm, userSession);
            }

        }

        @Override
        public String getId() {
            if (userSession != null) {
                return super.getId();
            }

            String issuedFor = accessToken.getIssuedFor();

            if (issuedFor != null) {
                UserModel serviceAccount = keycloakSession.users().getServiceAccount(realm.getClientByClientId(issuedFor));

                if (serviceAccount != null) {
                    return serviceAccount.getId();
                }
            }

            return null;
        }
    }

    public CloseableKeycloakIdentity createIdentity(PolicyEvaluationRequest representation, ResourceServer resourceServer, AuthorizationProvider authorization, RealmModel realm) {
        KeycloakSession keycloakSession = authorization.getKeycloakSession();
        AccessToken accessToken = null;


        String subject = representation.getUserId();

        UserSessionModel userSession = null;
        if (subject != null) {
            UserModel userModel = keycloakSession.users().getUserById(realm, subject);

            if (userModel == null) {
                userModel = keycloakSession.users().getUserByUsername(realm, subject);
            }

            if (userModel != null) {
                String clientId = representation.getClientId();

                if (clientId == null) {
                    clientId = resourceServer.getClientId();
                }

                if (clientId != null) {
                    ClientModel clientModel = realm.getClientById(clientId);

                    AuthenticationSessionModel authSession = keycloakSession.authenticationSessions().createRootAuthenticationSession(realm)
                            .createAuthenticationSession(clientModel);
                    authSession.setProtocol(OIDCLoginProtocol.LOGIN_PROTOCOL);
                    authSession.setAuthenticatedUser(userModel);
                    userSession = keycloakSession.sessions().createUserSession(authSession.getParentSession().getId(), realm, userModel,
                            userModel.getUsername(), "127.0.0.1", "passwd", false, null, null, UserSessionModel.SessionPersistenceState.PERSISTENT);

                    AuthenticationManager.setClientScopesInSession(authSession);
                    ClientSessionContext clientSessionCtx = TokenManager.attachAuthenticationSession(keycloakSession, userSession, authSession);

                    accessToken = new TokenManager().createClientAccessToken(keycloakSession, realm, clientModel, userModel, userSession, clientSessionCtx);
                }
            }
        }

        if (accessToken == null) {
            accessToken = new AccessToken();

            accessToken.subject(representation.getUserId());
            ClientModel client = null;
            String clientId = representation.getClientId();

            if (clientId != null) {
                client = realm.getClientById(clientId);
            }

            if (client == null) {
                client = realm.getClientById(resourceServer.getClientId());
            }

            accessToken.issuedFor(client.getClientId());
            accessToken.audience(client.getId());
            accessToken.issuer(Urls.realmIssuer(keycloakSession.getContext().getUri().getBaseUri(), realm.getName()));
            accessToken.setRealmAccess(new AccessToken.Access());
        }

        if (representation.getRoleIds() != null && !representation.getRoleIds().isEmpty()) {
            if (accessToken.getRealmAccess() == null) {
                accessToken.setRealmAccess(new AccessToken.Access());
            }
            AccessToken.Access realmAccess = accessToken.getRealmAccess();

            representation.getRoleIds().forEach(realmAccess::addRole);
        }

        return new CloseableKeycloakIdentity(accessToken, keycloakSession, userSession);
    }
}
