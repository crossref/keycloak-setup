package org.crossref.keycloak

import com.fasterxml.jackson.core.type.TypeReference
import org.crossref.keycloak.management.APIKey
import org.crossref.keycloak.management.APIKeyResponse
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.management.Subscription
import org.crossref.keycloak.util.Constants
import org.keycloak.admin.client.CreatedResponseUtil
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.representations.idm.CredentialRepresentation
import org.keycloak.representations.idm.UserRepresentation
import org.keycloak.util.JsonSerialization
import java.math.BigInteger
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.security.MessageDigest
import java.util.Base64
import java.util.UUID

open class ManagementClient {
    var status = 0
    var returnedBody: String? = null
    val userToken = mutableMapOf<String, String>()

    data class UserDef(val username: String, val type: String)

    val keycloak = KeycloakBuilder.builder()
        .serverUrl(System.getenv("KEYCLOAK_ADMIN_URL") + "/auth")
        .realm("master")
        .clientId("admin-cli")
        .username("admin")
        .password(System.getenv("KEYCLOAK_ADMIN_PWD"))
        .build().apply { this.realm(System.getenv("CROSSREF_REALM")) }

    val realmResource = keycloak.realm(System.getenv("CROSSREF_REALM"))

    fun getUserIdByEmailAddress(emailAddress: String): String? {
        return realmResource.users().search(emailAddress)?.firstOrNull()?.id
    }

    fun getOrganizationSubGroups() = realmResource.getGroupByPath("/organizations").id.let {
        realmResource
            .groups()
            .group(it)
            .getSubGroups(0, -1, false)
    }

    fun deleteOrgsAndUsers() {
        getOrganizationSubGroups()
            .map {
                realmResource.groups().group(it.id).remove()
            }
        realmResource
            .users()
            .list()
            .filter { !setOf("crossref", "crossref-sync").contains(it.username) }
            .forEach {
                realmResource.users().get(it.id).remove()
            }
    }

    fun generateApiKeyHash(secret: String): String {
        val sha256 = MessageDigest
            .getInstance("SHA-256") ?: throw InstantiationException("Could not instantiate docker key identifier, no SHA-256 algorithm available.")
        return BigInteger(1, sha256.digest(secret.toByteArray()))
            .toString(16).let { hexHash ->
                if (hexHash.length < 64) {
                    "0".repeat(64 - hexHash.length) + hexHash
                } else {
                    hexHash
                }
            }
            .lowercase()
    }

    fun createUser(user: UserDef) {
        val role = if (user.type == "STAFF") Role.STAFF.roleName else if (user.type == "USER") Role.USER.roleName else Role.USER.roleName

        with(realmResource.users().search(user.username)) {
            if (this.size != 0) {
                realmResource.users().delete(this.first().id)
            }
            with(UserRepresentation()) {
                this.username = user.username
                this.email = user.username
                this.isEnabled = true
                this.isEmailVerified = true
                this.credentials = listOf(
                    CredentialRepresentation().apply {
                        this.type = "password"
                        this.value = "pwdPWD1!"
                    }
                )
                with(realmResource.users().create(this)) {
                    val userId = CreatedResponseUtil.getCreatedId(this)
                    with(realmResource.users().get(userId)) {
                        this.roles().realmLevel().add(listOf(realmResource.roles().get(role).toRepresentation()))
                    }
                }
            }
        }

        loginUser(user.username)
    }

    fun managerCreateUser(role: String, name: String, token: String) {
        callKeycloakEndpoint(
            "/user?role=$role",
            token,
            JsonSerialization.mapper.writeValueAsString(
                mapOf(
                    "username" to name,
                    "userId" to UUID.randomUUID()
                )
            ),
            "POST"
        )
    }

    fun loginUser(userName: String) {
        userToken[userName] = "Bearer " + KeycloakBuilder.builder()
            .serverUrl(System.getenv("KEYCLOAK_ADMIN_URL") + "/auth")
            .realm(System.getenv("CROSSREF_REALM"))
            .clientId(Constants.Crossref.CROSSREF_UI_CLIENT)
            .username(userName)
            .password("pwdPWD1!")
            .build().tokenManager().accessToken.token
    }

    fun managerAddSubscriptionToOrg(subscription: String, memberId: String, token: String) {
        callKeycloakEndpoint(
            "/organization/$memberId/subscription/$subscription",
            token,
            "",
            "PUT"
        )
    }

    fun managerRemoveSubscriptionToOrg(subscription: String, memberId: String, token: String) {
        callKeycloakEndpoint(
            "/organization/$memberId/subscription/$subscription",
            token,
            "",
            "DELETE"
        )
    }
    fun managerAddUserToOrg(user: String, memberId: String, token: String) {
        callKeycloakEndpoint("/organization/$memberId/user/${getUserIdByEmailAddress(user)}", token, null, "POST")
    }

    fun managerCreateApiKey(memberId: String, token: String, desc: String): String {
        callKeycloakEndpoint(
            "/organization/$memberId/key",
            token,
            JsonSerialization.mapper.writeValueAsString(APIKey(desc, UUID.randomUUID())),
            "POST",
            "application/json"
        )
        return generateApiKeyHash(JsonSerialization.mapper.readValue(returnedBody, object : TypeReference<APIKeyResponse>() {}).key)
    }

    fun checkPermission(keyHash: String, subscription: Subscription): Boolean {
        val subscriptionToResource = mapOf(Subscription.METADATA_PLUS to "metadata-plus-data")
        doReq(
            "${System.getenv("KEYCLOAK_ADMIN_URL")}/auth/realms/${System.getenv("CROSSREF_REALM")}/checkApiKeyPermissions",
            null,
            JsonSerialization.mapper.writeValueAsString(
                mapOf(
                    "apiKeyHash" to keyHash,
                    "clientSecret" to "**********",
                    "resource" to subscriptionToResource[subscription],
                    "clientName" to Constants.Crossref.RESOURCES_CLIENT,
                    "scope" to "",
                    "resourcePath" to ""
                )
            ),
            "POST"
        )
        return status == 200
    }

    fun callKeycloakEndpoint(url: String, token: String?, body: String?, method: String = "GET", contentType: String? = null) {
        doReq(
            System.getenv("KEYCLOAK_ADMIN_URL").let { originalUrl ->
                URI(originalUrl).let {
                    URI(it.scheme, null, it.host, it.port, "/auth/realms/${System.getenv("CROSSREF_REALM")}/crossref-management", null, null).toString() + url
                }
            },
            token,
            body,
            method,
            contentType
        )
    }

    fun callAuthenticatorEndpoint(url: String, body: String?, method: String = "GET", contentType: String? = null) {
        doReq(
            System.getenv("AUTHENTICATOR_URL").let { originalUrl ->
                URI(originalUrl).let {
                    URI(it.scheme, null, it.host, it.port, url, null, null).toString()
                }
            },
            "Basic " + Base64.getEncoder().encodeToString("${System.getenv("AUTHENTICATOR_INTERNAL_USER")}:${System.getenv("AUTHENTICATOR_INTERNAL_PASSWORD")}".toByteArray(Charsets.UTF_8)),
            body,
            method,
            contentType
        )
    }

    fun doReq(url: String, auth: String?, body: String?, method: String = "GET", contentType: String? = null) {
        val client = HttpClient.newBuilder().build()
        val request = HttpRequest.newBuilder()
            .uri(URI.create(url))
            .also { req ->
                auth?.let {
                    req.header("Authorization", it)
                }
            }
            .also { req ->
                body?.let {
                    if (contentType != null) {
                        req.header("Content-type", contentType)
                    } else {
                        req.header("Content-type", "application/json")
                    }
                }
                when (method) {
                    "POST" -> req.POST(HttpRequest.BodyPublishers.ofString(body ?: ""))
                    "PUT" -> req.PUT(HttpRequest.BodyPublishers.ofString(body ?: ""))
                    "DELETE" -> req.DELETE()
                }
            }
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())

        status = response.statusCode()
        returnedBody = response.body()
    }
}
