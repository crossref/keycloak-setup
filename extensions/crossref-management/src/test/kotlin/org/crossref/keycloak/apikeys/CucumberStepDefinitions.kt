package org.crossref.keycloak.apikeys

import com.fasterxml.jackson.core.type.TypeReference
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.util.Constants.Crossref.Companion.RESOURCES_CLIENT
import org.keycloak.admin.client.CreatedResponseUtil
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.admin.client.resource.PolicyResource
import org.keycloak.representations.idm.GroupRepresentation
import org.keycloak.representations.idm.UserRepresentation
import org.keycloak.representations.idm.authorization.PolicyRepresentation
import org.keycloak.util.JsonSerialization.mapper
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class CucumberStepDefinitions {

    companion object {
        private const val APIKEYATTR = "api-key"
    }

    private val keycloak = KeycloakBuilder.builder()
        .serverUrl(System.getenv("KEYCLOAK_ADMIN_URL") + "/auth")
        .realm("master")
        .clientId("admin-cli")
        .username("admin")
        .password(System.getenv("KEYCLOAK_ADMIN_PWD"))
        .build().apply { this.realm(System.getenv("CROSSREF_REALM")) }

    private val realmResource = keycloak.realm(System.getenv("CROSSREF_REALM"))
    private val orgsGroup = realmResource.getGroupByPath("/organizations").id
    private var status = 0
    private var keyIssuedTo = ""

    private fun createOrganization(orgName: String) {
        try {
            realmResource.getGroupByPath("/organizations/$orgName").also {
                realmResource.groups().group(it.id).remove()
            }
        } catch (_: Exception) {}

        val newGroup = GroupRepresentation().apply {
            this.name = orgName
        }

        CreatedResponseUtil.getCreatedId(
            realmResource.groups().add(newGroup)
        ).apply {
            newGroup.id = this
            realmResource.groups().group(orgsGroup).subGroup(newGroup)
        }
    }

    private fun disableApiKey(orgName: String) {
        with(realmResource.users().search("${orgName}_apikey")) {
            if (this.size != 0) {
                val key = this.first()
                key.isEnabled = false
                realmResource.users().get(key.id).update(key)
            }
        }
    }

    /**
     * Api Keys are supposed to be SHA256 hash codes. Because keycloak and this extension
     * don't really care about the content, they only interpret it as a string, for simplicity
     * we use the organization name as api-key value.
     */
    private fun createApiKey(orgName: String) {
        with(realmResource.users().search("${orgName}_apikey")) {
            if (this.size != 0) {
                realmResource.users().delete(this.first().id)
            }
            with(UserRepresentation()) {
                this.username = "$orgName@apikey.com"
                this.email = "$orgName@apikey.com"
                this.attributes = mapOf(APIKEYATTR to listOf(orgName))
                this.isEnabled = true
                this.isEmailVerified = true
                with(realmResource.users().create(this)) {
                    val userId = CreatedResponseUtil.getCreatedId(this)
                    with(realmResource.users().get(userId)) {
                        this.roles().realmLevel().add(listOf(realmResource.roles().get(Role.APIKEY.roleName).toRepresentation()))
                        this.joinGroup(realmResource.getGroupByPath("/organizations/$orgName").id)
                    }
                }
            }
        }
    }

    private fun getPolicyForResource(clientName: String, resourceName: String): Pair<PolicyResource, PolicyRepresentation> =
        realmResource.clients().findByClientId(clientName).first().id.let { clientId ->
            realmResource.clients().get(clientId).authorization().policies().findByName("$resourceName-access-policy").let {
                Pair(realmResource.clients().get(clientId).authorization().policies().policy(it.id), it)
            }
        }

    private fun addGroupToPolicy(groupId: String, policyResource: PolicyResource, policyRepresentation: PolicyRepresentation) {
        val groups = mapper.readValue(policyRepresentation.config.getOrDefault("groups", "[]"), object : TypeReference<MutableList<Map<String, Any>>>() {})
        if (groups.none { it.getOrDefault("id", "") == groupId }) {
            groups.add(mapOf<String, Any>("id" to groupId, "extendChildren" to false))
            policyRepresentation.config["groups"] = mapper.writeValueAsString(groups)
            policyResource.update(policyRepresentation)
        }
    }

    private fun removeGroupFromPolicy(groupId: String, policyResource: PolicyResource, policyRepresentation: PolicyRepresentation) {
        val groups = mapper.readValue(policyRepresentation.config.getOrDefault("groups", "[]"), object : TypeReference<MutableList<Map<String, Any>>>() {})
        if (groups.any { it.getOrDefault("id", "") == groupId }) {
            policyRepresentation.config["groups"] = mapper.writeValueAsString(groups.filterNot { it.getOrDefault("id", "") == groupId })
            policyResource.update(policyRepresentation)
        }
    }

    private fun setOrgPlusSubscriptionStatus(orgName: String, status: Boolean) {
        val policies = listOf(
            getPolicyForResource(RESOURCES_CLIENT, "metadata-plus-data"),
        )

        val orgId = realmResource.getGroupByPath("/organizations/$orgName").id

        if (status) {
            policies.forEach { (policyResource, policyRepresentation) ->
                addGroupToPolicy(orgId, policyResource, policyRepresentation)
            }
        } else {
            policies.forEach { (policyResource, policyRepresentation) ->
                removeGroupFromPolicy(orgId, policyResource, policyRepresentation)
            }
        }
    }

    @Given("Psychoceramics is an organization with PLUS subscription")
    fun psychoceramics_is_an_organization_with_plus_subscription() {
        createOrganization("Psychoceramics")
        setOrgPlusSubscriptionStatus("Psychoceramics", true)
    }

    @Given("Carberry is an organization without PLUS subscription")
    fun carberry_is_an_organization_without_plus_subscription() {
        createOrganization("Carberry")
    }

    @Given("an API-Key which was issued to Psychoceramics")
    fun an_api_key_which_was_issued_to_psychoceramics() {
        createApiKey("Psychoceramics")
        keyIssuedTo = "Psychoceramics"
    }

    @Given("an API-Key which was issued to Carberry")
    fun an_api_key_which_was_issued_to_carberry() {
        createApiKey("Carberry")
        keyIssuedTo = "Carberry"
    }

    @Given("Psychoceramics PLUS subscription was revoked")
    fun psychoceramics_plus_subscription_was_revoked() {
        setOrgPlusSubscriptionStatus("Psychoceramics", false)
    }

    @Given("the API-Key was disabled")
    fun the_api_key_was_disabled() {
        disableApiKey("Psychoceramics")
    }

    @When("the rest-api requests permission to PLUS for the API-key")
    fun the_rest_api_requests_permission_to_plus_for_the_api_key() {
        // http POST permission path with apikey and rest-api and plus resource
        // I'll get a http status return code
        val values = mapOf(
            "apiKeyHash" to keyIssuedTo,
            "clientSecret" to "**********",
            "resource" to "metadata-plus-data",
            "clientName" to RESOURCES_CLIENT
        )

        val requestBody: String = mapper
            .writeValueAsString(values)

        // This has been set so that the infinispan cache should hava a TTL below 2s, (1s ideally)
        Thread.sleep(2000)

        val client = HttpClient.newBuilder().build()
        val request = HttpRequest.newBuilder()
            .uri(URI.create("${System.getenv("KEYCLOAK_ADMIN_URL")}/auth/realms/${System.getenv("CROSSREF_REALM")}/checkApiKeyPermissions"))
            .header("Content-type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(requestBody))
            .build()
        status = client.send(request, HttpResponse.BodyHandlers.ofString()).statusCode()
    }

    @Then("the answer is {string}")
    fun the_answer_is(string: String?) {
        assert(status == 200 && string.equals("yes") || (status == 401 && string.equals("no")))
    }
}
