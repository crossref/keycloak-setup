package org.crossref.keycloak.apikeys

import com.google.common.cache.Cache
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.spyk
import org.crossref.keycloak.apikeys.resources.ApiKeyCheckResource
import org.crossref.keycloak.apikeys.resources.PermissionInfo
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.management.services.mockGroupModel
import org.keycloak.authorization.AuthorizationProvider
import org.keycloak.authorization.Decision
import org.keycloak.models.KeycloakSession
import org.keycloak.models.RoleModel
import org.keycloak.models.UserModel
import org.keycloak.util.JsonSerialization.mapper
import kotlin.test.Test

internal class UnitTests {

    private val apiKeyCache = mockk<Cache<Pair<String, String>, PermissionInfo>> {
        every { getIfPresent(any()) } returns null
        every { put(Pair("---", "name"), any()) } returns mockk()
    }

    @Test
    fun `Client secret does not match`() {
        val session = mockk<KeycloakSession> {
            every { context } returns mockk {
                every { realm } returns mockk {
                    every { getClientByClientId("name") } returns mockk {
                        every { secret } returns "different"
                    }
                }
            }
        }

        val apiKeyCheckResource = ApiKeyCheckResource(session, apiKeyCache)

        apiKeyCheckResource.checkApiKey(mapper.writeValueAsString(ApiKeyCheckResource.PermissionRequest("---", "---", "resource", "name"))).let {
            assert(it.status == 401)
            assert((it.entity as Map<String, String>)["error"].equals("Client credentials failed"))
        }
    }

    @Test
    fun `The api key is not present in the system`() {
        val session = mockk<KeycloakSession> {
            every { context } returns mockk {
                every { realm } returns mockk {
                    every { getClientByClientId("name") } returns mockk {
                        every { secret } returns "---"
                    }
                }
            }
            every { users() } returns mockk {
                every { searchForUserByUserAttributeStream(any(), "api-key", any()) } returns
                    listOf<UserModel>().stream()
            }
        }

        val apiKeyCheckResource = ApiKeyCheckResource(session, apiKeyCache)

        apiKeyCheckResource.checkApiKey(mapper.writeValueAsString(ApiKeyCheckResource.PermissionRequest("---", "---", "resource", "name"))).let {
            assert(it.status == 401)
            assert((it.entity as Map<String, String>)["access"].equals("denied"))
        }
    }

    @Test
    fun `The api key is present in the system but the api-key is disabled`() {
        val session = mockk<KeycloakSession> {
            every { context } returns mockk {
                every { realm } returns mockk {
                    every { getClientByClientId("name") } returns mockk {
                        every { secret } returns "---"
                    }
                }
            }
            every { users() } returns mockk {
                every { searchForUserByUserAttributeStream(any(), "api-key", any()) } returns
                    listOf<UserModel>(
                        mockk {
                            every { username } returns "apikey"
                            every { isEnabled } returns false
                            every { id } returns "1234"
                            every { groupsStream } returns listOf(
                                mockGroupModel("org1", 1),
                            ).stream()
                            every { realmRoleMappingsStream } returns
                                listOf<RoleModel>(
                                    mockk {
                                        every { name } returns Role.APIKEY.roleName
                                    },
                                    mockk {
                                        every { name } returns "other"
                                    },
                                ).stream()
                        },
                    ).stream()
            }
            every { getProvider(AuthorizationProvider::class.java) } returns mockk {
                every { storeFactory } returns mockk {
                    every { resourceServerStore } returns mockk {
                        every { findByClient(any()) } returns mockk {
                        }
                    }
                }
            }
        }

        val apiKeyCheckResource = ApiKeyCheckResource(session, apiKeyCache)

        val acr = spyk(apiKeyCheckResource) {
            every { createIdentity(any(), any(), any(), any()) } returns mockk {
                justRun { close() }
                every { evaluate(any(), any(), any(), any(), any()) } returns mockk {
                    every { results } returns listOf(
                        mockk {
                            every { effect } returns Decision.Effect.PERMIT
                            every { permission } returns mockk {
                                every { resource } returns mockk {
                                    every { name } returns "metadata-plus-data"
                                }
                            }
                        },
                    )
                }
                every { createEvaluationContext(any(), any(), any()) } returns mockk { }
            }
        }

        acr.checkApiKey(mapper.writeValueAsString(ApiKeyCheckResource.PermissionRequest("---", "---", "resource", "name"))).let {
            assert(it.status == 401)
            assert((it.entity as Map<String, String>)["access"].equals("denied"))
            assert((it.entity as Map<String, Long>)["memberId"]?.toInt() == 1)
        }
    }

    @Test
    fun `The api key is present in the system and api-key has permission to access the resource`() {
        val session = mockk<KeycloakSession> {
            every { context } returns mockk {
                every { realm } returns mockk {
                    every { getClientByClientId("name") } returns mockk {
                        every { secret } returns "---"
                    }
                }
            }
            every { users() } returns mockk {
                every { searchForUserByUserAttributeStream(any(), "api-key", any()) } returns
                    listOf<UserModel>(
                        mockk {
                            every { username } returns "apikey"
                            every { isEnabled } returns true
                            every { id } returns "1234"
                            every { groupsStream } returns listOf(
                                mockGroupModel("org1", 1),
                            ).stream()
                            every { realmRoleMappingsStream } returns
                                listOf<RoleModel>(
                                    mockk {
                                        every { name } returns Role.APIKEY.roleName
                                    },
                                    mockk {
                                        every { name } returns "other"
                                    },
                                ).stream()
                        },
                    ).stream()
            }
            every { getProvider(AuthorizationProvider::class.java) } returns mockk {
                every { storeFactory } returns mockk {
                    every { resourceServerStore } returns mockk {
                        every { findByClient(any()) } returns mockk {
                        }
                    }
                }
            }
        }

        val apiKeyCheckResource = ApiKeyCheckResource(session, apiKeyCache)

        val acr = spyk(apiKeyCheckResource) {
            every { createIdentity(any(), any(), any(), any()) } returns mockk {
                justRun { close() }
                every { evaluate(any(), any(), any(), any(), any()) } returns mockk {
                    every { results } returns listOf(
                        mockk {
                            every { effect } returns Decision.Effect.PERMIT
                            every { permission } returns mockk {
                                every { resource } returns mockk {
                                    every { name } returns "metadata-plus-data"
                                }
                            }
                        },
                    )
                }
                every { createEvaluationContext(any(), any(), any()) } returns mockk { }
            }
        }

        acr.checkApiKey(mapper.writeValueAsString(ApiKeyCheckResource.PermissionRequest("---", "---", "metadata-plus-data", "name"))).let {
            assert(it.status == 200)
            assert((it.entity as Map<String, String>)["access"].equals("granted"))
        }
    }

    @Test
    fun `The api key is present in the system but the api-key doesn't have permission to access the resource`() {
        val session = mockk<KeycloakSession> {
            every { context } returns mockk {
                every { realm } returns mockk {
                    every { getClientByClientId("name") } returns mockk {
                        every { secret } returns "---"
                    }
                }
            }
            every { users() } returns mockk {
                every { searchForUserByUserAttributeStream(any(), "api-key", any()) } returns
                    listOf<UserModel>(
                        mockk {
                            every { username } returns "apikey"
                            every { isEnabled } returns true
                            every { groupsStream } returns listOf(
                                mockGroupModel("org1", 1),
                            ).stream()
                            every { id } returns "1234"
                            every { realmRoleMappingsStream } returns
                                listOf<RoleModel>(
                                    mockk {
                                        every { name } returns Role.APIKEY.roleName
                                    },
                                    mockk {
                                        every { name } returns "other"
                                    },
                                ).stream()
                        },
                    ).stream()
            }
            every { getProvider(AuthorizationProvider::class.java) } returns mockk {
                every { storeFactory } returns mockk {
                    every { resourceServerStore } returns mockk {
                        every { findByClient(any()) } returns mockk {
                        }
                    }
                }
            }
        }

        val apiKeyCheckResource = ApiKeyCheckResource(session, apiKeyCache)

        val acr = spyk(apiKeyCheckResource) {
            every { createIdentity(any(), any(), any(), any()) } returns mockk {
                justRun { close() }
                every { evaluate(any(), any(), any(), any(), any()) } returns mockk {
                    every { results } returns listOf(
                        mockk {
                            every { effect } returns Decision.Effect.PERMIT
                            every { permission } returns mockk {
                                every { resource } returns mockk {
                                    every { name } returns "not-plus-data"
                                }
                            }
                        },
                    )
                }
                every { createEvaluationContext(any(), any(), any()) } returns mockk { }
            }
        }

        acr.checkApiKey(mapper.writeValueAsString(ApiKeyCheckResource.PermissionRequest("---", "---", "metadata-plus-data", "name"))).let {
            assert(it.status == 401)
            assert((it.entity as Map<String, String>)["access"].equals("denied"))
        }
    }
}
