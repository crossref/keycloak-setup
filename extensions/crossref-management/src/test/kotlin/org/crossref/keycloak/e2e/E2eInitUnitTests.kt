package org.crossref.keycloak.e2e

import org.apache.http.HttpStatus
import org.crossref.keycloak.ManagementClient
import org.crossref.keycloak.management.Subscription
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.keycloak.util.JsonSerialization
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@TestMethodOrder(OrderAnnotation::class)
class E2eInitTests : ManagementClient() {
    companion object {
        val staffUser = UserDef("staff@staff.com", "STAFF")
        val normalUser = UserDef("user@user.com", "USER")
        val normalUser2 = UserDef("user2@user.com", "USER")
    }

    fun createOrg(orgName: String, memberId: String, token: String) {
        callKeycloakEndpoint(
            "/organization",
            token,
            JsonSerialization.mapper.writeValueAsString(
                mapOf(
                    "name" to orgName,
                    "orgId" to "79283140-2FC4-4D5C-AED1-7D30CF34E8DB",
                    "memberId" to memberId?.toLong()
                )
            ),
            "POST"
        )
    }

    @Test
    @Order(1)
    fun createUsers() {
        deleteOrgsAndUsers()
        createUser(staffUser)
        createUser(normalUser)
        createUser(normalUser2)

        assertTrue(userToken.contains(normalUser.username))
        assertTrue(userToken.contains(normalUser2.username))
        assertTrue(userToken.contains(staffUser.username))
    }

    @Test
    @Order(2)
    fun createOrgs() {
        loginUser("staff@staff.com")
        loginUser("user@user.com")
        createOrg("org1", "1111", userToken["staff@staff.com"]!!)
        assertEquals(status, HttpStatus.SC_CREATED)
        createOrg("org2", "2222", userToken["staff@staff.com"]!!)
        assertEquals(status, HttpStatus.SC_CREATED)
    }

    @Test
    @Order(3)
    fun enablePlusOrg1() {
        loginUser("staff@staff.com")
        managerAddSubscriptionToOrg("METADATA_PLUS", "1111", userToken["staff@staff.com"]!!)
        assertEquals(status, HttpStatus.SC_NO_CONTENT)
    }

    @Test
    @Order(4)
    fun addUsersToOrgs() {
        loginUser("staff@staff.com")
        managerAddUserToOrg("user@user.com", "1111", userToken["staff@staff.com"]!!)
        assertEquals(status, HttpStatus.SC_NO_CONTENT)
        managerAddUserToOrg("user2@user.com", "2222", userToken["staff@staff.com"]!!)
        assertEquals(status, HttpStatus.SC_NO_CONTENT)
    }

    @Test
    @Order(5)
    fun createApiKey() {
        loginUser("user@user.com")
        managerCreateApiKey("1111", userToken["user@user.com"]!!, "")
        assertEquals(status, HttpStatus.SC_CREATED)
    }
}

@TestMethodOrder(OrderAnnotation::class)
class E2eUsageTests : ManagementClient() {
    @Test
    @Order(1)
    fun tryapikey1() {
        loginUser("user@user.com")
        loginUser("user2@user.com")
        loginUser("staff@staff.com")
        var hash = managerCreateApiKey("1111", userToken["user@user.com"]!!, "")
        var hash2 = managerCreateApiKey("2222", userToken["user2@user.com"]!!, "")
        checkPermission(hash, Subscription.METADATA_PLUS)
        assertEquals(status, HttpStatus.SC_OK)
        checkPermission(hash2, Subscription.METADATA_PLUS)
        assertEquals(status, HttpStatus.SC_UNAUTHORIZED)

        managerRemoveSubscriptionToOrg("METADATA_PLUS", "1111", userToken["staff@staff.com"]!!)
        assertEquals(status, HttpStatus.SC_NO_CONTENT)
        managerAddSubscriptionToOrg("METADATA_PLUS", "2222", userToken["staff@staff.com"]!!)
        assertEquals(status, HttpStatus.SC_NO_CONTENT)

        hash = managerCreateApiKey("1111", userToken["user@user.com"]!!, "")
        hash2 = managerCreateApiKey("2222", userToken["user2@user.com"]!!, "")

        checkPermission(hash, Subscription.METADATA_PLUS)
        assertEquals(status, HttpStatus.SC_UNAUTHORIZED)
        checkPermission(hash2, Subscription.METADATA_PLUS)
        assertEquals(status, HttpStatus.SC_OK)

        managerRemoveSubscriptionToOrg("METADATA_PLUS", "2222", userToken["staff@staff.com"]!!)
        assertEquals(status, HttpStatus.SC_NO_CONTENT)
        managerAddSubscriptionToOrg("METADATA_PLUS", "1111", userToken["staff@staff.com"]!!)
        assertEquals(status, HttpStatus.SC_NO_CONTENT)
    }
}
