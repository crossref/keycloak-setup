package org.crossref.keycloak.management.services

import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.spyk
import jakarta.ws.rs.ForbiddenException
import jakarta.ws.rs.NotAuthorizedException
import org.crossref.keycloak.management.APIKey
import org.crossref.keycloak.management.CrossrefManagementConfiguration
import org.crossref.keycloak.management.Organization
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.management.Subscription
import org.crossref.keycloak.management.SyncOperation
import org.crossref.keycloak.management.User
import org.crossref.keycloak.management.UserSyncMessage
import org.crossref.keycloak.management.resources.CrossrefManagementResource
import org.crossref.keycloak.util.Constants.Crossref.Companion.CROSSREF_UI_CLIENT
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.keycloak.models.KeycloakSession
import org.keycloak.representations.AccessToken
import java.util.UUID
import kotlin.test.assertEquals

class CrossrefManagementResourceUnitTests {

    fun mockCredentials(cmr: CrossrefManagementResource, role: Role?, uid: UUID? = null) {
        if (role == null) {
            every { cmr invoke "getAccessToken" withArguments listOf() } returns null
        } else {
            every { cmr invoke "getAccessToken" withArguments listOf() } returns mockk<AccessToken> {
                every { realmAccess.isUserInRole(any()) } answers { firstArg<String>() == role.roleName }
                every { subject } returns (uid ?: UUID.randomUUID()).toString()
            }
        }
    }

    fun mockCrossrefManagementResource(): Pair<KeycloakSession, CrossrefManagementResource> {
        mockk<KeycloakSession> {
            every { context.realm.id } returns UUID.randomUUID().toString()
            every { context.realm.enabledEventTypesStream } returns listOf<String>().stream()
            every { groups() } returns mockk {}
            every { context.realm.isEventsEnabled } returns false
            every { context.realm.eventsListenersStream } returns listOf<String>().stream()
            every { context.connection.remoteAddr } returns ""
            every { keycloakSessionFactory } returns mockk { }
            every { context.realm.getClientByClientId(CROSSREF_UI_CLIENT).webOrigins } returns setOf()
        }.let {
            return Pair(
                it,
                spyk(
                    CrossrefManagementResource(it, CrossrefManagementConfiguration("", "", "")),
                    recordPrivateCalls = true
                )
            )
        }
    }

    @Test
    fun `Find org by member id works without access token`() {
        val (_, cmr) = mockCrossrefManagementResource()

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { findOrgByMemberId(any()) } returns mockk {}
        }

        cmr.getOrg(123, originHeader = "localhost")
    }

    @Test
    fun `Find all orgs (and by name) works without access token`() {
        val (_, cmr) = mockCrossrefManagementResource()

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { findOrgs(any(), any()) } returns mockk {}
        }

        cmr.findOrgs("", 1, originHeader = "localhost")
        cmr.findOrgs("aa", 1, originHeader = "localhost")
    }

    @Test
    fun `Create an Org requires STAFF role`() {
        val (_, cmr) = mockCrossrefManagementResource()

        mockCredentials(cmr, null)
        assertThrows(NotAuthorizedException::class.java) {
            cmr.addOrg(Organization(UUID.randomUUID(), "name", 123), originHeader = "localhost")
        }

        mockCredentials(cmr, Role.STAFF)
        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { addOrg(any()).name } returns "name"
            every { addOrg(any()).memberId } returns 123
            every { addOrg(any()).orgId } returns UUID.randomUUID()
        }

        cmr.addOrg(Organization(UUID.randomUUID(), "name", 123), originHeader = "localhost")
    }

    @Test
    fun `Update an Org requires STAFF role`() {
        val (_, cmr) = mockCrossrefManagementResource()

        mockCredentials(cmr, null)

        assertThrows(NotAuthorizedException::class.java) {
            cmr.addOrg(Organization(UUID.randomUUID(), "name", 123), originHeader = "localhost")
        }

        mockCredentials(cmr, Role.STAFF)
        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { updateOrg(any()) } returns mockk {
                every { orgId } returns UUID.randomUUID()
            }
        }

        cmr.updateOrg(Organization(UUID.randomUUID(), "name", 123), originHeader = "localhost")
    }

    @Test
    fun `listuserinorgs works as USER`() {
        val uid = UUID.randomUUID()
        val userModel = mockUserModel(
            "user@crossref.org",
            uid,
            listOf(
                mockGroupModel("org1", 123),
                mockGroupModel("org2", 234)
            )
        )

        val (session, cmr) = mockCrossrefManagementResource()

        every { session.users() } returns mockk {
            every { getUserById(any(), uid.toString()) } returns userModel
        }
        every { session.context } returns mockk {
            every { realm } returns mockk {
                every { getClientByClientId(CROSSREF_UI_CLIENT).webOrigins } returns setOf()
            }
        }

        mockCredentials(cmr, Role.USER, uid)

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { getUsersFromOrg(any(), any(), any()) } returns mockk {}
        }

        cmr.listUsersInOrg(123, 1, 10, originHeader = "localhost")
    }

    @Test
    fun `listuserinorgs works as STAFF not belonging to any org`() {
        val uid = UUID.randomUUID()
        val userModel = mockUserModel(
            "user@crossref.org",
            uid
        )

        val (session, cmr) = mockCrossrefManagementResource()

        every { session.users().getUserById(any(), uid.toString()) } returns userModel

        mockCredentials(cmr, Role.STAFF, uid)

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { getUsersFromOrg(any(), any(), any()) } returns mockk {}
        }

        cmr.listUsersInOrg(123, 1, 10, originHeader = "localhost")
    }

    @Test
    fun `listuserinorgs fails as USER when not belonging to any org`() {
        val uid = UUID.randomUUID()
        val userModel = mockUserModel(
            "user@crossref.org",
            uid
        )

        val (session, cmr) = mockCrossrefManagementResource()

        every { session.users().getUserById(any(), uid.toString()) } returns userModel

        mockCredentials(cmr, Role.USER, uid)

        assertThrows(ForbiddenException::class.java) {
            cmr.listUsersInOrg(123, 1, 10, originHeader = "localhost")
        }
    }

    @Test
    fun `STAFF can list all users`() {
        val (session, cmr) = mockCrossrefManagementResource()

        every { session.context.realm.getRole(Role.USER.roleName) } returns mockk {}

        mockCredentials(cmr, Role.STAFF)

        cmr.getUsers("", 1, 10, originHeader = "localhost")
    }

    @Test
    fun `non STAFF cannot list all users`() {
        val (_, cmr) = mockCrossrefManagementResource()

        mockCredentials(cmr, Role.USER)

        assertThrows(ForbiddenException::class.java) {
            cmr.getUsers("", 1, 10, originHeader = "localhost")
        }

        mockCredentials(cmr, null)

        assertThrows(NotAuthorizedException::class.java) {
            cmr.getUsers("", 1, 10, originHeader = "localhost")
        }
    }

    @Test
    fun `create user works as STAFF`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.STAFF)

        every { cmr getProperty "userManagerService" } returns mockk<UserManagerService> {
            every { addUser(user, Role.USER) } returns mockk {}
        }

        cmr.addUser(user, Role.USER, originHeader = "localhost")
    }

    @Test
    fun `create user doesn't work if caller is not STAFF`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.USER)

        assertThrows(ForbiddenException::class.java) {
            cmr.addUser(user, Role.USER, originHeader = "localhost")
        }
        mockCredentials(cmr, null)

        assertThrows(NotAuthorizedException::class.java) {
            cmr.addUser(user, Role.USER, originHeader = "localhost")
        }
    }

    @Test
    fun `delete user works as STAFF`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.STAFF)

        every { cmr getProperty "userManagerService" } returns mockk<UserManagerService> {
            every { delUser(user.userId!!) } returns mockk {}
        }

        cmr.deleteUser(user.userId!!, originHeader = "localhost")
    }

    @Test
    fun `delete user doesn't work if caller is not STAFF`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.USER)

        assertThrows(ForbiddenException::class.java) {
            cmr.deleteUser(user.userId!!, originHeader = "localhost")
        }
        mockCredentials(cmr, null)

        assertThrows(NotAuthorizedException::class.java) {
            cmr.deleteUser(user.userId!!, originHeader = "localhost")
        }
    }

    @Test
    fun `add user to org works as STAFF`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.STAFF)

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { addUserToOrg(123, user.userId!!) } returns mockk {}
        }

        cmr.addUserToOrg(123, user.userId!!, originHeader = "localhost")
    }

    @Test
    fun `add user to work doesn't work if caller is not STAFF`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.USER)

        assertThrows(ForbiddenException::class.java) {
            cmr.addUserToOrg(123, user.userId!!, originHeader = "localhost")
        }
        mockCredentials(cmr, null)

        assertThrows(NotAuthorizedException::class.java) {
            cmr.addUserToOrg(123, user.userId!!, originHeader = "localhost")
        }
    }

    @Test
    fun `remove user from org works as STAFF`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.STAFF)

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { removeUserFromOrg(123, user.userId!!) } returns mockk {}
        }

        cmr.removeUserFromOrg(123, user.userId!!, originHeader = "localhost")
    }

    @Test
    fun `remove user from work doesn't work if caller is not STAFF`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.USER)

        assertThrows(ForbiddenException::class.java) {
            cmr.removeUserFromOrg(123, user.userId!!, originHeader = "localhost")
        }
        mockCredentials(cmr, null)

        assertThrows(NotAuthorizedException::class.java) {
            cmr.removeUserFromOrg(123, user.userId!!, originHeader = "localhost")
        }
    }

    @Test
    fun `list User Organizations as STAFF works`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.STAFF)

        cmr.listUserOrgs(user.userId!!, "", 1, 10, originHeader = "localhost")
    }

    @Test
    fun `list User Organizations as user works`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.USER, user.userId)

        cmr.listUserOrgs(user.userId!!, "", 1, 10, originHeader = "localhost")
    }

    @Test
    fun `list User Organizations as different user fails`() {
        val (_, cmr) = mockCrossrefManagementResource()
        val user = User(UUID.randomUUID(), "user@company.com")

        mockCredentials(cmr, Role.USER)

        assertThrows(ForbiddenException::class.java) {
            cmr.listUserOrgs(user.userId!!, "", 1, 10, originHeader = "localhost")
        }
    }

    @Test
    fun `Add api Key works as User`() {
        val orgId = UUID.randomUUID()
        val subject = UUID.randomUUID()

        val (session, cmr) = mockCrossrefManagementResource()

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { findOrgByMemberId(123).orgId } returns orgId
        }
        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { addKey(any(), orgId).keyObject.name } returns UUID.randomUUID().toString()
        }

        mockCredentials(cmr, Role.USER, subject)

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject,
            listOf(
                mockGroupModel("org", 123)
            )
        )

        cmr.addAPIKey(APIKey("descr", UUID.randomUUID()), 123, originHeader = "localhost")
    }

    @Test
    fun `Add api Key fails as Staff`() {
        val (_, cmr) = mockCrossrefManagementResource()

        mockCredentials(cmr, Role.STAFF)

        assertThrows(ForbiddenException::class.java) { cmr.addAPIKey(APIKey("descr", UUID.randomUUID()), 123, originHeader = "localhost") }
    }

    @Test
    fun `Add api Key fails as User if the user does not belong to the org`() {
        val (session, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()

        mockCredentials(cmr, Role.USER, subject)

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject,
            listOf(
                mockGroupModel("org", 124)
            )
        )

        assertThrows(ForbiddenException::class.java) { cmr.addAPIKey(APIKey("descr", UUID.randomUUID()), 123, originHeader = "localhost") }
    }

    @Test
    fun `List api works for user`() {
        val (session, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()
        val orgId = UUID.randomUUID()

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject,
            listOf(
                mockGroupModel("org", 123)
            )
        )

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { findOrgByMemberId(123).orgId } returns orgId
        }
        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { listKeys(orgId, 1, 10) } returns mockk {}
        }

        mockCredentials(cmr, Role.USER, subject)
        cmr.listAPIKeys(123, 1, 10, originHeader = "localhost")
    }

    @Test
    fun `List api works for staff`() {
        val (session, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()
        UUID.randomUUID()

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject
        )

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { findOrgByMemberId(124).orgId } returns UUID.randomUUID()
        }
        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { listKeys(any(), 1, 10) } returns mockk {}
        }

        mockCredentials(cmr, Role.STAFF, subject)
        cmr.listAPIKeys(124, 1, 10, originHeader = "localhost")
    }

    @Test
    fun `List api fails for user if they dont't belong to the same org`() {
        val (session, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()
        val orgId = UUID.randomUUID()

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject,
            listOf(
                mockGroupModel("org", 124)
            )
        )

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { findOrgByMemberId(123).orgId } returns orgId
        }
        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { listKeys(orgId, 1, 10) } returns mockk {}
        }

        mockCredentials(cmr, Role.USER, subject)
        assertThrows(ForbiddenException::class.java) { cmr.listAPIKeys(123, 1, 10, originHeader = "localhost") }
    }

    @Test
    fun `Staff can enable key`() {
        val (_, cmr) = mockCrossrefManagementResource()

        val keyId = UUID.randomUUID()

        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { setKeyEnabled(keyId, any()) } returns mockk {}
        }

        mockCredentials(cmr, Role.STAFF)
        cmr.enableAPIKey(keyId, originHeader = "localhost")
    }

    @Test
    fun `User cannot enable key`() {
        val (_, cmr) = mockCrossrefManagementResource()

        val keyId = UUID.randomUUID()

        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { setKeyEnabled(keyId, any()) } returns mockk {}
        }

        mockCredentials(cmr, Role.USER)
        assertThrows(ForbiddenException::class.java) { cmr.enableAPIKey(keyId, originHeader = "localhost") }
    }

    @Test
    fun `Staff can disable key`() {
        val (_, cmr) = mockCrossrefManagementResource()

        val keyId = UUID.randomUUID()

        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { setKeyEnabled(keyId, any()) } returns mockk {}
        }

        mockCredentials(cmr, Role.STAFF)
        cmr.disableAPIKey(keyId, originHeader = "localhost")
    }

    @Test
    fun `User cannot disable key`() {
        val (_, cmr) = mockCrossrefManagementResource()

        val keyId = UUID.randomUUID()

        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { setKeyEnabled(keyId, any()) } returns mockk {}
        }

        mockCredentials(cmr, Role.USER)
        assertThrows(ForbiddenException::class.java) { cmr.disableAPIKey(keyId, originHeader = "localhost") }
    }

    @Test
    fun `User can delete key`() {
        val (session, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()
        val keyId = UUID.randomUUID()

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject,
            listOf(
                mockGroupModel("org", 123)
            )
        )

        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { getApiKeyOrganization(keyId) } returns Organization(UUID.randomUUID(), "org", 123)
            every { deleteKey(keyId) } returns mockk {}
        }

        mockCredentials(cmr, Role.USER, subject)
        cmr.deleteAPIKey(keyId, originHeader = "localhost")
    }

    @Test
    fun `Staff cannot delete key`() {
        val (_, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()
        val keyId = UUID.randomUUID()

        mockCredentials(cmr, Role.STAFF, subject)
        assertThrows(ForbiddenException::class.java) { cmr.deleteAPIKey(keyId, originHeader = "localhost") }
    }

    @Test
    fun `User cannot delete key from an org he doesn't belong to`() {
        val (session, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()
        val keyId = UUID.randomUUID()

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject,
            listOf(
                mockGroupModel("org", 124)
            )
        )

        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { getApiKeyOrganization(keyId) } returns Organization(UUID.randomUUID(), "org", 123)
            every { deleteKey(keyId) } returns mockk {}
        }

        mockCredentials(cmr, Role.STAFF, subject)
        assertThrows(ForbiddenException::class.java) { cmr.deleteAPIKey(keyId, originHeader = "localhost") }
    }

    @Test
    fun `User can update key`() {
        val (session, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()
        val keyId = UUID.randomUUID()
        val key = APIKey("descr", UUID.randomUUID(), keyId)

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject,
            listOf(
                mockGroupModel("org", 123)
            )
        )

        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { getApiKeyOrganization(keyId) } returns Organization(UUID.randomUUID(), "org", 123)
            every { updateKey(key) } returns mockk {}
        }

        mockCredentials(cmr, Role.USER, subject)
        cmr.updateAPIKey(key, originHeader = "localhost")
    }

    @Test
    fun `Staff cannot update key`() {
        val (_, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()
        val keyId = UUID.randomUUID()
        val key = APIKey("descr", UUID.randomUUID(), keyId)

        mockCredentials(cmr, Role.STAFF, subject)
        assertThrows(ForbiddenException::class.java) { cmr.updateAPIKey(key, originHeader = "localhost") }
    }

    @Test
    fun `User cannot update key from an org he doesn't belong to`() {
        val (session, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()
        val keyId = UUID.randomUUID()
        val key = APIKey("descr", UUID.randomUUID(), keyId)

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject,
            listOf(
                mockGroupModel("org", 124)
            )
        )

        every { cmr getProperty "keyManagerService" } returns mockk<APIKeyManagerService> {
            every { getApiKeyOrganization(keyId) } returns Organization(UUID.randomUUID(), "org", 123)
            every { updateKey(key) } returns mockk {}
        }

        mockCredentials(cmr, Role.STAFF, subject)
        assertThrows(ForbiddenException::class.java) { cmr.updateAPIKey(key, originHeader = "localhost") }
    }

    @Test
    fun `User cannot add or delete subscription to an org`() {
        val (_, cmr) = mockCrossrefManagementResource()

        mockCredentials(cmr, Role.USER)

        assertThrows(ForbiddenException::class.java) {
            cmr.addOrgSubscription(123, Subscription.METADATA_PLUS, originHeader = "localhost")
        }

        assertThrows(ForbiddenException::class.java) {
            cmr.removeOrgSubscription(123, Subscription.METADATA_PLUS, originHeader = "localhost")
        }
    }

    @Test
    fun `Staff can add or remove subscriptions to an org`() {
        val (_, cmr) = mockCrossrefManagementResource()

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { addOrgSubscription(123, Subscription.METADATA_PLUS) } returns mockk {}
            every { delOrgSubscription(123, Subscription.METADATA_PLUS) } returns mockk {}
        }

        mockCredentials(cmr, Role.STAFF)

        cmr.addOrgSubscription(123, Subscription.METADATA_PLUS, originHeader = "localhost")

        val (_, cmr2) = mockCrossrefManagementResource()

        every { cmr2 getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { addOrgSubscription(123, Subscription.METADATA_PLUS) } returns mockk {}
            every { delOrgSubscription(123, Subscription.METADATA_PLUS) } returns mockk {}
        }

        mockCredentials(cmr2, Role.STAFF)

        cmr2.removeOrgSubscription(123, Subscription.METADATA_PLUS, originHeader = "localhost")
    }

    @Test
    fun `Staff can list org subscriptions`() {
        val (_, cmr) = mockCrossrefManagementResource()

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { getOrgSubscriptions(123) } returns listOf(Subscription.METADATA_PLUS)
        }

        mockCredentials(cmr, Role.STAFF)

        assertEquals(cmr.getOrgSubscription(123, originHeader = "localhost").entity, listOf(Subscription.METADATA_PLUS))
    }

    @Test
    fun `User can list org subscriptions if belongs to such org`() {
        val (session, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject,
            listOf(
                mockGroupModel("org", 123)
            )
        )

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { getOrgSubscriptions(123) } returns listOf(Subscription.METADATA_PLUS)
        }

        mockCredentials(cmr, Role.USER, subject)

        assertEquals(cmr.getOrgSubscription(123, originHeader = "localhost").entity, listOf(Subscription.METADATA_PLUS))
    }

    @Test
    fun `User cannot list org subscriptions if doesn't belong to such org`() {
        val (session, cmr) = mockCrossrefManagementResource()

        val subject = UUID.randomUUID()

        every { session.users().getUserById(any(), subject.toString()) } returns mockUserModel(
            "a@b.com",
            subject,
            listOf(
                mockGroupModel("org", 124)
            )
        )

        every { cmr getProperty "orgManagerService" } returns mockk<OrgManagerService> {
            every { getOrgSubscriptions(123) } returns listOf(Subscription.METADATA_PLUS)
        }

        mockCredentials(cmr, Role.USER, subject)

        assertThrows(ForbiddenException::class.java) {
            cmr.getOrgSubscription(123, originHeader = "localhost")
        }
    }

    @Test
    fun `user with PWDSYNC role can update sync users`() {
        val (_, cmr) = mockCrossrefManagementResource()

        every { cmr getProperty "userManagerService" } returns mockk<UserManagerService> {
            justRun { syncUser(any(), any()) }
        }

        mockCredentials(cmr, Role.PWDSYNC)

        cmr.userSync(UserSyncMessage("name", SyncOperation.SYNC, "", "", "", 1), originHeader = "localhost")
    }

    @Test
    fun `user with no PWDSYNC role cannot update sync users`() {
        val (_, cmr) = mockCrossrefManagementResource()

        every { cmr getProperty "userManagerService" } returns mockk<UserManagerService> {
            justRun { syncUser(any(), any()) }
        }

        mockCredentials(cmr, null)
        assertThrows(NotAuthorizedException::class.java) {
            cmr.userSync(UserSyncMessage("name", SyncOperation.SYNC, "", "", "", 1), originHeader = "localhost")
        }

        mockCredentials(cmr, Role.USER)
        assertThrows(ForbiddenException::class.java) {
            cmr.userSync(UserSyncMessage("name", SyncOperation.SYNC, "", "", "", 1), originHeader = "localhost")
        }
        mockCredentials(cmr, Role.STAFF)
        assertThrows(ForbiddenException::class.java) {
            cmr.userSync(UserSyncMessage("name", SyncOperation.SYNC, "", "", "", 1), originHeader = "localhost")
        }
        mockCredentials(cmr, Role.APIKEY)
        assertThrows(ForbiddenException::class.java) {
            cmr.userSync(UserSyncMessage("name", SyncOperation.SYNC, "", "", "", 1), originHeader = "localhost")
        }
    }
}
