package org.crossref.keycloak.management.services

import com.fasterxml.jackson.core.type.TypeReference
import io.cucumber.datatable.DataTable
import io.cucumber.java.Before
import io.cucumber.java.DataTableType
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import jakarta.ws.rs.InternalServerErrorException
import jakarta.ws.rs.NotAuthorizedException
import org.crossref.keycloak.ManagementClient
import org.crossref.keycloak.management.APIKey
import org.crossref.keycloak.management.APIKeyResponse
import org.crossref.keycloak.management.Organization
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.management.Subscription
import org.crossref.keycloak.management.User
import org.crossref.keycloak.util.Constants
import org.crossref.keycloak.util.Constants.Crossref.Companion.CROSSREF_UI_CLIENT
import org.junit.jupiter.api.Assertions.assertThrows
import org.keycloak.admin.client.CreatedResponseUtil
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.representations.idm.GroupRepresentation
import org.keycloak.representations.idm.UserRepresentation
import org.keycloak.util.JsonSerialization.mapper
import java.util.UUID
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class CrossrefManagementStepDefinitions : ManagementClient() {

    private var createdOrg: String? = null
    private var newName: String? = null
    private val foundOrgs = mutableSetOf<Long>()

    private var listReturned = false

    private val orgsGroup = realmResource.getGroupByPath("/organizations").id
    private val assignedTo = mutableSetOf<String>()

    // Api Keys created via keycloak API, used only for testing key management features
    // to keep track of Key ID's
    private val apiKeys = mutableMapOf<String, UserRepresentation>()

    // User by subscription manager to keep key secrets to check whether API key permission
    // check service works.
    private val apiKeySecrets = mutableMapOf<String, String>()

    private var newDescription: String = ""

    @Before
    internal fun before_or_after_all() {
        deleteOrgsAndUsers()

        callAuthenticatorEndpoint("/api/test/deluser/bill@psychoceramics.org", null)
        callAuthenticatorEndpoint("/api/test/deluser/ameera@psychoceramics.org", null)
        callAuthenticatorEndpoint("/api/test/deluser/jordan@psychoceramics.org", null)
        callAuthenticatorEndpoint("/api/test/deluser/peter@psychoceramics.org", null)

        Thread.sleep(4000)
    }

    @DataTableType
    fun orgEntry(orgEntry: Map<String, String>): Organization =
        Organization(UUID.randomUUID(), orgEntry["Name"].toString(), orgEntry["Member ID"]?.toLong() ?: -1)

    @DataTableType
    fun subscriptionEntry(subscriptionEntry: Map<String, String>): Subscription =
        Subscription.valueOf(subscriptionEntry["Subscription ID"]!!)

    @DataTableType
    fun respEntry(respEntry: Map<String, String>): Constants.ResourceResponseType =
        Constants.ResourceResponseType.valueOf(respEntry["Type ID"].toString())

    @DataTableType
    fun userEntry(userEntry: Map<String, String>): UserDef =
        UserDef(userEntry["Email Address"].toString(), userEntry["User Type"].toString())

    @DataTableType
    fun userType(typeEntry: Map<String, String>): Role? =
        if (typeEntry["Type ID"]!! != "GUEST") Role.valueOf(typeEntry["Type ID"]!!) else null

    @DataTableType
    fun userRelationShip(relation: Map<String, String>): Pair<String, Long> =
        Pair(relation["Email Address"]!!, relation["Organization"]!!.toLong())

    private fun getGroupByMemberId(memberId: Long) =
        getOrganizationSubGroups()
            .first {
                it.attributes.getOrDefault("memberId", listOf("-1"))?.first()?.equals(memberId.toString()) ?: false
            }

    private fun joinOrg(username: String, memberId: Long) {
        realmResource.users().get(realmResource.users().search(username).first().id).joinGroup(getGroupByMemberId(memberId).id)
    }

    private fun createOrganization(org: Organization) {
        try {
            realmResource.getGroupByPath("/organizations/${org.name}").also {
                realmResource.groups().group(it.id).remove()
            }
        } catch (_: Exception) {}

        val newGroup = GroupRepresentation().apply {
            this.name = org.name
            this.attributes = mapOf(
                "memberId" to listOf(org.memberId.toString()),
                "subscriptions" to listOf("[]"),
                "descriptions" to listOf("")
            )
        }
        CreatedResponseUtil.getCreatedId(
            realmResource.groups().add(newGroup)
        ).apply {
            newGroup.id = this
            realmResource.groups().group(orgsGroup).subGroup(newGroup)
        }
    }

    private fun createAPIKey(keyId: String, memberId: Long, description: String? = null) {
        with(UserRepresentation()) {
            this.email = "apikey-" + UUID.randomUUID().toString().substring(0..8) + "@crossref.org"
            this.isEnabled = true
            this.isEmailVerified = true
            this.attributes = mapOf(
                "description" to listOf(description ?: keyId),
                "issuedBy" to listOf(UUID.randomUUID().toString())
            )
            with(realmResource.users().create(this)) {
                val userId = CreatedResponseUtil.getCreatedId(this)
                with(realmResource.users().get(userId)) {
                    apiKeys[keyId] = this.toRepresentation()
                    this.roles().realmLevel().add(listOf(realmResource.roles().get(Role.APIKEY.roleName).toRepresentation()))
                    val group = realmResource.groups().groups("", 0, 999, false).first().subGroups.first {
                        (
                            it.attributes?.get(
                                "memberId"
                            )?.first()?.toLong() ?: -1
                            ) == memberId
                    }
                    this.joinGroup(group.id)
                }
            }
        }
    }

    @Given("the following organizations")
    fun the_following_organizations(dataTable: List<Organization>) {
        dataTable.map { createOrganization(it) }
    }

    @Given("the following user types")
    fun the_following_user_types(dataTable: List<Role?>) {
    }

    @Given("the following users")
    fun the_following_users(dataTable: List<UserDef>) {
        dataTable.filter {
            it.type != "GUEST"
        }.map {
            createUser(it)
        }
    }

    @Given("the following subscription types")
    fun the_following_subscription_types(dataTable: DataTable?) {
    }

    @Given("the following response types")
    fun the_following_response_types(dataTable: List<Constants.ResourceResponseType>) {
    }

    @Given("the following user organization assignments")
    fun the_following_user_organization_assignments(dataTable: List<Pair<String, Long>>) {
        dataTable.forEach {
            joinOrg(it.first, it.second)
        }
    }

    @When("user {string} requests retrieval of the organization with Member ID {string}")
    fun user_requests_retrieval_of_the_organization_with_member_id(string: String?, string2: String?) {
        callKeycloakEndpoint("/organization/$string2", userToken[string], null)
        listReturned = false
    }

    @Then("organization {string} is returned")
    fun organization_is_returned(string: String?) {
        if (listReturned) {
            assert(mapper.readValue(returnedBody, object : TypeReference<List<Organization>>() {})!!.map { it.memberId.toString() }.toMutableSet().contains(string))
        } else {
            assert(mapper.readValue(returnedBody, object : TypeReference<Organization>() {})!!.memberId.toString() == string)
        }
        foundOrgs.add(string!!.toLong())
    }

    @Then("no other organizations are returned")
    fun no_other_organizations_are_returned() {
        if (listReturned) {
            assert(
                mapper.readValue(returnedBody, object : TypeReference<List<Organization>>() {}).none {
                    !foundOrgs.contains(it.memberId)
                }
            )
        }
    }

    @Then("response type {string} is returned")
    fun response_type_is_returned(string: String?) {
        assert(Constants.ResourceResponseType.valueOf(string!!).code == status)
    }

    @When("user {string} requests retrieval of all organizations")
    fun user_requests_retrieval_of_all_organizations(string: String?) {
        callKeycloakEndpoint("/organization", userToken[string], null)
        listReturned = true
    }

    @When("user {string} requests the creation of a new organization with Member ID {string} and Name {string}")
    fun user_requests_the_creation_of_a_new_organization_with_member_id_and_name(
        string: String?,
        string2: String?,
        string3: String?
    ) {
        // Write code here that turns the phrase above into concrete actions
        callKeycloakEndpoint(
            "/organization",
            userToken[string],
            mapper.writeValueAsString(
                mapOf(
                    "name" to string3,
                    "orgId" to "79283140-2FC4-4D5C-AED1-7D30CF34E8DB",
                    "memberId" to string2?.toLong()
                )
            ),
            "POST"
        )
        createdOrg = string3
        newName = string3
    }

    @Then("the organization is created")
    fun the_organization_is_created() {
        assert(status == 201)
        realmResource.getGroupByPath("/organizations/$createdOrg")
    }

    @Then("the organization is not created")
    fun the_organization_is_not_created() {
        // Write code here that turns the phrase above into concrete actions
        try {
            realmResource.getGroupByPath("/organizations/$createdOrg")
            throw Exception("Org found")
        } catch (e: InternalServerErrorException) {
            /* This should be javax.ws.rs.NotFoundException, but seems to be broken in keycloak 20.0+ */
        }
    }

    @Then("organization {string} is not updated")
    fun organization_is_not_updated(string: String?) {
        assert(
            !getOrganizationSubGroups()
                .first { it.attributes!!["memberId"]!![0].equals(string) }.name.equals(newName),
        )
    }

    @When("user {string} requests the organization with Member ID {string} is updated to have Name {string}")
    fun user_requests_the_organization_with_member_id_is_updated_to_have_name(
        string: String?,
        string2: String?,
        string3: String?
    ) {
        val id = getOrganizationSubGroups().first {
            it.attributes!!["memberId"]!![0].equals(
                string2
            )
        }.id

        callKeycloakEndpoint(
            "/organization",
            userToken[string],
            mapper.writeValueAsString(
                mapOf(
                    "name" to string3,
                    "orgId" to id,
                    "memberId" to 0
                )
            ),
            "PUT"
        )
        newName = string3
    }

    @Then("organization {string} is updated")
    fun organization_is_updated(string: String?) {
        // Write code here that turns the phrase above into concrete actions
        assert(
            getOrganizationSubGroups()
                .first { it.attributes!!["memberId"]!![0].equals(string) }.name.equals(newName),
        )
    }

    @Then("the user is created")
    fun the_user_is_created() {
        assert(status == Constants.ResourceResponseType.USER_CREATED.code)
    }

    @When("user {string} requests the creation of a new user with Email Address {string} and User Type {string}")
    fun user_requests_the_creation_of_a_new_user_with_email_address_and_user_type(
        string: String?,
        string2: String?,
        string3: String?
    ) {
        callKeycloakEndpoint(
            "/user?role=$string3",
            userToken[string],
            mapper.writeValueAsString(
                mapOf(
                    "username" to string2,
                    "userId" to UUID.randomUUID()
                )
            ),
            "POST"
        )
        listReturned = false
    }

    @Then("the user is not created")
    fun the_user_is_not_created() {
        assert(status != Constants.ResourceResponseType.USER_CREATED.code)
    }

    @Then("user {string} is not updated")
    fun user_is_not_updated(string: String?) {
        assert(
            !setOf(
                Constants.ResourceResponseType.RESOURCE_UPDATED.code,
                Constants.ResourceResponseType.RESOURCE_CREATED.code
            ).contains(status)
        )
    }

    @When("user {string} requests the deletion of an existing user with Email Address {string}")
    fun user_requests_the_deletion_of_an_existing_user_with_email_address(string: String?, string2: String?) {
        callKeycloakEndpoint("/user/${getUserIdByEmailAddress(string2!!)}", userToken[string], null, "DELETE")
    }

    @Then("user {string} is deleted")
    fun user_is_deleted(string: String?) {
        assert(status == Constants.ResourceResponseType.USER_DELETED.code)
        assert(getUserIdByEmailAddress(string!!) == null)
    }

    @Then("user {string} is not deleted")
    fun user_is_not_deleted(string: String?) {
        assert(status != Constants.ResourceResponseType.USER_DELETED.code)
        assert(getUserIdByEmailAddress(string!!) != null)
    }

    @When("user {string} requests that the user with Email Address {string} is assigned to the organization with Member ID {string}")
    fun user_requests_that_the_user_with_email_address_is_assigned_to_the_organization_with_member_id(
        string: String?,
        string2: String?,
        string3: String?
    ) {
        callKeycloakEndpoint("/organization/$string3/user/${getUserIdByEmailAddress(string2!!)}", userToken[string], null, "POST")
    }

    @Then("user {string} is assigned to organization {string}")
    fun user_is_assigned_to_organization(string: String?, string2: String?) {
        assert(
            realmResource.groups().group(getGroupByMemberId(string2!!.toLong()).id).members().any {
                it.username.equals(string!!)
            }
        )
        assignedTo.add(string2)
    }

    @Then("user {string} is still assigned to organization {string}")
    fun user_is_still_assigned_to_organization(string: String?, string2: String?) {
        user_is_assigned_to_organization(string, string2)
    }

    @Then("user {string} is assigned to no other organizations")
    fun user_is_assigned_to_no_other_organizations(string: String?) {
        assert(
            realmResource.users().get(getUserIdByEmailAddress(string!!)).groups(0, 1000, false).map {
                it.attributes["memberId"]?.get(0) ?: ""
            }.toSet() == assignedTo
        )
    }

    @When("user {string} requests that the user with Email Address {string} is assigned to organizations with the following member IDs")
    fun user_requests_that_the_user_with_email_address_is_assigned_to_organizations_with_the_following_member_i_ds(
        string: String?,
        string2: String?,
        dataTable: List<String>?
    ) {
        dataTable!!.drop(1).forEach {
            callKeycloakEndpoint("/organization/$it/user/${getUserIdByEmailAddress(string2!!)}", userToken[string], null, "POST")
        }
    }

    @When("user {string} requests that users with the following email addresses are assigned to the organization with Member ID {string}")
    fun user_requests_that_users_with_the_following_email_addresses_are_assigned_to_the_organization_with_member_id(
        string: String?,
        string2: String?,
        dataTable: List<String>?
    ) {
        dataTable!!.drop(1).forEach {
            callKeycloakEndpoint("/organization/$string2/user/${getUserIdByEmailAddress(it)}", userToken[string], null, "POST")
        }
    }

    @Then("user {string} is not assigned to organization {string}")
    fun user_is_not_assigned_to_organization(string: String?, string2: String?) {
        assert(
            !realmResource.groups().group(getGroupByMemberId(string2!!.toLong()).id).members().any {
                it.username.equals(string!!)
            }
        )
    }

    @When("user {string} requests that the user with Email Address {string} is unassigned from the organization with Member ID {string}")
    fun user_requests_that_the_user_with_email_address_is_unassigned_from_the_organization_with_member_id(
        string: String?,
        string2: String?,
        string3: String?
    ) {
        callKeycloakEndpoint("/organization/$string3/user/${getUserIdByEmailAddress(string2!!)}", userToken[string], null, "DELETE")
    }

    @Then("user {string} is unassigned from organization {string}")
    fun user_is_unassigned_from_organization(string: String?, string2: String?) {
        user_is_not_assigned_to_organization(string, string2)
    }

    @When("user {string} requests that the user with Email Address {string} is unassigned from organizations with the following member IDs")
    fun user_requests_that_the_user_with_email_address_is_unassigned_from_organizations_with_the_following_member_i_ds(
        string: String?,
        string2: String?,
        dataTable: List<String>?
    ) {
        dataTable!!.drop(1).forEach {
            callKeycloakEndpoint("/organization/$it/user/${getUserIdByEmailAddress(string2!!)}", userToken[string], null, "DELETE")
        }
    }

    @When("user {string} requests that users with the following email addresses are unassigned from the organization with Member ID {string}")
    fun user_requests_that_users_with_the_following_email_addresses_are_unassigned_from_the_organization_with_member_id(
        string: String?,
        string2: String?,
        dataTable: List<String>?
    ) {
        dataTable!!.drop(1).forEach {
            callKeycloakEndpoint("/organization/$string2/user/${getUserIdByEmailAddress(it)}", userToken[string], null, "DELETE")
        }
    }

    @When("user {string} requests a list of all users")
    fun user_requests_a_list_of_all_users(string: String?) {
        callKeycloakEndpoint("/user", userToken[string], null)
    }

    @Then("the list of all users is returned")
    fun the_list_of_all_users_is_returned() {
        // Write code here that turns the phrase above into concrete actions
        assert(
            mapper.readValue(returnedBody, object : TypeReference<List<User>>() {}).size ==
                realmResource.users().list().map { userRep ->
                    realmResource.users().get(userRep.id).roles().realmLevel().listEffective()
                        .map { it.toString() }
                }.filter { it.contains(Role.USER.roleName) }.size
        )
    }

    @When("user {string} requests a list of all users in organization with Member ID {string}")
    fun user_requests_a_list_of_all_users_in_organization_with_member_id(string: String?, string2: String?) {
        callKeycloakEndpoint("/organization/$string2/users", userToken[string], null)
    }

    @Then("user {string} is returned")
    fun user_is_returned(string: String?) {
        assert(
            mapper.readValue(returnedBody, object : TypeReference<List<User>>() {}).any {
                it.username == string
            }
        )
    }

    @When("user {string} requests the organizations {string} belongs to")
    fun user_requests_the_organizations_belongs_to(string: String?, string2: String?) {
        callKeycloakEndpoint("/user/${getUserIdByEmailAddress(string2!!)}/organizations", userToken[string], null)
        listReturned = true
    }

    @Then("Key {string} can be used to access Plus services")
    fun key_can_be_used_to_access_plus_services(string: String?) {
    }

    @Then("Key {string} cannot be used to access Plus services")
    fun key_cannot_be_used_to_access_plus_services(string: String?) {
    }

    @Given("Key {string} is disabled")
    fun key_is_disabled(string: String?) {
        realmResource.users().get(apiKeys[string!!]!!.id).let { userResource ->
            userResource.update(userResource.toRepresentation().also { it.isEnabled = false })
        }
    }

    @When("{string} requests to enable key {string}")
    fun reenables_key(string: String?, string2: String?) {
        callKeycloakEndpoint("/key/${apiKeys[string2!!]!!.id}/enable", userToken[string!!], "", "PUT")
    }

    @Then("key {string} is enabled")
    fun key_is_enabled(string: String?) {
        assert(realmResource.users().get(apiKeys[string!!]!!.id).toRepresentation().isEnabled == true)
    }

    @Then("key {string} is disabled")
    fun then_key_is_disabled(string: String?) {
        assert(realmResource.users().get(apiKeys[string!!]!!.id).toRepresentation().isEnabled == false)
    }

    @Given("Key {string} is associated with Member ID {string}")
    fun key_is_associated_with_member_id(string: String?, string2: String?) {
        createAPIKey(string!!, string2!!.toLong())
    }

    @When("{string} requests to disable key {string}")
    fun disables_key(string: String?, string2: String?) {
        callKeycloakEndpoint("/key/${apiKeys[string2!!]!!.id}/disable", userToken[string!!], "", "PUT")
    }

    @Given("Key {string} is associated with Member ID {string} and has description {string}")
    fun key_is_associated_with_member_id_and_has_description(string: String?, string2: String?, string3: String?) {
        // Write code here that turns the phrase above into concrete actions
        createAPIKey(string!!, string2!!.toLong(), string3!!)
    }

    @When("{string} edits description for Key {string} to {string}")
    fun edits_description_for_key_to(string: String?, string2: String?, string3: String?) {
        callKeycloakEndpoint(
            "/key",
            userToken[string!!],
            apiKeys[string2!!]?.let {
                mapper.writeValueAsString(
                    APIKey(string3!!, UUID.randomUUID(), UUID.fromString(it.id), it.username, it.isEnabled)
                )
            },
            "PUT"
        )
        newDescription = string3!!
    }

    @Then("description for Key {string} is updated")
    fun description_for_key_is_updated(string: String?) {
        assert(realmResource.users().get(apiKeys[string!!]!!.id).toRepresentation().attributes["description"]!!.first().equals(newDescription))
    }

    @Then("description for Key {string} is not updated")
    fun description_for_key_is_not_updated(string: String?) {
        assert(!realmResource.users().get(apiKeys[string!!]!!.id).toRepresentation().attributes["description"]!!.first().equals(newDescription))
    }

    @When("{string} requests to delete Key {string}")
    fun deletes_key(string: String?, string2: String?) {
        callKeycloakEndpoint(
            "/key/${apiKeys[string2!!]!!.id}",
            userToken[string!!],
            "",
            "DELETE"
        )
    }

    @Then("Key {string} is deleted")
    fun key_is_deleted(string: String?) {
        assert(realmResource.users().search(apiKeys[string!!]!!.username).size == 0)
    }

    @Then("Key {string} is not deleted")
    fun key_is_not_deleted(string: String?) {
        assert(realmResource.users().search(apiKeys[string!!]!!.username).size == 1)
    }

    @When("{string} creates the key whose ID will be {string} for the organization {string}")
    fun creates_the_key_whose_id_will_be(string: String?, string2: String?, string3: String?) {
        callKeycloakEndpoint(
            "/organization/$string3/key",
            userToken[string!!],
            mapper.writeValueAsString(APIKey(string2!!, UUID.randomUUID())),
            "POST",
            "application/json"
        )
        if (status == 201) {
            apiKeySecrets[string2!!] =
                generateApiKeyHash(mapper.readValue(returnedBody, object : TypeReference<APIKeyResponse>() {}).key)
        }
    }

    @When("{string} requests all API keys for Member ID {string}")
    fun requests_all_api_keys_for_member_id(string: String?, string2: String?) {
        callKeycloakEndpoint(
            "/organization/$string2/key",
            userToken[string!!],
            null,
            "GET"
        )
    }

    @Then("Key {string} is returned")
    fun key_is_returned(string: String?) {
        assert(
            mapper.readValue(returnedBody, object : TypeReference<List<APIKey>>() {}).any {
                it.description == string
            }
        )
    }

    @When("{string} adds {string} to Member ID {string}")
    fun adds_to_member_id(string: String?, string2: String?, string3: String?) {
        callKeycloakEndpoint(
            "/organization/$string3/subscription/$string2",
            userToken[string!!],
            "",
            "PUT"
        )
    }

    @Then("Key ID {string} can access Metadata Plus services")
    fun key_id_can_access_metadata_plus_services(string: String?) {
        assertTrue { checkPermission(apiKeySecrets[string]!!, Subscription.METADATA_PLUS) }
    }

    @When("{string} removes {string} from Member ID {string}")
    fun removes_from_member_id(string: String?, string2: String?, string3: String?) {
        callKeycloakEndpoint(
            "/organization/$string3/subscription/$string2",
            userToken[string!!],
            "",
            "DELETE"
        )
    }

    @Then("Key ID {string} cannot access Metadata Plus services")
    fun key_id_cannot_access_metadata_plus_services(string: String?) {
        assertFalse { checkPermission(apiKeySecrets[string]!!, Subscription.METADATA_PLUS) }
    }

    @When("{string} requests subscriptions for Member ID {string}")
    fun requests_subscriptions_for_member_id(string: String?, string2: String?) {
        callKeycloakEndpoint(
            "/organization/$string2/subscription",
            userToken[string!!],
            null,
            "GET"
        )
    }

    @Then("the response returns {string} subscription")
    fun the_response_returns_subscription(string: String?) {
        assert(mapper.readValue(returnedBody, object : TypeReference<List<Subscription>>() {})!!.contains(Subscription.valueOf(string!!)))
    }

    @Given("{string} does not have an active account in the User Management API")
    fun does_not_have_an_active_account_in_the_user_management_api(string: String?) {
        assert(realmResource.users().search(string).size == 0)
    }

    @Given("{string} has an active account in Authenticator")
    fun has_an_active_account_in_authenticator(string: String?) {
        callAuthenticatorEndpoint("/api/test/userexists/$string", null)
        assertTrue(returnedBody == "false")
        callAuthenticatorEndpoint("/api/test/adduser", mapper.writeValueAsString(mapOf("username" to string, "password" to "password1")), "POST")
        assertTrue(mapper.readValue(returnedBody, object : TypeReference<Map<String, String>>() {})["status"] == "success")
        Thread.sleep(10000)
    }

    @Then("{string} can login to Authenticator with password {string}")
    fun can_login_to_authenticator_with_password(string: String?, string2: String?) {
        callAuthenticatorEndpoint("/api/v1/authenticate", mapper.writeValueAsString(mapOf("username" to string, "password" to string2)), "POST")
        assertTrue(mapper.readValue(returnedBody, object : TypeReference<Map<String, Any>>() {})["status"] as String == "success")
    }

    @Then("{string} can login to crossref UI with password {string}")
    fun can_login_to_crossref_ui_with_password(string: String?, string2: String?) {
        Thread.sleep(10000)
        KeycloakBuilder.builder()
            .serverUrl(System.getenv("KEYCLOAK_ADMIN_URL") + "/auth")
            .realm(System.getenv("CROSSREF_REALM"))
            .clientId(CROSSREF_UI_CLIENT)
            .username(string)
            .password(string2)
            .build().tokenManager().accessToken.token
    }

    @Then("no email is sent to {string}")
    fun no_email_is_sent_to(string: String?) {
    }

    @Given("{string} does not have an active account in Authenticator")
    fun does_not_have_an_active_account_in_authenticator(string: String?) {
        callAuthenticatorEndpoint("/api/test/userexists/$string", null)
        assertTrue(returnedBody == "false")
    }

    @Then("user {string} is created in Authenticator without attaching any legacy role")
    fun user_is_created_in_authenticator_without_attaching_any_legacy_role(string: String?) {
        Thread.sleep(10000)
        callAuthenticatorEndpoint("/api/test/userexists/$string", null)
        assertTrue(returnedBody == "true")
    }

    @Then("user {string} is sent an email to set his password")
    fun user_is_sent_an_email_to_set_his_password(string: String?) {
    }

    @Given("{string} has an active account in the User Management API")
    fun has_an_active_account_in_the_user_management_api(string: String?) {
        createUser(UserDef(string!!, "USER"))
    }

    @Then("{string} cannot login to crossref UI with password {string}")
    fun cannot_login_to_crossref_ui_with_password(string: String?, string2: String?) {
        assertThrows(NotAuthorizedException::class.java) {
            can_login_to_crossref_ui_with_password(string, string2)
        }
    }

    @When("{string} sets his password to {string}")
    fun sets_his_password_to(string: String?, string2: String?) {
        callAuthenticatorEndpoint("/api/v1/add-user", mapper.writeValueAsString(mapOf("username" to string, "password" to string2)), "POST")
        assertTrue(mapper.readValue(returnedBody, object : TypeReference<Map<String, String>>() {})["status"] == "ok")
    }

    @When("user {string} is deleted from Authenticator")
    fun user_is_deleted_from_authenticator(string: String?) {
        callAuthenticatorEndpoint("/api/test/deluser/$string", null)
        Thread.sleep(10000)
    }

    @Then("{string} cannot login to Authenticator with password {string}")
    fun cannot_login_to_authenticator_with_password(string: String?, string2: String?) {
        callAuthenticatorEndpoint("/api/v1/authenticate", mapper.writeValueAsString(mapOf("username" to string, "password" to string2)), "POST")
        assertTrue(mapper.readValue(returnedBody, object : TypeReference<Map<String, Any>>() {})["status"] as String == "failure")
    }

    @When("user {string} is created in Authenticator with password {string}")
    fun user_is_created_in_authenticator_with_password(string: String?, string2: String?) {
        callAuthenticatorEndpoint("/api/test/adduser", mapper.writeValueAsString(mapOf("username" to string, "password" to string2)), "POST")
        Thread.sleep(4000)
    }
}
