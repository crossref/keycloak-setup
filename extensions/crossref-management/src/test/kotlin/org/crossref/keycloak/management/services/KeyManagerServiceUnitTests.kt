package org.crossref.keycloak.management.services

import io.mockk.every
import io.mockk.mockk
import org.crossref.keycloak.management.APIKey
import org.crossref.keycloak.management.CrossrefManagementConfiguration
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.management.fromUserModelToKey
import org.crossref.keycloak.management.utils.KeyDescriptionTooLong
import org.crossref.keycloak.management.utils.KeyNotFoundException
import org.crossref.keycloak.management.utils.OrgNotFoundException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.keycloak.models.RoleModel
import java.util.UUID

class KeyManagerServiceUnitTests {
    private val configuration = CrossrefManagementConfiguration("", "", "")

    @Test
    fun `getApiKeyOrganization works`() {
        val keyId = UUID.randomUUID()

        val keyManager = APIKeyManagerService(
            mockk {
                every { users().getUserById(any(), keyId.toString()).groupsStream } returns listOf(
                    mockGroupModel("org", 123)
                ).stream()
                every { context.realm } returns mockk {}
            },
            configuration
        )

        keyManager.getApiKeyOrganization(keyId)
    }

    @Test
    fun `getApiKeyOrganization fails if keynotfound`() {
        val keyId = UUID.randomUUID()

        val keyManager = APIKeyManagerService(
            mockk {
                every { users().getUserById(any(), keyId.toString()) } returns null
                every { context.realm } returns mockk {}
            },
            configuration
        )

        assertThrows(KeyNotFoundException::class.java) { keyManager.getApiKeyOrganization(keyId) }
    }

    @Test
    fun `Add Key works`() {
        val orgId = UUID.randomUUID()
        val keyId = UUID.randomUUID()
        val apikeyRole = mockk<RoleModel> {}

        val keyManager = APIKeyManagerService(
            mockk {
                every { context.realm.getGroupById(orgId.toString()) } returns mockGroupModel("org", 123, orgId)
                every { context.realm.getRole(Role.APIKEY.roleName) } returns apikeyRole
                every { users().addUser(any(), keyId.toString(), any(), false, false) } returns mockUserKeyModel(
                    "descr",
                    keyId.toString()
                )
            },
            configuration
        )

        keyManager.addKey(APIKey("descr", UUID.randomUUID(), keyId), orgId)
    }

    @Test
    fun `Add Key fails because org not found`() {
        val orgId = UUID.randomUUID()
        val keyId = UUID.randomUUID()

        val keyManager = APIKeyManagerService(
            mockk {
                every { context.realm.getGroupById(orgId.toString()) } returns null
            },
            configuration
        )

        assertThrows(OrgNotFoundException::class.java) { keyManager.addKey(APIKey("descr", UUID.randomUUID(), keyId), orgId) }
    }

    @Test
    fun `Add Key fails because description too long`() {
        val keyManager = APIKeyManagerService(
            mockk { },
            configuration
        )

        assertThrows(KeyDescriptionTooLong::class.java) { keyManager.addKey(APIKey("x".repeat(256), UUID.randomUUID(), UUID.randomUUID()), UUID.randomUUID()) }
    }

    @Test
    fun `List keys works`() {
        val orgId = UUID.randomUUID()
        val keyManager = APIKeyManagerService(
            mockk {
                every { context.realm.getGroupById(orgId.toString()) } returns mockk {}
                every { users().getGroupMembersStream(any(), any()) } returns listOf(
                    mockUserKeyModel("111", UUID.randomUUID().toString()),
                    mockUserModel("q@q.com"),
                    mockUserKeyModel("222", UUID.randomUUID().toString()),
                    mockUserModel("p@p.com")
                ).stream()
            },
            configuration
        )

        assert(keyManager.listKeys(orgId, 1, 10).currentPage().size == 2)
    }

    @Test
    fun `set enabled key works`() {
        val keyId = UUID.randomUUID()

        val keyManager = APIKeyManagerService(
            mockk {
                every { context.realm } returns mockk {}
                every { users().getUserById(any(), keyId.toString()) } returns mockUserKeyModel("123", keyId.toString())
            },
            configuration
        )

        keyManager.setKeyEnabled(keyId, true)
        keyManager.setKeyEnabled(keyId, false)
    }

    @Test
    fun `set enabled key fails if key not found`() {
        val keyId = UUID.randomUUID()

        val keyManager = APIKeyManagerService(
            mockk {
                every { context.realm } returns mockk {}
                every { users().getUserById(any(), keyId.toString()) } returns null
            },
            configuration
        )

        assertThrows(KeyNotFoundException::class.java) { keyManager.setKeyEnabled(keyId, true) }
    }

    @Test
    fun `delete key works`() {
        val keyId = UUID.randomUUID()
        val key = mockUserKeyModel("123", keyId.toString())

        val keyManager = APIKeyManagerService(
            mockk {
                every { context.realm } returns mockk {}
                every { users().getUserById(any(), keyId.toString()) } returns key
                every { users().removeUser(any(), key) } returns true
            },
            configuration
        )

        keyManager.deleteKey(keyId)
    }

    @Test
    fun `delete key fails`() {
        val keyId = UUID.randomUUID()
        mockUserKeyModel("123", keyId.toString())

        val keyManager = APIKeyManagerService(
            mockk {
                every { context.realm } returns mockk {}
                every { users().getUserById(any(), keyId.toString()) } returns null
            },
            configuration
        )

        assertThrows(KeyNotFoundException::class.java) { keyManager.deleteKey(keyId) }
    }

    @Test
    fun `update key works`() {
        val keyId = UUID.randomUUID()
        val key = mockUserKeyModel("123", keyId.toString())
        val key2 = mockUserKeyModel("456", keyId.toString())

        val keyManager = APIKeyManagerService(
            mockk {
                every { context.realm } returns mockk {}
                every { users().getUserById(any(), keyId.toString()) } returns key
            },
            configuration
        )

        keyManager.updateKey(fromUserModelToKey(key2))
    }

    @Test
    fun `update key fails`() {
        val keyId = UUID.randomUUID()
        val key2 = mockUserKeyModel("456", keyId.toString())
        val key3 = mockUserKeyModel("4".repeat(256), keyId.toString())

        val keyManager = APIKeyManagerService(
            mockk {
                every { context.realm } returns mockk {}
                every { users().getUserById(any(), keyId.toString()) } returns null
            },
            configuration
        )

        assertThrows(KeyNotFoundException::class.java) { keyManager.updateKey(fromUserModelToKey(key2)) }
        assertThrows(KeyDescriptionTooLong::class.java) { keyManager.updateKey(fromUserModelToKey(key3)) }
    }
}
