package org.crossref.keycloak.management.services

import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import org.crossref.keycloak.management.Organization
import org.crossref.keycloak.management.Subscription
import org.crossref.keycloak.management.utils.InvalidOrgNameException
import org.crossref.keycloak.management.utils.OrgAlreadyExistsException
import org.crossref.keycloak.management.utils.OrgNotFoundException
import org.crossref.keycloak.management.utils.UserNotFoundException
import org.crossref.keycloak.util.Constants.Crossref.Companion.RESOURCES_CLIENT
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.keycloak.authorization.AuthorizationProvider
import org.keycloak.connections.jpa.JpaConnectionProvider
import org.keycloak.models.ClientModel
import org.keycloak.models.GroupModel
import org.keycloak.models.ModelDuplicateException
import org.keycloak.models.jpa.entities.GroupEntity
import org.keycloak.representations.idm.authorization.DecisionStrategy
import org.keycloak.representations.idm.authorization.Logic
import org.keycloak.util.JsonSerialization.mapper
import java.util.UUID
import java.util.stream.Stream
import kotlin.test.assertEquals

class OrgManagerServiceUnitTests {

    private val rootUuid = UUID.fromString("5F978C08-E8CF-4DE1-B08C-B6A9E2E3D143")

    @Test
    fun `Find org by member id works`() {
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "12345"), any(), any()) } returns listOf(
                    mockGroupModel("name", 12345)
                ).stream()
                every { context.realm } returns mockk {}
            }
        )

        assert(orgManager.findOrgByMemberId(12345).name == "name")
    }

    @Test
    fun `findOrgs using member id works`() {
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "12345"), any(), any()) } returns listOf(
                    mockGroupModel("name", 12345)
                ).stream()
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "1234"), any(), any()) } returns Stream.empty()
                every { context.realm } returns mockk {}
            }
        )

        assert(orgManager.findOrgs("12345", 1).currentPage()[0].name == "name")
        assert(orgManager.findOrgs("1234", 1).currentPage().size == 0)
    }

    @Test
    fun `Find org by member id cannot find org`() {
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), any(), any(), any()) } returns listOf<GroupModel>().stream()
                every { context.realm } returns mockk {}
            }
        )

        assertThrows(OrgNotFoundException::class.java) {
            orgManager.findOrgByMemberId(12345)
        }
    }

    @Test
    fun `Find orgs works`() {
        val orgManager = OrgManagerService(
            mockk {
                every {
                    getProvider(JpaConnectionProvider::class.java)
                        .entityManager
                        .createQuery(any(), GroupEntity::class.java)
                } returns mockk {
                    every { setFirstResult(any()) } returns mockk {}
                    every { setMaxResults(any()) } returns mockk {}
                    every { resultStream } returns listOf(
                        mockGroupEntity("org1", 1),
                        mockGroupEntity("org2", 2),
                        mockGroupEntity("org3", 3)
                    ).stream()
                }
                every { groups().searchGroupsByAttributes(any(), any(), any(), any()) } returns listOf<GroupModel>().stream()
                every { context.realm.topLevelGroupsStream } returns listOf(
                    mockGroupModel("organizations", 0, rootUuid)
                ).stream()
            }
        )

        val orgs = orgManager.findOrgs("", 1)
        assert(orgs.currentPage().size == 3)
    }

    @Test
    fun `Find orgs by name works`() {
        val orgManager = OrgManagerService(
            mockk {
                every { getProvider(JpaConnectionProvider::class.java).entityManager.createQuery(any(), GroupEntity::class.java) } returns mockk {
                    every { setFirstResult(any()) } returns mockk {}
                    every { setMaxResults(any()) } returns mockk {}
                    every { setParameter(any<String>(), any<String>()) } returns mockk {}
                    every { resultStream } returns listOf(
                        mockGroupEntity("org1", 1),
                        mockGroupEntity("org2", 2),
                        mockGroupEntity("org3", 3)
                    ).stream()
                }
                every { groups().searchGroupsByAttributes(any(), any(), any(), any()) } returns listOf<GroupModel>().stream()
                every { context.realm.topLevelGroupsStream } returns listOf(
                    mockGroupModel("organizations", 0, rootUuid)
                ).stream()
            }
        )

        val orgs = orgManager.findOrgs("rg", 1)
        assert(orgs.currentPage().size == 3)
    }

    @Test
    fun `Create an org with invalid name fails`() {
        val orgManager = OrgManagerService(mockk {})

        assertThrows(InvalidOrgNameException::class.java) {
            orgManager.addOrg(Organization(UUID.randomUUID(), "*wrong*", 200))
        }
    }

    @Test
    fun `Create an org with existent memberId fails`() {
        val orgManager = OrgManagerService(
            mockk {
                mockk {
                    every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "200"), any(), any()) } returns listOf(
                        mockGroupModel("name", 200)
                    ).stream()
                    every { context.realm } returns mockk {}
                }
            }
        )

        assertThrows(OrgAlreadyExistsException::class.java) {
            orgManager.addOrg(Organization(UUID.randomUUID(), "name", 200))
        }
    }

    @Test
    fun `Create an org with existent name fails`() {
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "200"), any(), any()) } returns listOf<GroupModel>().stream()
                every { context.realm } returns mockk {
                    every { createGroup("name", any<GroupModel>()) } throws mockk<ModelDuplicateException> {}
                    every { getGroupById(any()) } returns mockk { }
                }
            }
        )

        assertThrows(OrgAlreadyExistsException::class.java) {
            orgManager.addOrg(Organization(UUID.randomUUID(), "name", 200))
        }
    }

    @Test
    fun `Create an org works`() {
        val orgsRoot = mockGroupModel("organizations", 1)
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "200"), any(), any()) } returns listOf<GroupModel>().stream()
                every { context.realm } returns mockk {
                    every { createGroup("name", orgsRoot) } returns mockGroupModel("name", 200)
                    every { getGroupById(any()) } returns orgsRoot
                }
            }
        )

        assert(orgManager.addOrg(Organization(UUID.randomUUID(), "name", 200)).name == "name")
    }

    @Test
    fun `Update an org with invalid name fails`() {
        val orgManager = OrgManagerService(mockk {})

        assertThrows(InvalidOrgNameException::class.java) {
            orgManager.addOrg(Organization(UUID.randomUUID(), "*wrong*", 200))
        }
    }

    @Test
    fun `Update an org to an existent name fails`() {
        val uuid = UUID.randomUUID()

        val orgManager = OrgManagerService(
            mockk {
                every {
                    getProvider(JpaConnectionProvider::class.java)
                        .entityManager
                        .createQuery(any(), GroupEntity::class.java)
                        .setParameter(any<String>(), any<String>())
                        .resultStream
                } returns listOf(
                    mockGroupEntity("name", 200)
                ).stream()
            }
        )

        assertThrows(OrgAlreadyExistsException::class.java) {
            orgManager.updateOrg(Organization(uuid, "name", 200))
        }
    }

    @Test
    fun `Update an org to an non-existent name works`() {
        val uuid = UUID.randomUUID()

        val orgManager = OrgManagerService(
            mockk {
                every { context.realm.getGroupById(any()) } returns mockGroupModel("name", 200, uuid)
                every {
                    getProvider(JpaConnectionProvider::class.java)
                        .entityManager
                        .createQuery(any(), GroupEntity::class.java)
                        .setParameter(any<String>(), any<String>())
                        .resultStream
                } returns listOf<GroupEntity>().stream()
            }
        )

        val origOrg = Organization(uuid, "name", 200)
        val updated = orgManager.updateOrg(Organization(uuid, "name2", 200))
        assert(origOrg.orgId == updated.orgId)
        assert(updated.name == "name2")
        assert(origOrg.memberId == updated.memberId)
    }

    @Test
    fun `Update an org to an non-existent uuid created a new org`() {
        val uuid = UUID.randomUUID()
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), any(), any(), any()) } returns listOf<GroupModel>().stream()
                every { context.realm } returns mockk {
                    every { getGroupById(uuid.toString()) } returns null
                    every { getGroupById(rootUuid.toString()) } returns mockk {}
                    every { createGroup("name", any<GroupModel>()) } returns mockGroupModel("name", 200)
                    every { topLevelGroupsStream } returns listOf(
                        mockGroupModel("organizations", 0, rootUuid)
                    ).stream()
                }
                every {
                    getProvider(JpaConnectionProvider::class.java)
                        .entityManager
                        .createQuery(any(), GroupEntity::class.java)
                        .setParameter(any<String>(), any<String>())
                        .resultStream
                } returns listOf<GroupEntity>().stream()
            }
        )

        val origOrg = Organization(uuid, "name", 200)
        val updated = orgManager.updateOrg(origOrg)
        assert(updated.orgId != origOrg.orgId)
        assert(updated.memberId == origOrg.memberId)
        assert(updated.name == origOrg.name)
    }

    @Test
    fun `Add user to Organization works`() {
        val uid = UUID.randomUUID()
        val gid = UUID.randomUUID()
        val group = mockGroupModel("org", 123, gid)

        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "123"), 0, 1) } returns listOf(
                    group
                ).stream()
                every { context.realm.getGroupById(gid.toString()) } returns group
                every { users().getUserById(any(), uid.toString()).joinGroup(group) } returns mockk {}
            }
        )
        orgManager.addUserToOrg(123, uid)
    }

    @Test
    fun `Add user to Organization works throws User not found`() {
        val uid = UUID.randomUUID()
        val gid = UUID.randomUUID()
        val group = mockGroupModel("org", 123, gid)

        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "123"), 0, 1) } returns listOf(
                    group
                ).stream()
                every { context.realm } returns mockk { }
                every { users().getUserById(any(), uid.toString()) } returns null
            }
        )
        assertThrows(UserNotFoundException::class.java) {
            orgManager.addUserToOrg(123, uid)
        }
    }

    @Test
    fun `Add user to Organization works throws org not found exception`() {
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "123"), 0, 1) } returns listOf<GroupModel>().stream()
                every { context.realm } returns mockk { }
            }
        )
        assertThrows(OrgNotFoundException::class.java) {
            orgManager.addUserToOrg(123, UUID.randomUUID())
        }
    }

    @Test
    fun `Add subscription to organization works`() {
        val clientModel = mockk<ClientModel> {}
        val groupId = UUID.randomUUID()
        val policyId = UUID.randomUUID().toString()

        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "123"), 0, 1) } returns listOf(
                    mockGroupModel("org1", 123, groupId)
                ).stream()
                every { context.realm.getClientByClientId(RESOURCES_CLIENT) } returns clientModel
                every { getProvider(AuthorizationProvider::class.java).getProviderFactory(any()).onUpdate(any(), any(), any()) } returns mockk {}
                every { getProvider(AuthorizationProvider::class.java).storeFactory } returns mockk {
                    every { resourceServerStore.findByClient(clientModel) } returns mockk {}
                    every { policyStore.findByName(any(), "metadata-plus-data-access-policy").id } returns policyId
                    every { policyStore.findById(any(), any(), policyId) } returns mockk {
                        every { id } returns policyId
                        every { name } returns ""
                        every { name = any() } returns mockk {}
                        every { description = any() } returns mockk {}
                        every { decisionStrategy = any() } returns mockk {}
                        every { logic = any() } returns mockk {}
                        every { removeConfig(any()) } returns mockk {}
                        every { type } returns ""
                        every { decisionStrategy } returns DecisionStrategy.UNANIMOUS
                        every { logic } returns Logic.POSITIVE
                        every { config.getOrDefault("groups", "[]") } returns "[]"
                        every { resourceServer.realm } returns mockk {}
                        justRun { config.put("groups", mapper.writeValueAsString(listOf(mapOf("id" to groupId.toString(), "extendChildren" to false)))) }
                    }
                }
            }
        )

        orgManager.addOrgSubscription(123, Subscription.METADATA_PLUS)
    }

    @Test
    fun `Add subscription fails if organization is not found`() {
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "123"), 0, 1) } returns listOf<GroupModel>().stream()
                every { context.realm } returns mockk {}
            }
        )

        assertThrows(OrgNotFoundException::class.java) {
            orgManager.addOrgSubscription(123, Subscription.METADATA_PLUS)
        }
    }

    @Test
    fun `Remove subscription from organization works`() {
        val clientModel = mockk<ClientModel> {}
        val groupId = UUID.randomUUID()
        val policyId = UUID.randomUUID().toString()

        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "123"), 0, 1) } returns listOf(
                    mockGroupModel("org1", 123, groupId)
                ).stream()
                every { context.realm.getClientByClientId(RESOURCES_CLIENT) } returns clientModel
                every { getProvider(AuthorizationProvider::class.java).getProviderFactory(any()).onUpdate(any(), any(), any()) } returns mockk {}
                every { getProvider(AuthorizationProvider::class.java).storeFactory } returns mockk {
                    every { resourceServerStore.findByClient(clientModel) } returns mockk {}
                    every { policyStore.findByName(any(), "metadata-plus-data-access-policy").id } returns policyId
                    every { policyStore.findById(any(), any(), policyId) } returns mockk {
                        every { id } returns policyId
                        every { name } returns ""
                        every { name = any() } returns mockk {}
                        every { description = any() } returns mockk {}
                        every { decisionStrategy = any() } returns mockk {}
                        every { logic = any() } returns mockk {}
                        every { removeConfig(any()) } returns mockk {}
                        every { type } returns ""
                        every { decisionStrategy } returns DecisionStrategy.UNANIMOUS
                        every { logic } returns Logic.POSITIVE
                        every { resourceServer.realm } returns mockk {}
                        every { config.getOrDefault("groups", "[]") } returns mapper.writeValueAsString(
                            listOf(
                                OrgManagerService.PolicyGroup(groupId.toString(), false)
                            )
                        )
                        justRun { config.put("groups", "[]") }
                    }
                }
            }
        )

        orgManager.delOrgSubscription(123, Subscription.METADATA_PLUS)
    }

    @Test
    fun `Delete subscription fails if organization is not found`() {
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "123"), 0, 1) } returns listOf<GroupModel>().stream()
                every { context.realm } returns mockk {}
            }
        )

        assertThrows(OrgNotFoundException::class.java) {
            orgManager.delOrgSubscription(123, Subscription.METADATA_PLUS)
        }
    }

    @Test
    fun `Get Org subscriptions works`() {
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "123"), 0, 1) } returns listOf(
                    mockGroupModel("org1", 123, subscriptions = listOf(Subscription.METADATA_PLUS))
                ).stream()
                every { context.realm } returns mockk {}
            }
        )

        assertEquals(orgManager.getOrgSubscriptions(123), listOf(Subscription.METADATA_PLUS))
    }

    @Test
    fun `Get Org subscriptions fails if org not found`() {
        val orgManager = OrgManagerService(
            mockk {
                every { groups().searchGroupsByAttributes(any(), mapOf("memberId" to "123"), 0, 1) } returns listOf<GroupModel>().stream()
                every { context.realm } returns mockk {}
            }
        )

        assertThrows(OrgNotFoundException::class.java) {
            orgManager.getOrgSubscriptions(123)
        }
    }
}
