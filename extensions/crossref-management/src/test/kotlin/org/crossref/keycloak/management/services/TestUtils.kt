package org.crossref.keycloak.management.services

import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import org.crossref.keycloak.management.Subscription
import org.keycloak.events.EventBuilder
import org.keycloak.models.GroupModel
import org.keycloak.models.RoleModel
import org.keycloak.models.UserModel
import org.keycloak.models.jpa.entities.GroupEntity
import org.keycloak.util.JsonSerialization.mapper
import java.util.UUID

fun mockGroupEntity(orgName: String, memberId: Long, gid: UUID? = null): GroupEntity {
    return mockk {
        every { id } returns (gid ?: UUID.randomUUID()).toString()
        every { name } returns orgName
        every { attributes } returns listOf(
            mockk {
                every { name } returns "memberId"
                every { value } returns memberId.toString()
            }
        )
    }
}

fun mockGroupModel(orgName: String, memberId: Long, gid: UUID? = null, subscriptions: List<Subscription> = listOf()): GroupModel {
    var origName = orgName
    return mockk {
        every { id } returns (gid ?: UUID.randomUUID()).toString()
        every { name } answers { origName }
        every { attributes } returns mapOf(
            "memberId" to listOf(memberId.toString()),
            "subscriptions" to listOf(mapper.writeValueAsString(subscriptions))
        )
        every { name = any() } answers {
            origName = it.invocation.args[0] as String
        }
        every { setAttribute(any(), any()) } returns mockk {}
    }
}

fun mockEventBuilder(): EventBuilder {
    return mockk {
        every { detail(any() as String, any() as String) } returns this
        every { event(any()) } returns this
        every { user(any() as String) } returns this
        justRun { success() }
    }
}

fun mockUserModel(userEmail: String, uid: UUID? = null, groups: List<GroupModel>? = null, roles: List<String>? = null): UserModel {
    return mockk {
        every { username } returns userEmail
        every { email } returns userEmail
        every { id } returns (uid ?: UUID.randomUUID()).toString()
        every { getGroupsStream(any(), any(), any()) } returns (groups ?: listOf()).stream()
        every { groupsStream } returns (groups ?: listOf()).stream()
        every { isEnabled = any() } returns mockk {}
        every { setAttribute(any(), any()) } returns mockk {}
        every { isEmailVerified = any() } returns mockk {}
        every { grantRole(any()) } returns mockk {}
        every { joinGroup(any()) } returns mockk {}
        every { attributes } returns mockk {}
        every { roleMappingsStream } returns (roles ?: listOf()).map { mockk<RoleModel> { every { name } returns it } }.stream()
        every { hasRole(any()) } answers { (roles ?: listOf()).contains((it.invocation.args[0] as RoleModel).name) }
    }
}

fun mockUserKeyModel(description: String, uid: String? = null): UserModel {
    return mockk {
        every { email } returns "apikey-${UUID.randomUUID()}@crossref.org"
        every { username } returns "apikey-${UUID.randomUUID()}@crossref.org"
        every { email = any() } returns mockk {}
        every { id } returns (uid ?: UUID.randomUUID()).toString()
        every { isEnabled = any() } returns mockk {}
        every { isEnabled } returns true
        every { setAttribute(any(), any()) } returns mockk {}
        every { isEmailVerified = any() } returns mockk {}
        every { grantRole(any()) } returns mockk {}
        every { joinGroup(any()) } returns mockk {}
        every { attributes.get("description") } returns listOf(description)
        every { attributes.get("issuedBy") } returns listOf(UUID.randomUUID().toString())
    }
}
