package org.crossref.keycloak.management.services

import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.spyk
import jakarta.ws.rs.ForbiddenException
import org.crossref.keycloak.management.CrossrefManagementConfiguration
import org.crossref.keycloak.management.Role
import org.crossref.keycloak.management.SyncOperation
import org.crossref.keycloak.management.User
import org.crossref.keycloak.management.UserSyncMessage
import org.crossref.keycloak.management.utils.InvalidUsernameException
import org.crossref.keycloak.management.utils.UserAlreadyExistsException
import org.crossref.keycloak.management.utils.UserNotFoundException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.keycloak.models.ModelDuplicateException
import org.keycloak.models.RoleModel
import org.keycloak.models.UserModel
import java.util.UUID

class UserManagerServiceUnitTests {
    private val configuration = CrossrefManagementConfiguration("", "http://localhost:8080/", "user:pwd")

    @Test
    fun `Find users works`() {
        val userManager = UserManagerService(
            mockk {
                every { users().searchForUserStream(any(), mapOf(UserModel.SEARCH to "*@cross*", UserModel.USERNAME to "@"), 0, 10) } returns listOf(
                    mockUserModel("john@crossref.org", roles = listOf(Role.USER.roleName)),
                    mockUserModel("mary@crossref.org", roles = listOf(Role.USER.roleName)),
                    mockUserModel("staff@crossref.org", roles = listOf(Role.STAFF.roleName)),
                ).stream()
                every { context.realm } returns mockk {}
                every { context.realm.getRole(Role.USER.roleName).name } returns Role.USER.roleName
            },
            configuration
        )

        assert(userManager.getUsers("@cross", 1, 10).currentPage().size == 2)
    }

    @Test
    fun `add user works`() {
        val role = mockk<RoleModel> {}

        val userManager = spyk(
            UserManagerService(
                mockk {
                    every { users().addUser(any(), "user@crossref.org") } returns mockk {
                        every { id } returns UUID.randomUUID().toString()
                        every { username } returns "user@crossref.org"
                        every { email = "user@crossref.org" } returns mockk {}
                        every { isEnabled = true } returns mockk {}
                        every { grantRole(role) } returns mockk {}
                    }
                    every { context.realm.getRole(Role.USER.roleName) } returns role
                },
                configuration
            )
        ) {
            justRun { sendActions(any(), any()) }
        }

        userManager.addUser(User(UUID.randomUUID(), "user@crossref.org"), Role.USER)
    }

    @Test
    fun `add user throws InvalidUsernameException`() {
        val userManager = UserManagerService(mockk {}, configuration)

        assertThrows(InvalidUsernameException::class.java) {
            userManager.addUser(User(UUID.randomUUID(), "user_crossref.lol"), Role.USER)
        }
    }

    @Test
    fun `add user Only allows creation of User and Staff`() {
        val userManager = UserManagerService(mockk {}, configuration)

        Role.values().filter { !setOf(Role.USER, Role.STAFF).contains(it) }.forEach {
            assertThrows(ForbiddenException::class.java) {
                userManager.addUser(User(UUID.randomUUID(), "user@crossref.org"), it)
            }
        }
    }

    @Test
    fun `cannot add duplicated user`() {
        val role = mockk<RoleModel> {}

        val userManager = spyk(
            UserManagerService(
                mockk {
                    every { users().addUser(any(), "user@crossref.org") } throws mockk<ModelDuplicateException>()
                    every { context.realm.getRole(Role.USER.roleName) } returns role
                },
                configuration
            )
        ) {
            every { sendActions(any(), any()) } returns mockk {}
        }

        assertThrows(UserAlreadyExistsException::class.java) {
            userManager.addUser(User(UUID.randomUUID(), "user@crossref.org"), Role.USER)
        }
    }

    @Test
    fun `delete user works`() {
        val uid = UUID.randomUUID()
        val userModel = mockUserModel("user@crosssref.org", uid)

        val userManager = UserManagerService(
            mockk {
                every { users() } returns mockk {
                    every { getUserById(any(), uid.toString()) } returns userModel
                    every { removeUser(any(), userModel) } returns true
                }
                every { context.realm } returns mockk { }
            },
            configuration
        )

        userManager.delUser(uid)
    }

    @Test
    fun `delete non-user fails`() {
        val uid = UUID.randomUUID()

        val userManager = UserManagerService(
            mockk {
                every { users().getUserById(any(), uid.toString()) } returns null
                every { context.realm } returns mockk { }
            },
            configuration
        )

        assertThrows(UserNotFoundException::class.java) {
            userManager.delUser(uid)
        }
    }

    @Test
    fun `get user orgs works`() {
        val uid = UUID.randomUUID()
        val userModel = mockUserModel(
            "user@crossref.org",
            uid,
            listOf(
                mockGroupModel("org1", 1),
                mockGroupModel("org2", 2)
            )
        )

        val userManager = UserManagerService(
            mockk {
                every { users().getUserById(any(), uid.toString()) } returns userModel
                every { context.realm } returns mockk { }
            },
            configuration
        )

        assert(userManager.getOrgs(uid, null, null, null).currentPage().size == 2)
    }

    @Test
    fun `get non existent user orgs fails`() {
        val uid = UUID.randomUUID()

        val userManager = UserManagerService(
            mockk {
                every { users().getUserById(any(), uid.toString()) } returns null
                every { context.realm } returns mockk { }
            },
            configuration
        )

        assertThrows(UserNotFoundException::class.java) {
            assert(userManager.getOrgs(uid, null, null, null).currentPage().size == 2)
        }
    }

    @Test
    fun `sync user hash works`() {
        val userModel = mockUserModel("username@site.org", roles = listOf(Role.USER.roleName))
        val request = UserSyncMessage("username@site.org", SyncOperation.SYNC, "", "", "", 1)

        val userManager = UserManagerService(
            mockk {
                every { users().getUserByEmail(any(), "username@site.org") } returns userModel
                every { context.realm.getRole(Role.USER.roleName).name } returns Role.USER.roleName
            },
            configuration
        )

        val mock = spyk(userManager, recordPrivateCalls = true)
        justRun { mock invoke "syncUserHash" withArguments listOf(userModel, request) }

        mock.syncUser(request, mockEventBuilder())
    }

    @Test
    fun `sync user delete works`() {
        val userModel = mockUserModel("username@site.org", roles = listOf(Role.USER.roleName))
        val request = UserSyncMessage("username@site.org", SyncOperation.DELETE, "", "", "", 1)

        val userManager = UserManagerService(
            mockk {
                every { users().getUserByEmail(any(), "username@site.org") } returns userModel
                every { users().removeUser(any(), userModel) } returns true
                every { context.realm.getRole(Role.USER.roleName).name } returns Role.USER.roleName
            },
            configuration
        )

        val mock = spyk(userManager, recordPrivateCalls = true)
        every { mock invoke "syncUserHash" withArguments listOf(userModel, request) } returns mockk {}

        mock.syncUser(request, mockEventBuilder())
    }

    @Test
    fun `sync user doesnt work if role is not exclusively ROLE_USER`() {
        val userModel = mockUserModel("username@site.org", roles = listOf(Role.USER.roleName, Role.STAFF.roleName))
        val request = UserSyncMessage("username@site.org", SyncOperation.DELETE, "", "", "", 1)

        val userManager = UserManagerService(
            mockk {
                every { users().getUserByEmail(any(), "username@site.org") } returns userModel
                every { context.realm.getRole(Role.USER.roleName).name } returns Role.USER.roleName
            },
            configuration
        )

        userManager.syncUser(request, mockEventBuilder())
    }
}
