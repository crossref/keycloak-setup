Feature: Subscription check with API Keys
  ORGANIZATIONS can have a SUBSCRIPTION (for example PLUS). These apply to various Crossref services (for example rest-api).
  Organizations can be issued any number of API-KEYS. A user belonging to an organization can try to consume a service by using an
  API-Key. If a user tries to access a service using an API-key, the service needs to be able to know whether that key
  has permission to consume the service based on its organization subscription status.

  This feature enables a Crossref service to check if an API-key has access granted to a given subscription.

  Background:
    Given Psychoceramics is an organization with PLUS subscription
    Given Carberry is an organization without PLUS subscription

  Scenario: Subscriber organization with PLUS subscription
    Given an API-Key which was issued to Psychoceramics
    When the rest-api requests permission to PLUS for the API-key
    Then the answer is "yes"

  Scenario: Member organization without PLUS subscription
    Given an API-Key which was issued to Carberry
    When the rest-api requests permission to PLUS for the API-key
    Then the answer is "no"

  Scenario: Subscriber organization whose PLUS subscription has been cancelled
    We cancel account when PLUS subscribers don't pay
    Given Psychoceramics PLUS subscription was revoked
    And an API-Key which was issued to Psychoceramics
    When the rest-api requests permission to PLUS for the API-key
    Then the answer is "no"

  Scenario: API-Key is disabled
    We can disable API-Keys if their use doesn't meet the accepted use policy
    Given an API-Key which was issued to Psychoceramics
    And the API-Key was disabled
    When the rest-api requests permission to PLUS for the API-key
    Then the answer is "no"

