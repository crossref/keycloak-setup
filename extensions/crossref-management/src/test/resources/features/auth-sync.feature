Feature: Authenticator - Keycloak sync

Authenticator is the previous authentication system that keycloak will replace. However for some time they
are going to work at the same time and need some synchronization making sure that users keep the same credentials
and all the systems relying in the authentication systems keep working with the same credentials too.
Users should be synced from Keycloak to Authenticator so that users can login to both new and legacy services with
one password and so that users can set/reset passwords.

Background:
  Given the following user types
    | Type ID | Description                                                                       |
    | STAFF   | A member of the Crossref support team who has full access to the API              |
  And the following users
    | User Type | Email Address          |
    | STAFF     | josh@crossref.org      |

Scenario: User exists in Authenticator
  Given "ameera@psychoceramics.org" does not have an active account in the User Management API
  And "ameera@psychoceramics.org" has an active account in Authenticator
  When user "josh@crossref.org" requests the creation of a new user with Email Address "ameera@psychoceramics.org" and User Type "USER"
  Then "ameera@psychoceramics.org" can login to Authenticator with password "password1"
  And "ameera@psychoceramics.org" can login to crossref UI with password "password1"
  And no email is sent to "ameera@psychoceramics.org"


Scenario: User does not exist in Authenticator
  Given "bill@psychoceramics.org" does not have an active account in the User Management API
  And "bill@psychoceramics.org" does not have an active account in Authenticator
  When user "josh@crossref.org" requests the creation of a new user with Email Address "bill@psychoceramics.org" and User Type "USER"
  Then user "bill@psychoceramics.org" is created in Authenticator without attaching any legacy role
  Then user "bill@psychoceramics.org" is sent an email to set his password


Scenario: User is deleted from Keycloak
  Given "jordan@psychoceramics.org" has an active account in the User Management API
  And "jordan@psychoceramics.org" has an active account in Authenticator
  And "jordan@psychoceramics.org" can login to crossref UI with password "password1"
  And "jordan@psychoceramics.org" can login to Authenticator with password "password1"
  When user "josh@crossref.org" requests the deletion of an existing user with Email Address "jordan@psychoceramics.org"
  Then "jordan@psychoceramics.org" cannot login to crossref UI with password "password1"
  And "jordan@psychoceramics.org" can login to Authenticator with password "password1"

Scenario: User sets password
New users sent an email to set password for first time and existing users sent email to reset password
  When "bill@psychoceramis.org" sets his password to "password2"
  Then "bill@psychoceramis.org" can login to Authenticator with password "password2"
  Then "bill@psychoceramis.org" can login to Authenticator with password "password2"

Scenario: Authenticator user is deleted
  Given "peter@psychoceramics.org" has an active account in the User Management API
  And "peter@psychoceramics.org" has an active account in Authenticator
  And "peter@psychoceramics.org" can login to crossref UI with password "password1"
  And "peter@psychoceramics.org" can login to Authenticator with password "password1"
  When user "peter@psychoceramics.org" is deleted from Authenticator
  Then "peter@psychoceramics.org" cannot login to crossref UI with password "password1"
  And "peter@psychoceramics.org" cannot login to Authenticator with password "password1"

Scenario: User is created in authenticator and does not exist in Key Manager
  Given "ameera@psychoceramics.org" does not have an active account in the User Management API
  And "ameera@psychoceramics.org" does not have an active account in Authenticator
  When user "ameera@psychoceramics.org" is created in Authenticator with password "password1"
  Then "ameera@psychoceramics.org" can login to crossref UI with password "password1"