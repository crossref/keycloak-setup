Feature: API Key Management API

An organization API Key provides access to the features of a subscription.
Key is assigned to the org, not the users within it.
If a key is misused, our observability infrastructure will tell us using its ID.
A user can assign an API Key Name and API Key Description and an API Key ID is automatically assigned.


Background:
  Given the following user types
    | Type ID | Description                                                                       |
    | STAFF   | A member of the Crossref support team who has full access to the API              |
    | USER    | A logged in user who is not a Crossref employee and has limited access to the API |
    | GUEST   | A person who is not logged in and has minimal access to the API                   |
  And the following users
    | User Type | Email Address          |
    | STAFF     | josh@crossref.org      |
    | USER      | sarah@not.crossref.org |
    | USER      | mike@not.crossref.org  |
  And the following organizations
    | Member ID | Name                |
    | 12345     | Psychoceramics      |
    | 67890     | Dept. of Psychology |
    | 19283     | Wesleyan University |
  And the following user organization assignments
    | Email Address          | Organization |
    | sarah@not.crossref.org | 12345        |
    | mike@not.crossref.org  | 12345        |
    | mike@not.crossref.org  | 67890        |
  And the following response types
    | Type ID         | Description                                                              |
    | KEY_CREATED	  | A new key was created													 |
    | KEY_DISABLED    | An existing key was disabled											 |
    | KEY_ENABLED	  | A disabled key was re-enabled											 |
    | KEY_DELETED 	  | An existing key was deleted												 |
    | KEY_UPDATED	  | A key was updated														 |
    | FORBIDDEN       | An attempted operation was refused because the user is not authorized    |


Scenario: User can create an API key for an organization
  When "sarah@not.crossref.org" creates the key whose ID will be "111" for the organization "12345"
  Then response type "KEY_CREATED" is returned

Scenario: User cannot create an API key for an organization they don't belong to
  When "sarah@not.crossref.org" creates the key whose ID will be "111" for the organization "67890"
  Then response type "FORBIDDEN" is returned

Scenario: Staff can disable an API key
  Given Key "111" is associated with Member ID "12345"
  When "josh@crossref.org" requests to disable key "111"
  Then response type "KEY_DISABLED" is returned

Scenario: Staff can re-enable an API key
  Given Key "111" is associated with Member ID "12345"
  And Key "111" is disabled
  When "josh@crossref.org" requests to enable key "111"
  Then response type "KEY_ENABLED" is returned

Scenario: User cannot enable an API key
  Given Key "111" is associated with Member ID "12345"
  And Key "111" is disabled
  When "sarah@not.crossref.org" requests to enable key "111"
  Then key "111" is disabled
  And response type "FORBIDDEN" is returned

Scenario: User cannot disable an API key
  Given Key "111" is associated with Member ID "12345"
  When "sarah@not.crossref.org" requests to disable key "111"
  Then key "111" is enabled
  And response type "FORBIDDEN" is returned

Scenario: Staff cannot delete an API key
  Given Key "222" is associated with Member ID "12345"
  When "josh@crossref.org" requests to delete Key "222"
  Then Key "222" is not deleted
  And response type "FORBIDDEN" is returned

Scenario: User can delete an API key
  Given Key "222" is associated with Member ID "12345"
  When "sarah@not.crossref.org" requests to delete Key "222"
  Then Key "222" is deleted
  And response type "KEY_DELETED" is returned

Scenario: User cannot delete an API key for an organization it doesn't belong to
  Given Key "222" is associated with Member ID "67890"
  When "sarah@not.crossref.org" requests to delete Key "222"
  Then Key "222" is not deleted
  And response type "FORBIDDEN" is returned

Scenario: User can update description
  Given Key "111" is associated with Member ID "12345" and has description "Key for Product X"
  When "sarah@not.crossref.org" edits description for Key "111" to "Key for Product Y"
  Then description for Key "111" is updated
  And response type "KEY_UPDATED" is returned

Scenario: User cannot update description for an organization he does not belong to
  Given Key "111" is associated with Member ID "67890" and has description "Key for Product X"
  When "sarah@not.crossref.org" edits description for Key "111" to "Key for Product Y"
  Then description for Key "111" is not updated
  And response type "FORBIDDEN" is returned

Scenario: User can view API Keys for an organization to which they belong
  Given Key "111" is associated with Member ID "12345"
  And Key "222" is associated with Member ID "12345"
  When "sarah@not.crossref.org" requests all API keys for Member ID "12345"
  Then Key "111" is returned
  And Key "222" is returned

Scenario: User cannot view API Keys for an organization to which they do not belong
  When "sarah@not.crossref.org" requests all API keys for Member ID "67890"
  Then response type "FORBIDDEN" is returned

Scenario: Staff can view API Keys for any organization
  Given Key "111" is associated with Member ID "12345"
  And Key "222" is associated with Member ID "12345"
  When "josh@crossref.org" requests all API keys for Member ID "12345"
  Then Key "111" is returned
  And Key "222" is returned