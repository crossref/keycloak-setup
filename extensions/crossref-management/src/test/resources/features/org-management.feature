Feature: Organization Management API

An organization is an entity known to Crossref. It can be a Member, Sponsor, Service Provider or Subscriber, or any combination of these. These are currently administered in SugarCRM.

The Organization Management API is a basis for how we keep track of Organizations, their permissions and subscriptions with Crossref. This API is intended to be used as part of Crossref's administrative processes by Crossref staff, or users acting on behalf of Crossref.

In the first iteration, the Organization Management API is not automatically connected to SugarCRM. Organizations are added to the Organization Management API manually using the Organization Management feature. Once created, an organization's Member ID is read-only.

We keep track of other metadata for organizations in other APIs (e.g. sponsorship etc). In the first iteration, these will not be connected.

Background:
  Given the following organizations
    | Member ID | Name                |
    | 12345     | Psychoceramics      |
    | 67890     | Dept. of Psychology |
    | 19283     | Wesleyan University |
  And the following user types
    | Type ID | Description                                                                       |
    | STAFF   | A member of the Crossref support team who has full access to the API              |
    | USER    | A logged in user who is not a Crossref employee and has limited access to the API |
    | GUEST   | A person who is not logged in and has minimal access to the API                   |
  And the following users
    | Email Address          | User Type |
    | josh@crossref.org      | STAFF     |
    | sarah@not.crossref.org | USER      |
    | Joe                    | GUEST     |
  And the following response types
    | Type ID        | Description                                                                               |
    | ORG_NOT_FOUND  | An existing organization was not found                                                    |
    | ORG_CREATED    | A new organization was created                                                            |
    | DUPLICATED_ORG | A new organization was not created because it is a duplicate of an existing organization  |
    | ORG_UPDATED    | An existing organization's name was updated                                               |
    | FORBIDDEN      | An attempted operation was refused because the user is not authorized                     |

Scenario: Any user can retrieve an organization by its Member ID
  When user "Joe" requests retrieval of the organization with Member ID "12345"
  Then organization "12345" is returned
  But no other organizations are returned

Scenario: Any user cannot retrieve an organization that does not exist
  When user "Joe" requests retrieval of the organization with Member ID "999"
  Then response type "ORG_NOT_FOUND" is returned

Scenario: Any user can retrieve all organizations
  When user "Joe" requests retrieval of all organizations
  Then organization "12345" is returned
  And organization "67890" is returned
  And organization "19283" is returned
  But no other organizations are returned

Scenario: Staff user can create a new organization
  When user "josh@crossref.org" requests the creation of a new organization with Member ID "998877" and Name "University Press"
  Then the organization is created
  And response type "ORG_CREATED" is returned

Scenario: Staff user cannot create a new organization that is a duplicate of an existing organization
  When user "josh@crossref.org" requests the creation of a new organization with Member ID "12345" and Name "Pickle Press"
  Then the organization is not created
  And organization "12345" is not updated
  And response type "DUPLICATED_ORG" is returned

Scenario: Staff user can change an organization's name
  When user "josh@crossref.org" requests the organization with Member ID "67890" is updated to have Name "Department of Psychology"
  Then organization "67890" is updated
  And response type "ORG_UPDATED" is returned

Scenario: Normal user cannot create a new organization
  When user "sarah@not.crossref.org" requests the creation of a new organization with Member ID "123" and Name "College Press"
  Then the organization is not created
  And response type "FORBIDDEN" is returned

Scenario: Normal user cannot change an organization's name
  When user "sarah@not.crossref.org" requests the organization with Member ID "12345" is updated to have Name "Psycho-ceramics"
  Then organization "12345" is not updated
  And response type "FORBIDDEN" is returned