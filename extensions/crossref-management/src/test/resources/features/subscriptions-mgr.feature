Feature: Organization Subscriptions API

A subscription is a group of features that we give to an organization as part of a contract. Currently, the only subscription we provide is Metadata Plus, which organizations pay for. Some of the Plus subscribers are members, and some are not.

Background:
  Given the following user types
    | Type ID | Description                                                                       |
    | STAFF   | A member of the Crossref support team who has full access to the API              |
    | USER    | A logged in user who is not a Crossref employee and has limited access to the API |
    | GUEST   | A person who is not logged in and has minimal access to the API                   |
  And the following users
    | User Type | Email Address          |
    | STAFF     | josh@crossref.org      |
    | USER      | sarah@not.crossref.org |
    | USER      | mike@not.crossref.org  |
  And the following organizations
    | Member ID | Name                |
    | 12345     | Psychoceramics      |
    | 67890     | Dept. of Psychology |
    | 19283     | Wesleyan University |
  And the following user organization assignments
    | Email Address          | Organization |
    | sarah@not.crossref.org | 12345        |
    | mike@not.crossref.org  | 12345        |
    | mike@not.crossref.org  | 67890        |
  And the following subscription types
    | Subscription ID | Description |
    | METADATA_PLUS    | Enhanced access to Crossref APIs and snapshot	|
  And the following response types
    | Type ID              | Description                                                              |
    | SUBSCRIPTION_ADDED   | A new user was created                                                   |
    | SUBSCRIPTION_REMOVED | An existing user was deleted                                             |
    | FORBIDDEN            | An attempted operation was refused because the user is not authorized    |

Scenario: Staff can add subscription to an org
  When "sarah@not.crossref.org" creates the key whose ID will be "111" for the organization "12345"
  And "josh@crossref.org" adds "METADATA_PLUS" to Member ID "12345"
  Then response type "SUBSCRIPTION_ADDED" is returned
  And Key ID "111" can access Metadata Plus services

Scenario: User cannot add subscription to an org
  When "sarah@not.crossref.org" creates the key whose ID will be "111" for the organization "12345"
  And "sarah@not.crossref.org" adds "METADATA_PLUS" to Member ID "12345"
  Then response type "FORBIDDEN" is returned
  And Key ID "111" cannot access Metadata Plus services

Scenario: Staff can view subscriptions for any org
  When "josh@crossref.org" adds "METADATA_PLUS" to Member ID "67890"
  And "josh@crossref.org" requests subscriptions for Member ID "67890"
  Then the response returns "METADATA_PLUS" subscription

Scenario: Users can view subscriptions for their own org
  When "josh@crossref.org" adds "METADATA_PLUS" to Member ID "12345"
  And "sarah@not.crossref.org" requests subscriptions for Member ID "12345"
  Then the response returns "METADATA_PLUS" subscription

Scenario Outline: Users can view subscriptions for all orgs to which they belong when they're a member of multiple orgs
  When "josh@crossref.org" adds "METADATA_PLUS" to Member ID <Member ID>
  And "mike@not.crossref.org" requests subscriptions for Member ID <Member ID>
  Then the response returns "METADATA_PLUS" subscription

  Examples:
    | Member ID |
    | "12345"   |
    | "67890"   |

Scenario: User cannot view subscriptions for orgs they don't belong to
  When "sarah@not.crossref.org" requests subscriptions for Member ID "67890"
  Then response type "FORBIDDEN" is returned

Scenario: Staff can remove subscription from an org
  When "mike@not.crossref.org" creates the key whose ID will be "222" for the organization "67890"
  And "josh@crossref.org" removes "METADATA_PLUS" from Member ID "67890"
  Then response type "SUBSCRIPTION_REMOVED" is returned
  And Key ID "222" cannot access Metadata Plus services

Scenario: Users cannot remove subscription from an org
  When "sarah@not.crossref.org" removes "METADATA_PLUS" from Member ID "12345"
  Then response type "FORBIDDEN" is returned