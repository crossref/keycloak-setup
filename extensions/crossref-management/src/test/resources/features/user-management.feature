Feature: User Management API

Users interact with Crossref. They usually belong to an organization, sometimes more than one. We give them a user account and credentials with which they can identify themselves and log in. Once created, an user's Email Address is read-only.

Keycloak handles credentials, so we don't describe that process here. This includes notifying new users via email.

Background:
  Given the following user types
    | Type ID | Description                                                                       |
    | STAFF   | A member of the Crossref support team who has full access to the API              |
    | USER    | A logged in user who is not a Crossref employee and has limited access to the API |
    | GUEST   | A person who is not logged in and has minimal access to the API                   |
  And the following users
    | User Type | Email Address          |
    | STAFF     | josh@crossref.org      |
    | USER      | sarah@not.crossref.org |
    | USER      | mike@not.crossref.org  |
  And the following organizations
    | Member ID | Name                |
    | 12345     | Psychoceramics      |
    | 67890     | Dept. of Psychology |
    | 19283     | Wesleyan University |
  And the following user organization assignments
    | Email Address          | Organization |
    | sarah@not.crossref.org | 12345        |
    | mike@not.crossref.org  | 12345        |
    | mike@not.crossref.org  | 67890        |
  And the following response types
    | Type ID         | Description                                                              |
    | USER_CREATED    | A new user was created                                                   |
    | USER_DELETED    | An existing user was deleted                                             |
    | USER_ASSIGNED   | A user was assigned to an organization                                   |
    | USER_UNASSIGNED | A user was unassigned from an organization                               |
    | DUPLICATED_USER | A new user was not created because it is a duplicate of an existing user |
    | FORBIDDEN       | An attempted operation was refused because the user is not authorized    |

Scenario Outline: Staff user can create a new user
  When user "josh@crossref.org" requests the creation of a new user with Email Address <Email Address> and User Type <User Type>
  Then the user is created
  And response type "USER_CREATED" is returned

  Examples:
    | Email Address            | User Type |
    | "sally@crossref.org"     | "STAFF"   |
    | "sally@not.crossref.org" | "USER"    |

Scenario: Staff user cannot create a new use that is a duplicate of an existing user
  When user "josh@crossref.org" requests the creation of a new user with Email Address "mike@not.crossref.org" and User Type "STAFF"
  Then the user is not created
  And user "mike@not.crossref.org" is not updated
  And response type "DUPLICATED_USER" is returned

Scenario Outline: Normal user cannot create a new user
  When user "sarah@not.crossref.org" requests the creation of a new user with Email Address <Email Address> and User Type <User Type>
  Then the user is not created
  And response type "FORBIDDEN" is returned

  Examples:
    | Email Address            | User Type   |
    | "sally@crossref.org"     | "STAFF"     |
    | "sally@not.crossref.org" | "USER"      |

Scenario: Staff user can delete an existing user
  When user "josh@crossref.org" requests the deletion of an existing user with Email Address "sarah@not.crossref.org"
  Then user "sarah@not.crossref.org" is deleted
  And response type "USER_DELETED" is returned

Scenario: Normal user cannot delete an existing user
  When user "sarah@not.crossref.org" requests the deletion of an existing user with Email Address "mike@not.crossref.org"
  Then user "mike@not.crossref.org" is not deleted
  And response type "FORBIDDEN" is returned

Scenario: Staff user can assign a user to an organization
  When user "josh@crossref.org" requests that the user with Email Address "sarah@not.crossref.org" is assigned to the organization with Member ID "67890"
  Then user "sarah@not.crossref.org" is assigned to organization "67890"
  And user "sarah@not.crossref.org" is still assigned to organization "12345"
  But user "sarah@not.crossref.org" is assigned to no other organizations
  And response type "USER_ASSIGNED" is returned

Scenario: Staff user can assign a user to multiple organizations
  When user "josh@crossref.org" requests that the user with Email Address "sarah@not.crossref.org" is assigned to organizations with the following member IDs
    | Member ID |
    | 67890     |
    | 19283     |
  Then user "sarah@not.crossref.org" is assigned to organization "67890"
  And user "sarah@not.crossref.org" is assigned to organization "19283"
  And user "sarah@not.crossref.org" is still assigned to organization "12345"
  But user "sarah@not.crossref.org" is assigned to no other organizations
  And response type "USER_ASSIGNED" is returned

Scenario: Staff user can assign multiple users to an organization
  When user "josh@crossref.org" requests that users with the following email addresses are assigned to the organization with Member ID "19283"
    | Email Address          |
    | sarah@not.crossref.org |
    | mike@not.crossref.org  |
  Then user "sarah@not.crossref.org" is assigned to organization "19283"
  And user "sarah@not.crossref.org" is still assigned to organization "12345"
  But user "sarah@not.crossref.org" is assigned to no other organizations
  And user "mike@not.crossref.org" is assigned to organization "19283"
  And user "mike@not.crossref.org" is still assigned to organization "12345"
  And user "mike@not.crossref.org" is still assigned to organization "67890"
  But user "mike@not.crossref.org" is assigned to no other organizations
  And response type "USER_ASSIGNED" is returned

Scenario: Normal user cannot assign a user to an organization
  When user "mike@not.crossref.org" requests that the user with Email Address "sarah@not.crossref.org" is assigned to the organization with Member ID "67890"
  Then user "sarah@not.crossref.org" is not assigned to organization "67890"
  But user "sarah@not.crossref.org" is still assigned to organization "12345"
  And user "sarah@not.crossref.org" is assigned to no other organizations
  And response type "FORBIDDEN" is returned

Scenario: Staff user can unassign a user from an organization
  When user "josh@crossref.org" requests that the user with Email Address "mike@not.crossref.org" is unassigned from the organization with Member ID "67890"
  Then user "mike@not.crossref.org" is unassigned from organization "67890"
  But user "mike@not.crossref.org" is still assigned to organization "12345"
  And user "mike@not.crossref.org" is assigned to no other organizations
  And response type "USER_UNASSIGNED" is returned

Scenario: Staff user can unassign a user from multiple organizations
  When user "josh@crossref.org" requests that the user with Email Address "mike@not.crossref.org" is unassigned from organizations with the following member IDs
    | Member ID |
    | 12345     |
    | 67890     |
  Then user "mike@not.crossref.org" is unassigned from organization "12345"
  And user "mike@not.crossref.org" is unassigned from organization "67890"
  And user "mike@not.crossref.org" is assigned to no other organizations
  And response type "USER_UNASSIGNED" is returned

Scenario: Staff user can unassign multiple users from an organization
  When user "josh@crossref.org" requests that users with the following email addresses are unassigned from the organization with Member ID "12345"
    | Email Address          |
    | sarah@not.crossref.org |
    | mike@not.crossref.org  |
  Then user "sarah@not.crossref.org" is unassigned from organization "12345"
  And user "sarah@not.crossref.org" is assigned to no other organizations
  And user "mike@not.crossref.org" is unassigned from organization "12345"
  But user "mike@not.crossref.org" is still assigned to organization "67890"
  And user "mike@not.crossref.org" is assigned to no other organizations
  And response type "USER_UNASSIGNED" is returned

Scenario: Normal user cannot unassign a user from an organization
  When user "mike@not.crossref.org" requests that the user with Email Address "sarah@not.crossref.org" is unassigned from the organization with Member ID "12345"
  Then user "sarah@not.crossref.org" is still assigned to organization "12345"
  But user "sarah@not.crossref.org" is assigned to no other organizations
  And response type "FORBIDDEN" is returned

Scenario: Staff user can list all users
  When user "josh@crossref.org" requests a list of all users
  Then the list of all users is returned

Scenario: Normal user can list all users in their organization
  When user "sarah@not.crossref.org" requests a list of all users in organization with Member ID "12345"
  Then user "sarah@not.crossref.org" is returned
  And user "mike@not.crossref.org" is returned

Scenario: Normal user cannot list users in an organization they don't belong to
  When user "sarah@not.crossref.org" requests a list of all users in organization with Member ID "67890"
  Then response type "FORBIDDEN" is returned

Scenario: Normal user cannot list all users
  When user "sarah@not.crossref.org" requests a list of all users
  Then response type "FORBIDDEN" is returned

Scenario: Normal user can request to which organization they belong
  When user "sarah@not.crossref.org" requests the organizations "sarah@not.crossref.org" belongs to
  Then organization "12345" is returned

Scenario: Normal user cannot request to which organization other user belongs to
  When user "sarah@not.crossref.org" requests the organizations "mike@not.crossref.org" belongs to
  Then response type "FORBIDDEN" is returned

Scenario: Staff user can request to which organization other user belongs to
  When user "josh@crossref.org" requests the organizations "sarah@not.crossref.org" belongs to
  Then organization "12345" is returned
