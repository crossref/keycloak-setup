import csv
import sys
import requests
from requests.auth import HTTPBasicAuth
import os
import uuid
from keycloak import KeycloakAdmin, KeycloakOpenID
import json
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import hashlib
import time





AUTHENTICATOR_URL = os.environ["AUTHENTICATOR_URL"]
KEYCLOAK_URL= os.environ["KEYCLOAK_URL"]
KEYCLOAK_ADMIN_PWD=os.environ["KEYCLOAK_ADMIN_PWD"]
KEYCLOAK_APIKEY_SALT = os.environ["KEYCLOAK_APIKEY_SALT"]
KEYCLOAK_STAFF_PWD = os.environ["KEYCLOAK_STAFF_PWD"]
KEYCLOAK_STAFF_USER = os.environ["KEYCLOAK_STAFF_USER"]

basic = HTTPBasicAuth(os.environ["AUTH_SVC"], os.environ["AUTH_SVC_PWD"])

class Token:
    def __init__(self, user , pwd):
        self.keycloakObj = KeycloakOpenID(
            server_url=KEYCLOAK_URL,
            client_id="ui",
            realm_name="crossref",
            verify=False)

        self.token = self.keycloakObj.token(user,pwd)
        self.expire_time = time.time() + self.token["expires_in"] - 30

    def access_token(self):
        if time.time() > self.expire_time:
            self.token =self.keycloakObj.refresh_token(self.token["refresh_token"])
            self.expire_time = time.time() + self.token["expires_in"] - 30

        return self.token["access_token"]

def keymanager_request(func):
    def wrapper(*args,**kwargs):
        prn = kwargs.get("prn",False)
        if prn:
            print(
                "=============== " + KEYCLOAK_URL + args[0] + " ====================="
            )
        resp = func(*args,**kwargs)

        content = resp.content.decode()
        if 'application/json' in resp.headers.get('Content-Type',""):
            content = json.loads(content)
        if prn:
            print(f"=================== {resp.status_code} ========================")
        return content
    wrapper.__outer__ = func.__name__
    return wrapper

def generate_hash(key):
    q=hashlib.sha256()
    q.update((KEYCLOAK_APIKEY_SALT+key).encode())
    return q.hexdigest()

@keymanager_request
def do_GET(tok,path,**kwargs):
   headers = {"Authorization": "Bearer " + tok}
   return requests.get(KEYCLOAK_URL + path, headers=headers, verify=False)

@keymanager_request
def do_POST(tok,path,contents,**kwargs):
   headers = {"Authorization": "Bearer " + tok}
   if type(contents)==str:
       headers['Content-type']= "Application/x-www-form-urlencoded"
       return requests.post(KEYCLOAK_URL + path, headers=headers, data=contents, verify=False)
   elif type (contents==dict):
       return requests.post(KEYCLOAK_URL + path, headers=headers, json=contents, verify=False)

@keymanager_request
def do_PUT(tok,path,contents,**kwargs):
   headers = {"Authorization": "Bearer " + tok}
   return requests.put(KEYCLOAK_URL + path, headers=headers, json=contents, verify=False)

def userExistsInAuthenticator(uname):
    response = requests.post(AUTHENTICATOR_URL+"/api/v1/user-info", 
                 auth=basic,
                 json = {"username": uname})

    try:
        return response.json()["status"]=="success"
    except:
        return False

def createAuthenticatorUser(uname):
    print ("Creating {} in Authenticator...".format(uname))

    response = requests.post(AUTHENTICATOR_URL+"/api/v1/add-user",
                           auth=basic,
                           json = {"username":uname, "password":str(uuid.uuid4())})

    print(response.text)

def createOrg(memberId,name,tok):
    resp=do_POST(tok,"realms/crossref/crossref-management/organization",{"orgId":"9d7074ad-4622-40e8-9807-8139bc9b0bb7","name":name,"memberId":memberId})
    resp2=do_PUT(tok, "realms/crossref/crossref-management/organization/{}/subscription/METADATA_PLUS".format(memberId),"")

    print(resp2)

    if resp.get("name") == name:
        print ("Organization {}  [{}] created.".format(name,memberId))
    elif resp.get("type")=='DUPLICATED_ORG':
        print ("Organization {}  [{}] already exists.".format(name,memberId))
    else:
        print ("Organization {}  [{}] ERROR creating.".format(name,memberId))

def orgExistsInKeycloak(memberId, name, tok):
    try: 
        resp=do_GET(tok,"realms/crossref/crossref-management/organization/{}".format(memberId))
        if resp.get("type")=="ORG_NOT_FOUND":
            return False
        elif resp.get("name") == name:
            return True
        raise Exception("error")
    except:
        print ("Error testing org {} [{}]".format(name, memberId))
        return True

def createKeycloakUser(email, memberId, tok, userId=None):
    if not userId:
        print ("Creating user {} in keycloak".format(email))
        resp=do_POST(tok,"realms/crossref/crossref-management/user",{"username":email})
        print (resp)
        userId=resp['userId']
    print ("Attaching user {} to group {}".format(email,memberId))
    print (do_POST(tok,"realms/crossref/crossref-management/organization/{}/user/".format(memberId)+userId,""))
    return userId

def createApiKey(tok, memberId, key, kc):
    print ("creating key for memberid {}".format(memberId))
    resp = do_POST(tok,"realms/crossref/crossref-management/organization/{}/key".format(memberId),{"description":"","issuedBy":"effaabea-9857-4bab-9c9a-e75d447682fe","keyId":"09d3aa41-48e6-42ec-bc9b-b280004819b2","name":"8c1420df-406c-45fd-a3d4-6a4a069e5cc1"})
    print(resp)
    try:
      keyId = resp["keyObject"]['keyId']
    except:
      print ("error creating api key for {}".format(memberId))
      return

    user=kc.get_user(keyId)
    user["attributes"]["api-key"][0]=generate_hash(key)
    kc.update_user(keyId,user)

    
def getKeycloakUsers(tok):
    return {i['username']: i['userId'] for i in do_GET(tok,"realms/crossref/crossref-management/user?pagesize=20000")}

#######################################################################
#################### MIGRATION START ##################################
#######################################################################

staff=Token(KEYCLOAK_STAFF_USER, KEYCLOAK_STAFF_PWD)

if not userExistsInAuthenticator(os.environ["AUTH_SVC"]):
    print("Wrong credentials")
    sys.exit(1)

csv_file = open(sys.argv[1])

rows = [i for i in csv.reader(csv_file, delimiter=",", quotechar='"')]

headers=rows[0]

rows = [{j:k for j,k in zip(headers,i)} for i in rows[1:]]#[:1]   # DELETE THIS FOR COMPLETE DATASET

# map of orgs
orgs = {}
for i in rows:
    if i["Name"] not in orgs:
        orgs[i["Name"]] = i["Crossref Member ID"]
    elif orgs[i["Name"]] != i["Crossref Member ID"]:
        raise Exception("Mismatch in orgs! {}".format(i["Crossref Member ID"]))

# Convert emails to lowercase
for i in rows:
    i["Email Address"] = i["Email Address"].lower()

# User creation in Authenticator
print("USER CREATION IN AUTHENTICATOR")

nonExistentUsers=[]
for i in rows:
    sys.stderr.write("Testing: {}                             \r".format(i["Email Address"]))
    sys.stderr.flush()
    if not userExistsInAuthenticator(i["Email Address"]):
        nonExistentUsers.append(i["Email Address"])

print("                                                           ")

if nonExistentUsers:
    print (nonExistentUsers)
    input("Press ENTER to create the previous users in Authenticator.")
    
    for i in nonExistentUsers:
        createAuthenticatorUser(i, staff.access_token())

    print ("DONE!!\n")


# Org creation
print("ORGANIZATION CREATION IN KEYCLOAK")

nonExistentOrgs=[]

for name, memberId in orgs.items():
    sys.stderr.write("Testing: {}                             \r".format(name))
    sys.stderr.flush()
    if not orgExistsInKeycloak(memberId, name, staff.access_token()):
        nonExistentOrgs.append((memberId, name))

print("                                                           ")

if nonExistentOrgs:
    print (nonExistentOrgs)
    input("Press ENTER to create the previous Organizations in KeyCloak.")
    
    for i,j in nonExistentOrgs:
        createOrg(i, j, staff.access_token())

    print ("DONE!!\n")

keycloakUsers = getKeycloakUsers(staff.access_token())


try:
    kc = KeycloakAdmin(
                            server_url=KEYCLOAK_URL,
                            username='admin',
                            password=KEYCLOAK_ADMIN_PWD,
                            realm_name="crossref",
                            user_realm_name="master",
                            client_id="admin-cli",
                            auto_refresh_token = ['get', 'put', 'post', 'delete'],
                            verify=False,
                            timeout=9999)
except:
    print("Wrong keymaker admin credentials")
    sys.exit(0)

migratorpwd=str(uuid.uuid4())+"Aa_!"

print ("Creating migrator_@crossref.org")
memberIds = set([i["Crossref Member ID"] for i in rows if i["Crossref Member ID"].isdigit()])
if "migrator_@crossref.org" not in keycloakUsers:
    keycloakUsers["migrator_@crossref.org"]=createKeycloakUser("migrator_@crossref.org", memberIds.pop(), staff.access_token())
for i in memberIds:
    print (do_POST(staff.access_token(),"realms/crossref/crossref-management/organization/{}/user/".format(i)+keycloakUsers["migrator_@crossref.org"],""))

print(kc.set_user_password(keycloakUsers["migrator_@crossref.org"],migratorpwd,False))
print ("DONE!!")

tok_migrator = Token("migrator_@crossref.org", migratorpwd)

for i in rows:
    if userExistsInAuthenticator(i["Email Address"]) and i["Email Address"] not in keycloakUsers:
        userId = createKeycloakUser(i["Email Address"], i["Crossref Member ID"], staff.access_token(), keycloakUsers.get(i["Email Address"]))
        keycloakUsers[i["Email Address"]]=userId
    createApiKey(tok_migrator.access_token(), i["Crossref Member ID"],i["REST API and OAI PMH Plus token"], kc)
